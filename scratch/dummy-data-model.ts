import dummyData from './dummy'

/**
 * Scale an array of numbers to be within a range of min, max
 * @param rangeVals Array of numbers to scale
 * @param newMin Minimum of new bounding range
 * @param newMax Maximum of new bounding range
 */
export function scaleNumbersToRange(rangeVals: number[], newMin: number, newMax: number) {
  const oldMin = rangeVals[0]
  const oldMax = rangeVals[rangeVals.length - 1]
  const riderSize = oldMax - oldMin
  const deltaMin = newMin - oldMin
  const deltaMax = newMax - oldMax
  // Slide old range values to start at new min
  const oldRangeValsAtNewMin = rangeVals.map((val) => {
    return val + deltaMin
  })
  const oldMaxAfterSliding = newMin + riderSize
  const oldMaxSlidingAmt = oldMaxAfterSliding - newMax // amount that old max needs to slide to become new max, this amount represents the maximum sliding range values will need to do to become new range
  return oldRangeValsAtNewMin.map((val, i) => {
    const deltaFromMin = val - newMin
    const slidingAmt = deltaFromMin / riderSize * oldMaxSlidingAmt
    return val - slidingAmt
  })
}

/**
 * Take a data series and double it on itself
 * @param data Array of xy values (array)
 */
export function doubleData(data: number[][]) {
  const timeStart = data[0][0]
  const timeEnd = data[data.length - 1][0]
  const timeSize = Math.floor(timeEnd - timeStart)

  const shiftedDupData = data.map(xy => [xy[0] + timeSize, xy[1]])
  return data.concat(shiftedDupData)
}


const TRADING_DAY_START = new Date(Date.UTC(2019, 7-1, 1, 13, 30, 0, 0)).valueOf() // Mon Jul 01 2019 09:30:00 GMT-0400 (Eastern Daylight Time)
const TRADING_DAY_END = TRADING_DAY_START + 6.5 * 60 * 60 * 1000 // Mon Jul 01 2019 16:00:00 GMT-0400 (Eastern Daylight Time)


function scaleDataToTimeRange(data: number[][]){
  const timeSeries = data.map(xy => xy[0])
  const scaled = scaleNumbersToRange(timeSeries, TRADING_DAY_START, TRADING_DAY_END)
  // swap in newly scalled time series
  return data.map((xy, i) => [Math.floor(scaled[i]), xy[1]])
}

export function getData(){
  return scaleDataToTimeRange(
    doubleData(
      doubleData(
        doubleData(
          dummyData
        )
      )
    )
  )
}

const getRandInRange = (rangeMax: number) => Math.floor(Math.random() * rangeMax)

function getRand(howManyRand: number, rangeMax: number) {
  if (howManyRand > rangeMax)
    throw Error(
      "Cannot generate more random numbers than than length of what's available!"
    );
  const rands: { [index: string]: boolean } = {};
  let num;
  for (let i = 0; i < howManyRand; i++) {
    num = getRandInRange(rangeMax);
    if (rands[num] === undefined) {
      rands[num] = true;
    } else {
      howManyRand += 1;
    }
  }
  return rands;
}

export function getDataScatter(){
  const numRandIndexes = Math.floor(dummyData.length * 0.85) // cull 40% of indexes
  const idxToCullLookUp: { [index: string]: boolean } = getRand(numRandIndexes, dummyData.length)
  // console.log(dummyData.length, numRandIndexes, idxToCullLookUp)

  return getData().slice().filter((item, i) => !idxToCullLookUp[i]).map(item => [item[0], item[1] + getRandInRange(20) - getRandInRange(20) ])
}

