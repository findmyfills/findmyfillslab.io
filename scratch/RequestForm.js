

class RequestForm extends React.Component{
  constructor(props){
    super(props)
    this.state = {
      method: 'GET',
      endpoint:'http://localhost:8080',
      reqBody: `{ "title": "foo", "body": "bar", "userId": 1 }`,
    }
  }

  render(){
    const { method, endpoint, reqBody } = this.state
    return (
      <div className="container-request-form">
        <style jsx>{`
        .container-request-form {
          width: 500px;
        }
        .field {
          margin-bottom: 20px;
        }
        input {
          width: 100%;
          height: 50px;
        }
      `}</style>
        <p>https://jsonplaceholder.typicode.com/posts/1/comments</p>
        <p>https://jsonplaceholder.typicode.com/posts</p>
        <div className="field">
          <select name="Method" value={method} onChange={({ target }) => this.setState({ method: target.value })}>
            <option value="GET">GET</option>
            <option value="POST">POST</option>
          </select>
        </div>
        <div className="field">
          <label>
            api end point:
            <input 
              type="text"
              value={endpoint}
              onChange={({ target }) => this.setState({ endpoint: target.value })}
              />
          </label>
        </div>
        <div className="field">
          <label>
            Req. Body:
            <input 
              type="text"
              value={reqBody}
              disabled={method === 'GET'}
              onChange={({ target }) => this.setState({ reqBody: target.value })}
              />
          </label>
        </div>
        <div className="field">
          <button 
            onClick={()=>{
              const fetchParams = {
                method,
                headers: {
                  'content-type': 'application/json; charset=UTF-8'
                },
              }

              if (method === 'POST'){
                Object.assign(fetchParams, {
                  body: reqBody
                })
              }

              fetch(endpoint, fetchParams)
                .then(res => {
                  const contentType = res.headers.get('Content-Type')
                  console.log(`response header content type: %c${contentType}`,'color: teal')
                  if (contentType.indexOf('application/json') !== -1){
                    return res.json()
                  } else if (contentType.indexOf('text/html') !== -1) {
                    return res.text()
                  } else {
                    return res
                  }
                })
                .then(data => console.log(data) )
                .catch(err => console.error(err) )
            }}
            >
            Submit
          </button>
        </div>
      </div>
    )
  }
}


export default RequestForm