import { ISearchIndex } from 'js-search';

export default class RankedSearchIndex implements ISearchIndex {
	_tokenToUidToDocumentMap = {}

	indexDocument(token, uid, doc) {
		if (typeof this._tokenToUidToDocumentMap[token] !== 'object') {
			this._tokenToUidToDocumentMap[token] = {}
		}

		this._tokenToUidToDocumentMap[token][uid] = doc
	}

	search(tokens, corpus) {
		const intersectingDocumentMap = {}
		let exactMatch
		let exactMatchToken
		const beginsWith = []
		const tokenToUidToDocumentMap = this._tokenToUidToDocumentMap

		for (let i = 0, numTokens = tokens.length; i < numTokens; i++) {
			const token = tokens[i]
			const documentMap = tokenToUidToDocumentMap[token]

			if (!documentMap) {
				return []
			}

			if (i === 0) {
				if (documentMap[token]) {
					exactMatch = documentMap[token.toUpperCase()]
					exactMatchToken = token.toUpperCase()
				}

				const keys = Object.keys(documentMap)

				for (let j = 0, numKeys = keys.length; j < numKeys; j++) {
					const uid = keys[j]

					if (uid === exactMatchToken) {
						continue
					} else if ((new RegExp("^" + token + ".*", 'i')).test(uid)) {
						beginsWith.push(documentMap[uid])
						continue
					}

					intersectingDocumentMap[uid] = documentMap[uid]
				}
			} else {
				const keys = Object.keys(intersectingDocumentMap)

				for (let j = 0, numKeys = keys.length; j < numKeys; j++) {
					const uid = keys[j]

					if (typeof documentMap[uid] !== 'object') {
						delete intersectingDocumentMap[uid]
					}
				}
			}
		}

		const keys = Object.keys(intersectingDocumentMap)
		const documents = []

		for (let i = 0, numKeys = keys.length; i < numKeys; i++) {
			const uid = keys[i]

			documents.push(intersectingDocumentMap[uid])
		}

		const finalDocuments = beginsWith.concat(documents)

		exactMatch && finalDocuments.unshift(exactMatch)

		return finalDocuments
	}
}
