import React, { ReactType } from 'react';
import config from '../app.config';
import LoginPrompt from '../components/LoginPrompt';
import fuiPendingAuthFix from '../styles/firebaseui-fix';
import { SessionStatus } from '../types';
import { handleFetchErrors, pick } from '../util';

const FIREBASE_UI_CONTAINER_ID = 'firebaseui-auth-container'

const FIREBASE_CONFIG = {
  apiKey: "AIzaSyBJCRSO1zXTNmeCtl-tX7svrBb41m1racY",
  authDomain: "findmyfills-api.firebaseapp.com",
  databaseURL: "https://findmyfills-api.firebaseio.com",
  projectId: "findmyfills-api",
  storageBucket: "findmyfills-api.appspot.com",
  messagingSenderId: "775506017401",
  appId: "1:775506017401:web:59782fe9de1415af"
};

const FIREBASE_UI_CONFIG = {
  // signInSuccessUrl: '/login/index.html',
  tosUrl: '/disclaimer', // Terms of service url.
  privacyPolicyUrl: '/privacy', // Privacy policy url.
  callbacks: {
    signInSuccessWithAuthResult: function(authResult, redirectUrl) {
      // User successfully signed in.
      console.log('signInSuccessWithAuthResult', authResult, redirectUrl)
      return false; // don't redirect automatically
    },
    signInFailure: function(error) {
      // Some unrecoverable error occurred during sign-in.
      // Return a promise when error handling is completed and FirebaseUI
      // will reset, clearing any UI. This commonly occurs for error code
      // 'firebaseui/anonymous-upgrade-merge-conflict' when merge conflict
      // occurs. Check below for more details on this.
      console.error(error)
    },
  },
}


export type FirebaseUser = {
  displayName: string
  email: string
  emailVerified: true
  isAnonymous: false
  metadata: {
    a: string // "1564203163959"
    b: string // "1564270635719"
    lastSignInTime: string // "Sat, 27 Jul 2019 23:37:15 GMT"
    creationTime: string // "Sat, 27 Jul 2019 04:52:43 GMT
  }
  phoneNumber: string | null
  photoURL: string | null
  providerData: {
    displayName: string | null
    email: string | null
    phoneNumber: string | null
    photoURL: string | null
    providerId: string | null
    uid: string
  }[]
  refreshToken: string
}


enum AuthenticatedUserType {
  displayName = 'displayName',
  email = 'email',
  emailVerified = 'emailVerified',
  phoneNumber = 'phoneNumber',
  photoURL = 'photoURL',
  providerData = 'providerData'
}

export type AuthenticatedUser = Pick<FirebaseUser, AuthenticatedUserType>

export const AuthUserContext = React.createContext<AuthenticatedUser | null>(null)


type Props = { }
type State = {
  user: AuthenticatedUser | null
  sessionStatus: SessionStatus
  sessionStatusTime: number | undefined
}

export function withLogin(WrappedComponent: ReactType){

  class LoginWrapper extends React.Component<Props, State> {
    static async getInitialProps ({ req, query }) {
      const user = req && req.session ? req.session.decodedToken : null
      // don't fetch anything from firebase if the user is not found
      // const snap = user && await req.firebaseServer.database().ref('messages').once('value')
      // const messages = snap && snap.val()
      const messages = null
      return { user, messages }
    }

    static setToken = (token: string) => {
      document.cookie = "token=" + token;
    }

    fbUiInstance: any | null

    constructor (props: Props) {
      super(props)
      this.fbUiInstance = null
      this.state = {
        user: null,
        sessionStatus: SessionStatus.SIGNED_OUT,
        sessionStatusTime: undefined,
      }
    }

    componentDidMount () {
      this.initFirebaseInstance()
      this.initFirebaseUiInstance()
    }

    initSessionWithToken(
      accessToken: string,
      userInfos: Pick<FirebaseUser, AuthenticatedUserType>,
    ){
			const endpoint = `${config.API_DOMAIN}/initSession/`

      fetch(endpoint, {
        method: 'POST',
        credentials: 'include',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({
          'id_token': accessToken
        })
      }).then(handleFetchErrors).then(res => {
        res.text().then((payload) => {
          console.log(`/initSession at ${Date.now()}: ${payload}`)
          this.setState({
            user: userInfos,
            sessionStatus: SessionStatus.SIGNED_IN,
            sessionStatusTime: Date.now(),
          })
        })
      }).catch((error) => {
        console.log(error);
      })
    }

    handleUserSignOut = () => {
			firebase.auth().signOut();
			
			const endpoint = `${config.API_DOMAIN}/endSession/`

      fetch(endpoint, {
        method: 'POST',
        credentials: 'include',
        headers: { 'Content-Type': 'application/json' },
        body: ''
      }).then(handleFetchErrors).then(res => {
        this.setState({
          sessionStatus: SessionStatus.SIGNED_OUT,
          sessionStatusTime: Date.now(),
          user: null,
        })
        res.text().then((payload) => {
          console.log(`/endSession at ${Date.now()}: ${payload}`)
        })
      }).catch((error)=>{
        console.log(error)
      })
    }

    changeTosTextToDisclaimer(){
      const elem = Array.from(document.getElementsByClassName('firebaseui-tos-link'))[0]
      if(elem !== undefined){
        elem.textContent = 'Disclaimer'
      }
    }

    attachHandlersToSignInBtns(){
      const signInBtns = Array.from(document.getElementsByClassName('firebaseui-idp-button'))
      console.log('signInBtns ----------------->', signInBtns)
      signInBtns.forEach((btn_el)=>{
        btn_el.addEventListener('click', ()=>{
          console.log('signInBtns CLICKEDDD ++++++++++++')
          this.setPendingStatus()
        })
      })
    }

    setPendingStatus(){
      this.setState({
        sessionStatus: SessionStatus.PENDING,
        sessionStatusTime: Date.now()
      })
    }

    initFirebaseInstance(){
      firebase.initializeApp(FIREBASE_CONFIG)
      firebase.auth().onAuthStateChanged((user: FirebaseUser | null) => { // onAuthStateChanged listener triggers every time the user ID token changes. This could happen when a new user signs in or signs out. It could also happen when the current user ID token expires and is refreshed.
        this.setPendingStatus()
        console.log('firebase.auth().onAuthStateChanged', user)
        if (user !== null) {  // If user is signed IN...
          const userInfos = pick(user, Object.keys(AuthenticatedUserType) as Array<(AuthenticatedUserType)>)
          user.getIdToken().then((accessToken: string) => {
            // Add the token to the browser's cookies. The server will then be able to verify the token against the API.
            // SECURITY NOTE: As cookies can easily be modified, only put the token (which is verified server-side) in a cookie; do not add other user information.
            this.initSessionWithToken(accessToken, userInfos)
          });
        } else { // If user is signed OUT...
          this.renderFirebaseUiToDom()  // Show the Firebase login button.
          this.setState({
            user: null,
            sessionStatus: SessionStatus.SIGNED_OUT,
          })
        }
      }, (error: Error) => {
        alert('Unable to log in: ' + error)
      });
    }

    initFirebaseUiInstance(){
      this.fbUiInstance = firebaseui.auth.AuthUI.getInstance() || new firebaseui.auth.AuthUI(firebase.auth());
    }

    renderFirebaseUiToDom(){
      const { fbUiInstance } = this
      if(fbUiInstance !== null){
        console.log('---------renderFirebaseUI---------')
        fbUiInstance.start(
          `#${FIREBASE_UI_CONTAINER_ID}`,
          Object.assign({},FIREBASE_UI_CONFIG, {
            credentialHelper: firebaseui.auth.CredentialHelper.NONE,
            signInOptions: [
              firebase.auth.GoogleAuthProvider.PROVIDER_ID,
              firebase.auth.EmailAuthProvider.PROVIDER_ID,
              // firebase.auth.FacebookAuthProvider.PROVIDER_ID,
              // firebase.auth.TwitterAuthProvider.PROVIDER_ID,
              // firebase.auth.GithubAuthProvider.PROVIDER_ID
            ],
          })
        )
        this.attachHandlersToSignInBtns()
        this.changeTosTextToDisclaimer()
      }
    }


    render () {
      const {
        children,
        ...restProps
      } = this.props
      const {
        user,
        sessionStatus,
        sessionStatusTime
      } = this.state

      return (
        <div>
          <style jsx global>{fuiPendingAuthFix}</style>
          {
            user !== null ? (
              <AuthUserContext.Provider value={user}>
                <WrappedComponent
                  onSignOut={this.handleUserSignOut}
                  {...restProps}
                  >
                  {children}
                </WrappedComponent>
              </AuthUserContext.Provider>
            ) : (
              <LoginPrompt
                status={sessionStatus}
                statusTime={sessionStatusTime}
                />
            )
          }
          <div id={FIREBASE_UI_CONTAINER_ID}/>
        </div>
      )
    }
  }

  return LoginWrapper
}



