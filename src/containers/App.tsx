import '@blueprintjs//icons/lib/css/blueprint-icons.css';
import { FocusStyleManager, IToastProps } from '@blueprintjs/core';
import '@blueprintjs/core/lib/css/blueprint.css';
import '@blueprintjs/datetime/lib/css/blueprint-datetime.css';
import '@blueprintjs/select/lib/css/blueprint-select.css';
import '@blueprintjs/table/lib/css/table.css';
import 'normalize.css/normalize.css';
import React from 'react';
import { connect } from 'react-redux';
import 'react-select/dist/react-select.css';
// import 'react-virtualized/styles.css';
import 'react-virtualized-select/styles.css';
import { Dispatch } from 'redux';
import { createResetAppStateAction, createSelectSymbolAction, createSetClientBrowserDimensionsAction, ResetAppStateAction, SelectSymbolAction, SetClientBrowserDimensionsAction, ToggleTooltipsAction, createToggleTooltipsAction } from '../actions';
import DataManager from '../components/DataManager';
import { withLoadingSpinner } from '../components/LoadingSpinner';
import LogOutButton from '../components/LogOutButton';
import MultiBeamNavigator from '../components/MultiBeamNavigator';
import Section from '../components/Section';
import StockChart, { withMouseIntercepts, withSeriesCreator } from '../components/StockChart';
import SymbolSearch from '../components/SymbolSearch';
import Toaster, { withToasts } from '../components/Toaster';
import UserFillsSummaryTable from '../components/UserFillsSummaryTable';
import UserFillsSummaryVisualization from '../components/UserFillsSummaryVisualization';
import UserFillsTradesTable from '../components/UserFillsTradesTable';
import { PayloadCsv } from '../models';
import { IApplicationState } from '../reducers';
// import '../styles/search.css';
import globalStyles from '../styles/global';
import { IOption } from '../types';
import CsvLoaderParser from './CsvLoaderParser';
import TimeControls from './TimeControls';
import { withToolTip } from '../components/ToolTip';


// const MultiBeamNavigatorWithToolTip = withToolTip(MultiBeamNavigator, 'tool tip text here')
// const SymbolSearchWithToolTip = withToolTip(SymbolSearch, 'Change to a new symbol.')


const DataManagerWithToasts = withToasts(DataManager)
FocusStyleManager.onlyShowFocusOnTabs(); // prevent input component will rendering outline on focus

const FillsTradesTableWithSpinner = withLoadingSpinner(
  UserFillsTradesTable,
  ({ appControls }: IApplicationState) => appControls.isFillsMetricsSpinnerActive
)

const FillsSummaryTableWithSpinner = withLoadingSpinner(
  UserFillsSummaryTable,
  ({ appControls }: IApplicationState) => appControls.isFillsMetricsSpinnerActive
)

const StockChartWithHocs = withMouseIntercepts(
  withSeriesCreator(
    withLoadingSpinner(
      StockChart,
      ({ appControls }: IApplicationState) => appControls.isStockChartSpinnerActive
    )
  )
)


type Props = {
  onSignOut?: () => void // firebase login HOC
} & StoreProps & DispatchProps

class App extends React.Component<Props> {

  constructor(props: Props) {
    super(props);
  }

  componentDidMount(){
    this.props.setBrowserDimensions(window.innerWidth, window.innerHeight)
    window.addEventListener('resize', ()=>{
      this.props.setBrowserDimensions(window.innerWidth, window.innerHeight)
    })
  }

  componentWillUnmount(){
    this.props.resetAppStateAction() // NOTE: On sign out, app state persists. If user signs back in again via Firebase Email/Password, re-entering dirty state will be unpredictable. Thus need to pre-emeptively reset all app states on unmount in order to provide a clean slate for re-entry.
  }

  render(){
    const { navigatorStepArrowW, selectedFmfCsvMetrics, browserW, browserH, selectedFmfCsvKey, timeZoneString, toggleTooltip } = this.props
    const APP_PADDING_L = 35
    const APP_PADDING_R = 35 - navigatorStepArrowW
    const APP_W = browserW - APP_PADDING_L - APP_PADDING_R
    const PANEL_LEFT_W = 380
    const PANEL_RIGHT_MARGIN_RIGHT = 15
    const PANEL_RIGHT_W = APP_W - PANEL_LEFT_W - PANEL_RIGHT_MARGIN_RIGHT
    const PANEL_RIGHT_TOP_W = PANEL_RIGHT_W - navigatorStepArrowW * 2
    const shouldRenderUserFillsTable = selectedFmfCsvMetrics !== undefined
    // console.log('browserW',browserW)
    // console.log('APP_W',APP_W)
    // console.log('navigatorStepArrowW',navigatorStepArrowW)
    // console.log('PANEL_RIGHT_W',PANEL_RIGHT_W)
    // console.log('PANEL_RIGHT_TOP_W',PANEL_RIGHT_TOP_W)
    return (
      <div id="app-container">
        <style jsx global>{globalStyles}</style>
        <style jsx>{`
          #app-container {
            padding: 15px ${APP_PADDING_R}px 15px ${APP_PADDING_L}px;
            position: absolute;
            top: 0; bottom: 0; left: 0;
            width: 100%;
            display: flex;
            overflow: hidden;
          }
          .panel-left {
            flex: 0 0 auto;
            flex-direction: column;
            width: ${PANEL_LEFT_W}px;
            display: flex;
          }
          .panel-right {
            flex: 0 0 auto;
            display: flex;
            flex-direction: column;
            margin-left: ${PANEL_RIGHT_MARGIN_RIGHT}px;
            width: ${PANEL_RIGHT_W}px;
          }
          .panel-right-top {
            padding: 0px ${navigatorStepArrowW}px;
            width: ${PANEL_RIGHT_W}px;
            display: flex;
            flex-direction: column;
            flex-grow: 1;
          }
          .panel-header {
            display: flex;
            align-items: center;
            margin: 0px;
            line-height: 60px;
            flex: 0 0 auto;
          }
        `}</style>
        <Toaster/>
        <DataManagerWithToasts/>
        <div className="panel-left-right-container">
          <div className="panel-left">
            <div>
              <a className="text-disclaimer" href="https://prooftrading.com">
                prooftrading.com
              </a>
              <p className="text-disclaimer">&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;</p>
              <a className="text-disclaimer" href="https://medium.com/prooftrading/find-my-fills-introducing-a-new-tool-for-visualizing-market-activity-at-every-timescale-4bb7df2dee0e">
                How to use this tool
              </a>
							{/* 
								<span>  |  </span>
								<a className="text-disclaimer" onClick={()=>toggleTooltip(true)}>
									tutorial
								</a>
							*/}
            </div>
            <div className="panel-header">
              <h1 id="app-title">FIND MY FILLS</h1>
            </div>
            <Section
              title="Import Your Fills"
              padding="0px"
              >
              <CsvLoaderParser
                enableDevOnlyUrlInput={false}
                />
            </Section>

            <Section
              title="Venue Breakdown & Key Metrics"
              >
              <UserFillsSummaryVisualization
                containerW={PANEL_LEFT_W}
                userFills={selectedFmfCsvMetrics && selectedFmfCsvMetrics.fills}
                />
            </Section>

            <Section
              overflow="auto"
              borderTop={true}
              >
              <FillsSummaryTableWithSpinner
                userFills={selectedFmfCsvMetrics && selectedFmfCsvMetrics.fills}
                csvSummary={selectedFmfCsvMetrics && selectedFmfCsvMetrics.summary}
                />
            </Section>

          </div>
          <div className="panel-right">
            <div className="panel-right-top">
              <div className="text-right">
                <a className="text-disclaimer" href="https://polygon.io">
                  powered by polygon.io
                </a>
              </div>
              <div className="panel-header">
                <h1 id="symbol-title">{this.props.selectedSymbolKey}</h1>
                <SymbolSearch
                  />
                <LogOutButton
                  onSignOut={this.props.onSignOut}
                  />
              </div>

              <Section
                shouldRenderContent={shouldRenderUserFillsTable}
                padding="0px"
                borderTop={true}
                >
                <FillsTradesTableWithSpinner
                  timeZoneString={timeZoneString}
                  selectedFmfCsvKey={selectedFmfCsvKey}
                  containerW={PANEL_RIGHT_TOP_W}
                  userFills={selectedFmfCsvMetrics && selectedFmfCsvMetrics.fills}
                  />
              </Section>

              <div id="container-stock-chart">
                <StockChartWithHocs
                  containerW={PANEL_RIGHT_TOP_W}
                  />
              </div>

              <Section padding="0px" border="none">
                <TimeControls
                  />
              </Section>
            </div>
            <div className=""
              style={{ marginBottom: '15px' }}
              >
              <MultiBeamNavigator
                containerW={PANEL_RIGHT_W}
                />
            </div>
          </div>
        </div>
      </div>
    )
  }
}


type StoreProps = {
  timeZoneString: string
  browserW: number
  browserH: number
  selectedFmfCsvKey: string
  selectedFmfCsvMetrics: PayloadCsv | undefined
  isFillsMetricsSpinnerActive: boolean
  selectedSymbolKey: string
  isStockChartSpinnerActive: boolean
  symbolOptions: IOption[]
  toasts: IToastProps[]
  navigatorStepArrowW: number
}

const mapStateToProps = ({ appControls, navigator, userFills: fillsMetrics }: IApplicationState) => ({
  timeZoneString: appControls.timeZoneString,
  browserW: appControls.browserW,
  browserH: appControls.browserH,
  selectedFmfCsvKey: appControls.selectedFmfCsvKey,
  selectedFmfCsvMetrics: fillsMetrics[appControls.selectedFmfCsvKey],
  isFillsMetricsSpinnerActive: appControls.isFillsMetricsSpinnerActive,
  selectedSymbolKey: appControls.selectedSymbolKey,
  navigatorStepArrowW: navigator.stepArrowW,
  isStockChartSpinnerActive: appControls.isStockChartSpinnerActive,
  symbolOptions: appControls.symbolOptions,
  toasts: appControls.toasts,
})

type DispatchProps = {
  setBrowserDimensions: SetClientBrowserDimensionsAction
  selectSymbol: SelectSymbolAction
	resetAppStateAction: ResetAppStateAction
	toggleTooltip: ToggleTooltipsAction
}

const mapDispatchToProps = (dispatch: Dispatch): DispatchProps => ({
  setBrowserDimensions: createSetClientBrowserDimensionsAction(dispatch),
  selectSymbol: createSelectSymbolAction(dispatch),
	resetAppStateAction: createResetAppStateAction(dispatch),
	toggleTooltip: createToggleTooltipsAction(dispatch),
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);


