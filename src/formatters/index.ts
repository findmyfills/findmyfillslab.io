import moment from 'moment';
import { BACKEND_FLOAT_MULTIPLIER, DUR_DAY, DUR_HR, DUR_MILLI, DUR_MIN, DUR_SEC } from '../constants';
import { PayloadCsvSummary } from '../models';
import { ADD_ON_TS_KEY, PayloadCsvFillWithTs } from '../records';
import { GRANULARITIES, INavBeam } from '../schemas/navigator';
import { FEGrainType } from '../types';
import { getLast, roundTo, withCommas } from '../util';



export type DurationFormatter = (timeRange: number) => string

export const milliFormatter: DurationFormatter = val => roundTo(val / DUR_MILLI, 0) + '-ms'
export const secFormatter: DurationFormatter = val => roundTo(val / DUR_SEC, 1) + '-sec'
export const minFormatter: DurationFormatter = val => roundTo(val / DUR_MIN, 1) + '-min'
export const hrFormatter: DurationFormatter = val => roundTo(val / DUR_HR, 1) + '-hr'
export const dayFormatter: DurationFormatter = val => roundTo(val / DUR_DAY, 1) + '-day'


export function getDurationFormater(duration: number): DurationFormatter {
  if(duration < DUR_SEC) return milliFormatter;
  if(duration < DUR_MIN) return secFormatter;
  if(duration < DUR_HR) return minFormatter;
  return hrFormatter;
}


export function getSizeFormatterFromFeGrainType(frontendGrainType: FEGrainType){
  return MAP_FE_GRAIN_TYPE_TO_TIME_FORMATTER[frontendGrainType]
}

export function getTimeRangeFormaterFromAvailableGrains(timerange: number) {
  const highestGrain = getLast(GRANULARITIES)
  let grain: INavBeam,
      matchingGrain: INavBeam,
      formatter = getSizeFormatterFromFeGrainType(highestGrain.key)
  for (let i = 1; i < GRANULARITIES.length; i++) {
    // Try to find the lowest grain that can still acommodate this timerange, otherwise default to formatter for the finest grain...
    grain = GRANULARITIES[i]
    if(timerange > grain.beamSize){
      matchingGrain = GRANULARITIES[i - 1]
      formatter = getSizeFormatterFromFeGrainType(matchingGrain.key)
      break
    }
  }
  return formatter
}

// TODO: Create custom formatting for ticks time display on rulers...
export function getRulerTicksFormatterFromFEGrainType(frontendGrainType: FEGrainType){
  return MAP_FE_GRAIN_TYPE_TO_RULER_TICKS_FORMATTER[frontendGrainType]
}


export function getFormatterForCsvFillKey(fillsKey: keyof PayloadCsvFillWithTs){
  const formatter = MAP_FILLS_KEY_TO_FORMATTER[fillsKey]
  return formatter !== undefined ? formatter : (val: any) => String(val)
}


export const MAP_FILLS_KEY_TO_FORMATTER: {
  [k in keyof PayloadCsvFillWithTs]: (val: number | string ) => string
} = {
  TIME: val => moment(val).format('YYYY/MM/DD HH:mm:ss.SSS z'),
  SYMBOL: val => String(val),
  SIZE: val => withCommas(val),
  PRICE: val => (+val).toFixed(4),
  VENUE: val => String(val),
  SIDE: val => String(val),
  matched: val => withCommas(val),
  markout: val => (+val / BACKEND_FLOAT_MULTIPLIER).toFixed(4),
  markout_px: val => (+val / BACKEND_FLOAT_MULTIPLIER).toFixed(4),
  markout_time: val => moment(val).format('HH:mm:ss.SSS'),
  [ADD_ON_TS_KEY]: val => (+val).toFixed(3),
}


export function getFormatterForCsvSummaryKey(summaryKey: keyof PayloadCsvSummary){
  const formatter = MAP_CSV_SUMMARY_KEY_TO_FORMATTER[summaryKey]
  return formatter !== undefined ? formatter : (val: any) => String(val)
}

export const MAP_CSV_SUMMARY_KEY_TO_FORMATTER: {
  [k in keyof PayloadCsvSummary]: (val: number | null ) => string
} = {
  volume: val => withCommas(val),
  count: val => withCommas(val),
  avg_size: val => withCommas(val),
  avg_price: val => withCommas(val / BACKEND_FLOAT_MULTIPLIER),
  arrival: val => withCommas(val / BACKEND_FLOAT_MULTIPLIER),
  vwap: val => withCommas(val / BACKEND_FLOAT_MULTIPLIER),
  end_midpoint: val => withCommas(val / BACKEND_FLOAT_MULTIPLIER),
  reversion_px: val => withCommas(val / BACKEND_FLOAT_MULTIPLIER),
  slippage_arrival: val => withCommas(val / BACKEND_FLOAT_MULTIPLIER),
  slippage_vwap: val => withCommas(val / BACKEND_FLOAT_MULTIPLIER),
  reversion: val => withCommas(val / BACKEND_FLOAT_MULTIPLIER),
  avg_markout: val => withCommas(val / BACKEND_FLOAT_MULTIPLIER),
}


export const MAP_FE_GRAIN_TYPE_TO_TIME_FORMATTER: {
  [K in FEGrainType]: DurationFormatter
} = {
  [FEGrainType.MILLI]: milliFormatter,
  [FEGrainType.SEC]: secFormatter,
  [FEGrainType.MIN]: minFormatter,
  [FEGrainType.HR]: minFormatter,
  [FEGrainType.DAY]: hrFormatter,
}

export const MAP_FE_GRAIN_TYPE_TO_RULER_TICKS_FORMATTER: {
  [K in FEGrainType]: (riderSize: number) => string
} = {
  [FEGrainType.MILLI]: val => roundTo(val / DUR_MILLI, 1) + 'ms',
  [FEGrainType.SEC]: val => roundTo(val / DUR_SEC, 1) + 's',
  [FEGrainType.MIN]: val => roundTo(val / DUR_SEC, 1) + 's',
  [FEGrainType.HR]: val => roundTo(val / DUR_MIN, 1) + 'min',
  [FEGrainType.DAY]: val => roundTo(val / DUR_HR, 1) + 'hr',
}