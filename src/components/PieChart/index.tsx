import Highcharts, { Chart, Options } from 'highcharts';
import React from 'react';
import { Colors } from '../../constants';
import { HcPieData } from '../../types';
import { toPctString } from '../../util';


type Props = {
  containerW: number
  containerH: number
  data: HcPieData[]
}

type State = {
}
const DEFAULT_OPTIONS: Options = {
  credits: {
    enabled: false
  },
  chart:{
    margin: 2,
    backgroundColor: Colors.TRANSPARENT,
  },
  title: {
    text: '',
  },
  legend: {
    enabled: false,
    symbolHeight: 8,
    itemStyle: {
      color: '#888',
      fontSize: '10px',
      fontWeight: 'light',
    }
  },
  tooltip: {
    formatter: function () {
      return `<b>${this.key}</b>: ${toPctString(this.y, 1)}`;
    }
  },
  plotOptions: {
    pie: {
      innerSize: '50%',
      allowPointSelect: true,
      cursor: 'pointer',
      dataLabels: { enabled: false },
      showInLegend: true,
    }
  },
}

class PieChart extends React.Component<Props, State> {

  chartInstance: Chart | null
  chartContainer: HTMLDivElement | null

  constructor(props: Props) {
    super(props)
    this.chartInstance = null
    this.chartContainer = null
    this.state = {
    }
  }

  componentDidMount() {
    this.initChart()
  }

  componentWillReceiveProps(nextProps: Props) {
    const { data } = nextProps
    if (data !== this.props.data) {
      this.updateSeries(data)
    }
  }

  initChart() {
    // const { containerW, containerH } = this.props
    const { data, containerW, containerH } = this.props
    const { chartContainer } = this
    if (chartContainer !== null) {
      if(DEFAULT_OPTIONS.chart === undefined) DEFAULT_OPTIONS.chart = {}
      DEFAULT_OPTIONS.chart.renderTo = chartContainer
      DEFAULT_OPTIONS.chart.width = containerW
      DEFAULT_OPTIONS.chart.height = containerH
      DEFAULT_OPTIONS.series = [{ type: 'pie', data }]
      this.chartInstance = Highcharts.chart(DEFAULT_OPTIONS)
    }
  }

  resizeChart(containerW: number, containerH: number) {
    const { chartInstance } = this
    if (chartInstance !== null) {
      chartInstance.setSize( containerW, containerH, false)
    }
  }

  updateSeries(pieChartData: HcPieData[]){
    const { chartInstance } = this
    if (chartInstance !== null) {
      chartInstance.series[0].update({
        type: 'pie',
        data: pieChartData
      }, false)
      chartInstance.redraw(false)
    }
  }

  render() {
    const { containerW } = this.props
    // console.log('%c Stock Chart rendering...', 'color:red')
    return (
      <div className="container-pie-chart"
        ref={ref => this.chartContainer = ref}
        />
    )
  }
}

export default PieChart