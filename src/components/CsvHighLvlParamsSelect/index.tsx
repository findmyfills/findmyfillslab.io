import { ButtonGroup, InputGroup } from '@blueprintjs/core';
import React from 'react';
import { MONTH_NAMES_FULL } from '../../constants';
import { CsvSideType, CsvTimezoneType, IOption } from '../../types';
import MenuSelect from '../MenuSelect';

type Props = {
  uniqueSymbolsInCsv: string[]
  sampleDateValueInCsv: string | number | undefined
  targetSymbol: string | undefined
  targetDate: string
  targetSide: CsvSideType
  targetTimezone: CsvTimezoneType
  onTargetSymbolChange: (symbol: string) => void
  onTargetTimezoneChange: (timezone: CsvTimezoneType) =>  void
  onTargetSideChange: (side: CsvSideType) => void
  onTargetDateChange: (mmddyyyy: string) => void
}

type State = {
  timezoneOptions: IOption[]
  sideOptions: IOption[]
  dateYearOptions: IOption[]
  dateMonthOptions: IOption[]
  dateDayOptions: IOption[]
}

const currentYear = new Date().getFullYear()
const dateYearOptions = new Array(11).fill(currentYear-10).map((yearZero, i) => ({
  value: String(yearZero + i),
  label: String(yearZero + i),
})).reverse()
const dateMonthOptions = new Array(12).fill(0).map((monthIndex, i) => ({
  value: String(monthIndex + i + 1),
  label: getMonthAbbrv(monthIndex + i),
}))

const dateDayOptions = new Array(31).fill(1).map((dayZero, i) => ({
  value: String(dayZero + i),
  label: String(dayZero + i),
}))

function getMonthAbbrv(monthIndex: number){
  return MONTH_NAMES_FULL[monthIndex].substring(0, 3).toUpperCase()
}

class CsvHighLvlParamsSelect extends React.Component<Props, State> {

  constructor(props: Props){
    super(props)

    this.state = {
      timezoneOptions: Object.keys(CsvTimezoneType).map(tz => ({ value: tz, label: tz })),
      sideOptions: Object.keys(CsvSideType).map(side => ({ value: side, label: side })),
      dateYearOptions,
      dateMonthOptions,
      dateDayOptions,
    }
  }

  renderSymbolInput(symbols: string[]){
    const { targetSymbol } = this.props
    return symbols.length === 1 ? (
      <InputGroup
        large={true}
        onChange={(e: React.FormEventHandler<HTMLElement>)=>{
          const { value } = e.target
          if(op !== null){
            this.props.onTargetSymbolChange(value)
          }
        }}
        placeholder="url here..."
        value={targetSymbol}
        />
    ) : null
  }

  renderSymbolSelect(){
    const { targetSymbol, uniqueSymbolsInCsv } = this.props
    const symbolOptions = uniqueSymbolsInCsv.map( symbol => ({
      value: symbol, label: symbol
    }))
    return (
      <MenuSelect
        menuLabel={targetSymbol || 'No symbol found'}
        menuRightIcon="caret-down"
        options={symbolOptions}
        onSelect={this.props.onTargetSymbolChange}
        />
    )
  }

  renderSideSelect(){
    const { targetSide } = this.props
    const { sideOptions } = this.state
    return (
      <MenuSelect
        menuLabel={targetSide}
        menuRightIcon="caret-down"
        options={sideOptions}
        onSelect={this.props.onTargetSideChange}
        />
    )
  }

  renderTimezoneSelect(){
    const { targetTimezone } = this.props
    const { timezoneOptions } = this.state
    return (
      <MenuSelect
        menuLabel={targetTimezone}
        menuIcon="globe"
        menuRightIcon="caret-down"
        options={timezoneOptions}
        onSelect={this.props.onTargetTimezoneChange}
        />
    )
  }

  renderDatePicker(){
    const { dateYearOptions, dateMonthOptions, dateDayOptions } = this.state
    const { targetDate, onTargetDateChange } = this.props
    const [ yyyy, mm, dd ] = targetDate.split('-')

    return (
      <>
        <MenuSelect
          menuLabel={yyyy}
          menuRightIcon="caret-down"
          options={dateYearOptions}
          onSelect={(val) => onTargetDateChange(`${val}-${mm}-${dd}`)}
          />
        <MenuSelect
          menuLabel={mm}
          menuRightIcon="caret-down"
          options={dateMonthOptions}
          onSelect={(val) => onTargetDateChange(`${yyyy}-${val}-${dd}`)}
          />
        <MenuSelect
          menuLabel={dd}
          menuRightIcon="caret-down"
          options={dateDayOptions}
          onSelect={(val) => onTargetDateChange(`${yyyy}-${mm}-${val}`)}
          />
      </>
    );
  }

  render() {
    return (
      <ButtonGroup fill={true}>
        {this.renderSymbolSelect()}
        {this.renderSideSelect()}
        {this.renderDatePicker()}
        {this.renderTimezoneSelect()}
      </ButtonGroup>
    )
  }
}

export default CsvHighLvlParamsSelect



