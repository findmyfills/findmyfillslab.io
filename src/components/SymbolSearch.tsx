import React from 'react';
import { connect } from 'react-redux';
import { FilterOptionsHandler } from 'react-select';
import createFilterOptions from 'react-select-fast-filter-options';
import Select from 'react-virtualized-select';
import { Dispatch } from 'redux';
import { createFetchSymbolRefData, createSelectSymbolAction, createSetSelectedFmfCsvData, FetchSymbolRefDataAction, SelectSymbolAction, SetSelectedFmfCsvData } from '../actions';
import config from '../app.config';
import { IApplicationState, selectSymbolOptn } from '../reducers';
import { IOption } from '../types';
import RankedSearchIndex from '../util/ranked-search-index';


type Props = {
} & StoreProps & DispatchProps

type State = {
  filterOptions: FilterOptionsHandler | undefined
}

class SymbolSearch extends React.PureComponent<Props, State> {

  constructor(props: Props) {
    super(props)
    this.state = {
      filterOptions: undefined
    }
  }

  componentWillReceiveProps(nextProps: Props, nextState: State){
    const { symbolOptions } = nextProps
    const { filterOptions } = nextState
    if (
      symbolOptions !== this.props.symbolOptions
      && symbolOptions.length > 0
      && filterOptions == undefined
    ){
      this.initFilterOptions(symbolOptions)
    }
  }

  initFilterOptions(symbolOptions: IOption[]){
    this.setState({
      filterOptions: createFilterOptions({
        options: symbolOptions,
        indexes: ['value', 'label'],
        searchIndex: new RankedSearchIndex('value')
      })
    })
    this.props.selectSymbol(config.DEFAULT_SYMBOL_ON_LOAD) // Select default symbol on app mount
  }

  componentDidMount() {
    this.props.fetchRefSymbolData()
  }

  handleSymbolOptionSelected = (option: IOption) => {
    const { selectedSymbolOption } = this.props
    if(
      option !== undefined && option !== null
      && selectedSymbolOption !== undefined
      && option.value !== selectedSymbolOption.value
    ){
      this.props.selectSymbol(option.value)
      this.props.setSelectedFmfCsvData('') // clear any rendered FMF fills data
    }
  }

  render() {
    const { symbolOptions, selectedSymbolOption } = this.props
    const { filterOptions } = this.state

    return (
      <div id="symbol-search-container">
        <Select
          filterOptions={filterOptions}
          options={symbolOptions}
          optionHeight={36}
          onChange={this.handleSymbolOptionSelected}
          autoBlur={true}
          openOnClick={false}
          placeholder="Search for a symbol..."
          clearable={false}
          value={selectedSymbolOption}
          inputProps={{
            autoComplete: 'off',
            autoCorrect: 'off',
            autoCapitalize: 'off',
            spellCheck: 'false'
          }}
        />
      </div>
    );
  }
}

type StoreProps = {
  symbolOptions: IOption[]
  selectedSymbolKey: string
  selectedSymbolOption: IOption | undefined
}

const mapStateToProps = ({ appControls }: IApplicationState): StoreProps => ({
  symbolOptions: appControls.symbolOptions,
  selectedSymbolKey: appControls.selectedSymbolKey,
  selectedSymbolOption: selectSymbolOptn(appControls.symbolOptions ,appControls.selectedSymbolKey),
})

type DispatchProps = {
  selectSymbol: SelectSymbolAction
  setSelectedFmfCsvData: SetSelectedFmfCsvData
  fetchRefSymbolData: FetchSymbolRefDataAction
}

const mapDispatchToProps = (dispatch: Dispatch): DispatchProps => ({
  selectSymbol: createSelectSymbolAction(dispatch),
  setSelectedFmfCsvData: createSetSelectedFmfCsvData(dispatch),
  fetchRefSymbolData: createFetchSymbolRefData(dispatch)
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SymbolSearch);


