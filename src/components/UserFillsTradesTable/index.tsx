import { Button, ButtonGroup } from '@blueprintjs/core';
import { IRegion, SelectionModes } from '@blueprintjs/table';
import moment from 'moment';
import React from 'react';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { createDrillDownToViewAction, IDrillDownToViewAction } from '../../actions';
import { Colors } from '../../constants';
import { getCsvFillsUnixTs } from '../../models';
import { PayloadCsvFill } from '../../records';
import { IApplicationState } from '../../reducers';
import { INavBeam } from '../../schemas/navigator';
import { FEGrainType } from '../../types';
import { deDupArray, getIndexesBtw } from '../../util';
import TableInteractive, { UserColSchema } from '../TableInteractive';
import { calcStateParams, Trade } from './util';


type Props = {
  timeZoneString: string
  userFills: PayloadCsvFill[] | undefined
  selectedFmfCsvKey: string
  containerW: number
} & StoreProps & DispatchProps

type State = {
  selectedRowIndexes: number[]
  columnSchemas: UserColSchema[]
  trades: Trade[]
  columnWidths: number[]
  csvEncodedUri: string
}

class UserFillsTradesTable extends React.Component<Props, State> {

  container: HTMLDivElement | null

  constructor(props: Props) {
    super(props)
    this.container = null
    this.state = {
      selectedRowIndexes: [],
      ...calcStateParams(props.userFills)
    }
  }

  componentWillReceiveProps(nextProps: Props){
    const { userFills, timeZoneString } = nextProps
    if (
      userFills !== this.props.userFills
      || timeZoneString !== this.props.timeZoneString
    ){
      this.setState(calcStateParams(userFills))
    }
  }

  handleTableSelection = (selectedRegions: IRegion[]) => {
    const { trades } = this.state
    const maxRowIndex = trades.length
    const rowIndexes: number[] = []
    // console.log('selectedRegions', selectedRegions)
    selectedRegions.forEach((region)=>{
      const { rows } = region
      if(rows !== undefined){ // First and last row indices in the region, inclusive and zero-indexed.
        if(rows === null){ // If null, then all rows are understood to be included in the region.
          rowIndexes.push(...getIndexesBtw(0, maxRowIndex))
        } else {
          const [ rowFirst, rowLast ] = rows
          rowIndexes.push(...getIndexesBtw(rowFirst, rowLast))
        }
      }
    })
    this.setState({
      selectedRowIndexes: deDupArray(rowIndexes).sort((a, b) => a - b),
    })
  }

  handleZoomToAllTrades = () => {
    const { trades } = this.state
    this.zoomToTrades(
      trades[0],
      trades[trades.length-1],
    )
  }

  handleZoomToSelectedTrades = () => {
    const { selectedRowIndexes, trades } = this.state
    const selectedTrades = selectedRowIndexes.map( index => trades[index])
    this.zoomToTrades(
      selectedTrades[0],
      selectedTrades[selectedTrades.length-1],
    )
  }

  zoomToTrades = (firstTrade: Trade, lastTrade: Trade) => {
    const { navBeams, activeBeamKeys, selectedDayStartUtc } = this.props

    const tStartActual = moment(getCsvFillsUnixTs(firstTrade)).valueOf()
    const tEndActual = moment(getCsvFillsUnixTs(lastTrade)).valueOf() // NOTE: trade.ts is the original timestamp retained in formatFillsTradesMetrics
    const tSizeActual = tEndActual - tStartActual

    const TRADES_RANGE_PADDING_DEFAULY =  1000 // ms
    const PCT_TRADES_RANGE_PADDING =  0.25 // a pct of the duration between 1st and last selected trades

    let tStartPadded: number,
        tEndPadded: number
    if(tSizeActual === 0){ // duration of zero, likely the result of a single selected trade...
      tStartPadded = tStartActual - TRADES_RANGE_PADDING_DEFAULY
      tEndPadded = tEndActual + TRADES_RANGE_PADDING_DEFAULY
    } else { // a non-zero duration based on two or more selected trades...
      const tPad = tSizeActual * PCT_TRADES_RANGE_PADDING
      tStartPadded = tStartActual - tPad
      tEndPadded = tEndActual + tPad
    }
    // console.log('--------------handleZoomToTrades-------------')
    // console.log('tStartPadded', moment(tStartPadded).format('HH:mm:ss'))
    // console.log('tEndPadded', moment(tEndPadded).format('HH:mm:ss'))
    // console.log('tSizeActual', tSizeActual)
    // console.log('---------------------------------------------')

    this.props.drillDownToView(
      navBeams,
      activeBeamKeys,
      tStartPadded - selectedDayStartUtc,
      tEndPadded - selectedDayStartUtc,
    )
  }

  renderTableActionButtons(){
    const { selectedRowIndexes, csvEncodedUri, trades } = this.state
    const { selectedFmfCsvKey } = this.props
    const numTradesTotal = trades.length
    const numTradesSelected = selectedRowIndexes.length
    const downloadCsvName = selectedFmfCsvKey.split('.')[0] + 'matched.csv'

    return (
      <ButtonGroup fill={false} vertical={true}>
        <Button
          text="zoom to all fills"
          intent="primary"
          minimal={false}
          disabled={numTradesTotal === 0}
          onClick={this.handleZoomToAllTrades}
          />
        <Button
          text="zoom to selected"
          intent="primary"
          minimal={false}
          disabled={numTradesSelected === 0}
          onClick={this.handleZoomToSelectedTrades}
          />
        <a className="bp3-button"
          href={csvEncodedUri}
          download={downloadCsvName}
          >
          download as csv
        </a>
      </ButtonGroup>
    )
  }

  render(){
    const { containerW } = this.props
    const { trades, columnSchemas, columnWidths } = this.state
    const TABLE_WIDGET_W = 150

    return (
      <div id="container-user-csv-fills-table"
        ref={el => this.container = el}
        >
        <style jsx>{`
          #container-user-csv-fills-table{
            display: flex;
            background: ${Colors.BP3_TABLE}
          }
          .table-widgets-container {
            width: ${TABLE_WIDGET_W}px;
            padding: 8px;
          }
          .table-fills-container {
            width: ${containerW - TABLE_WIDGET_W}px;
          }
        `}</style>
        <div className="table-widgets-container">
          {this.renderTableActionButtons()}
        </div>
        <div className="table-fills-container">
          <TableInteractive
            columnSchemas={columnSchemas}
            rowsData={trades}
            bp3Options={{
              numRows: 4, // number of visible rows
              defaultColumnWidth: 110,
              columnWidths,
              selectionModes: SelectionModes.ROWS_AND_CELLS,
              onSelection: this.handleTableSelection,
            }}
            />
        </div>
      </div>
    )
  }
}


type StoreProps = {
  navBeams: INavBeam[]
  activeBeamKeys: (FEGrainType)[]
  selectedDayStartUtc: number
}

const mapStateToProps = ({ navigator }: IApplicationState) => ({
  navBeams: navigator.navBeams,
  activeBeamKeys: navigator.activeBeamKeys,
  selectedDayStartUtc:  navigator.selectedDayStartUtc,
})

type DispatchProps = {
  drillDownToView: IDrillDownToViewAction
}

const mapDispatchToProps = (dispatch: Dispatch): DispatchProps => ({
  drillDownToView: createDrillDownToViewAction(dispatch)
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UserFillsTradesTable);

