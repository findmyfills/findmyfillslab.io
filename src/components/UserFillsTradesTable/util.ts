import { getFormatterForCsvFillKey } from '../../formatters';
import { appendTimestampKey, formatCsvFillsMetrics } from '../../models';
import { findColumnWForCsvFillLabel, findDataTypeForCsvFillKey, findLabelForCsvFillKey, PayloadCsvFill } from '../../records';
import { TableAlignType, UserColSchema } from '../TableInteractive';

export type Trade = Record<string, string>

export function createColumnsSchema(csvFill: PayloadCsvFill | undefined): UserColSchema[]{
  if(csvFill === undefined) return []
  const fillsKeys = Object.keys(csvFill) as (keyof PayloadCsvFill)[]
  return fillsKeys.map((key)=>({
    label: findLabelForCsvFillKey(key),
    key: findLabelForCsvFillKey(key),
    valType: findDataTypeForCsvFillKey(key),
    alignment: TableAlignType.RIGHT,
    formatter: getFormatterForCsvFillKey(key)
  }))
}

export function createCsvEncodedUri(trades: Trade[]){
  let csvEncodedUri = ''
  if(trades[0] !== undefined){
    const headerRow = Object.keys(trades[0])
    const bodyRows = trades.map((trade)=>{
      return headerRow.map( headerKey => trade[headerKey])
    })

    const csvRows = [ headerRow ].concat(bodyRows)
    const csvString = "data:text/csv;charset=utf-8," + csvRows.map(row => row.join(",")).join("\n");
    csvEncodedUri = encodeURI(csvString);
  }
  return csvEncodedUri
}

export function calcStateParams(userFills: PayloadCsvFill[] | undefined){
  let trades: Trade[] = []
  let columnSchemas: UserColSchema[] = []
  if(Array.isArray(userFills) && userFills.length > 0){
    const fillsWithTimestamps = appendTimestampKey(userFills)
    trades = fillsWithTimestamps.map(fill => formatCsvFillsMetrics(fill))
    columnSchemas = createColumnsSchema(fillsWithTimestamps[0])
  }

  return {
    trades,
    csvEncodedUri: createCsvEncodedUri(trades),
    columnSchemas,
    columnWidths: columnSchemas.map(({ label }) => findColumnWForCsvFillLabel(label))
  }
}