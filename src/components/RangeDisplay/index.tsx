import { Button, ButtonGroup, Position } from '@blueprintjs/core';
import { TimePicker, TimePrecision } from '@blueprintjs/datetime';
import { TimezoneDisplayFormat, TimezonePicker } from "@blueprintjs/timezone";
import moment from 'moment';
import React from 'react';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { createDrillDownToViewAction, createSetTimeZoneAction, IDrillDownToViewAction, SetTimeZoneAction } from '../../actions';
import { getDurationFormater, getSizeFormatterFromFeGrainType } from '../../formatters';
import { IApplicationState, selectBeamsByKeys } from '../../reducers';
import { INavBeam } from '../../schemas/navigator';
import { FEGrainType } from '../../types';
import { getLast } from '../../util';
import { normalizeTimePickerDateForAppTz, TimeStatus, utcToTimePickerdDate, validateTimeAndGetStatus } from './util';


type Props = {
  className?: string
} & StoreProps & DispatchProps

type State = {
  isInTimeEditMode: boolean
  editedTimeStart: Date
  editedTimeEnd: Date
  editedTimeStatus: TimeStatus
}

const DEFAULT_DIALOG_STATE = {
  isInTimeEditMode: false,
  editedTimeStart: new Date(),
  editedTimeEnd: new Date(),
  editedTimeStatus: TimeStatus.VALID,
}


class RangeDisplay extends React.Component<Props, State>{

  constructor(props: Props) {
    super(props)
    this.state = Object.assign({}, DEFAULT_DIALOG_STATE, {
      // additonal states...
    })
  }

  componentDidMount(){
    const { timeZoneString } = this.props
    this.checkAndEnforceEasternTimezone(timeZoneString)
    // this.checkAndEnforceClientTimezone(timeZoneString)
  }

  componentWillReceiveProps(nextProps: Props) {
    const { xAxisTimeMin, xAxisTimeMax } = nextProps
    const { isInTimeEditMode } = this.state

      if(isInTimeEditMode && (
        xAxisTimeMin !== this.props.xAxisTimeMin
        || xAxisTimeMax !== this.props.xAxisTimeMax
      )){
        this.setState({ // update current time in edit if time changed is trigger by other parts of the app
          editedTimeStart: new Date(xAxisTimeMin),
          editedTimeEnd: new Date(xAxisTimeMax),
        })
      }
  }

  checkAndEnforceEasternTimezone(timeZoneString: string){
    const ET = 'America/New_York'
    if(timeZoneString !== ET ){ // override with client timezone
      this.handleTimezoneChange(ET)
    }
  }

  checkAndEnforceClientTimezone(timeZoneString: string){
    const clientTzStr = moment.tz.guess()
    if(timeZoneString !== clientTzStr){ // override with client timezone
      this.handleTimezoneChange(clientTzStr)
    }
  }

  closeDialogWindow = () => {
    this.setState(
      Object.assign({}, this.state, DEFAULT_DIALOG_STATE)
    )
  }

  editTime = () => {
    const { xAxisTimeMin, xAxisTimeMax } = this.props
    this.setState({
      isInTimeEditMode: true,
      editedTimeStart: new Date(xAxisTimeMin),
      editedTimeEnd: new Date(xAxisTimeMax),
    })
  }

  validateTime = () => {
    const { minTimeAllowed, maxTimeAllowed, timeZoneString } = this.props
    const { editedTimeStart, editedTimeEnd } = this.state
    this.setState({
      editedTimeStatus: validateTimeAndGetStatus(
        editedTimeStart,
        editedTimeEnd,
        minTimeAllowed,
        maxTimeAllowed,
        timeZoneString,
      )
    })
  }

  handleEditTimeStart = (timePickerDate: Date) => {
    this.setState({
      editedTimeStart: normalizeTimePickerDateForAppTz(timePickerDate),
    }, this.validateTime)
  }

  handleEditTimeEnd = (timePickerDate: Date) => {
    this.setState({
      editedTimeEnd: normalizeTimePickerDateForAppTz(timePickerDate),
    }, this.validateTime)
  }

  cancelEditedTime = () => {
    this.setState(Object.assign({}, this.state, DEFAULT_DIALOG_STATE))
  }

  resetEditedTime = () => {
    this.cancelEditedTime()
    this.editTime()
  }

  confirmEditedTime = () => {
    const { navBeams, activeTimeGrainTypes, selectedDayStartUtc } = this.props
    const { editedTimeStart, editedTimeEnd } = this.state
    if(
      editedTimeStart !== undefined
      && editedTimeEnd !== undefined
      ) {
      // console.log('---------------')
      // console.log(editedTimeStart.valueOf(), editedTimeEnd.valueOf())
      this.props.drillDownToView(
        navBeams,
        activeTimeGrainTypes,
        editedTimeStart.valueOf() - selectedDayStartUtc,
        editedTimeEnd.valueOf() - selectedDayStartUtc,
      )
    }
    this.closeDialogWindow()
  }

  handleTimezoneChange = (timezone: string) => {
    this.props.setTimeZone(timezone)
  }


  renderTimeEditControls(){
    const { isInTimeEditMode, editedTimeStatus } = this.state
    return isInTimeEditMode ? (
      <ButtonGroup vertical={true}>
        <Button
          minimal={false}
          icon="reset"
          onClick={this.resetEditedTime}
          />
        <Button
          minimal={false}
          icon="tick"
          disabled={editedTimeStatus !== TimeStatus.VALID}
          onClick={this.confirmEditedTime}
          />
        <Button
          minimal={false}
          icon="cross"
          onClick={this.cancelEditedTime}
          />
      </ButtonGroup>
    ) : (
      <Button
        minimal={false}
        icon="edit"
        onClick={this.editTime}
        />
    )
  }

  render(){
    const { currentTimeGrainType, topLvlRiderStartAbs, topLvlRiderEndAbs, xAxisTimeMin, xAxisTimeMax, timeZoneString, className } = this.props
    const { isInTimeEditMode, editedTimeStart, editedTimeEnd, editedTimeStatus } = this.state
    // Create rider size info text...
    const riderSizeInfoFormatter = getSizeFormatterFromFeGrainType(currentTimeGrainType)
    const riderSize = topLvlRiderEndAbs - topLvlRiderStartAbs
    const currentViewSizeText = riderSizeInfoFormatter(riderSize)

    let editedViewSizeText = ''
    if(isInTimeEditMode){
      const editedViewSize = editedTimeEnd.valueOf() - editedTimeStart.valueOf()
      const editedViewSizeFormatter = getDurationFormater(editedViewSize)
      editedViewSizeText = editedViewSizeFormatter(editedViewSize)
    }

    // Calculate start time validation status for styling...
    let isEditTimeStartValid = true
    let isEditTimeEndValid = true
    if(editedTimeStatus === TimeStatus.ERROR_START_END){
      isEditTimeStartValid = false
      isEditTimeEndValid = false
    } else if(editedTimeStatus === TimeStatus.ERROR_START_ONLY){
      isEditTimeStartValid = false
    } else if(editedTimeStatus === TimeStatus.ERROR_END_ONLY){
      isEditTimeEndValid = false
    }


    return (
      <>
        <Button
          className="view-size-display"
          minimal={false}
          disabled={true}
          text={`view: ${isInTimeEditMode ? editedViewSizeText : currentViewSizeText}`}
          />
        <TimezonePicker
          placeholder="timezone"
          value={timeZoneString}
          onChange={this.handleTimezoneChange}
          valueDisplayFormat={TimezoneDisplayFormat.OFFSET}
          popoverProps={{
            position: Position.RIGHT_TOP,
            minimal: true,
          }}
          >
          <Button
            className="timezone-picker-button"
            icon="globe"
            rightIcon="caret-down"
            text={moment.tz(timeZoneString).format('z (Z)')}
            />
        </TimezonePicker>

        <div className={`time-picker-container ${!isEditTimeStartValid ? 'invalid-time' : ''}`}>
          <TimePicker
            precision={TimePrecision.MILLISECOND}
            selectAllOnFocus={true}
            showArrowButtons={isInTimeEditMode}
            disabled={!isInTimeEditMode}
            onChange={this.handleEditTimeStart}
            value={
              isInTimeEditMode ?
                utcToTimePickerdDate(editedTimeStart.valueOf(), timeZoneString)
                : utcToTimePickerdDate(xAxisTimeMin, timeZoneString)
            }
            />
        </div>
        {this.renderTimeEditControls()}
        <div className={`time-picker-container ${!isEditTimeEndValid ? 'invalid-time' : ''}`}>
          <TimePicker
            precision={TimePrecision.MILLISECOND}
            selectAllOnFocus={true}
            showArrowButtons={isInTimeEditMode}
            disabled={!isInTimeEditMode}
            onChange={this.handleEditTimeEnd}
            value={ // TimePicker always interpret unix in client timezone
              isInTimeEditMode ?
                utcToTimePickerdDate(editedTimeEnd.valueOf(), timeZoneString)
                  : utcToTimePickerdDate(xAxisTimeMax, timeZoneString)
            }
            />
        </div>
      </>
    )
  }
}


type StoreProps = {
  navBeams: INavBeam[]
  currentTimeGrainType: FEGrainType
  activeTimeGrainTypes: (FEGrainType)[]
  selectedSymbolKey: string
  selectedDayStartUtc: number
  minTimeAllowed: number
  maxTimeAllowed: number
  topLvlRiderStartAbs: number
  topLvlRiderEndAbs: number
  xAxisTimeMin: number
  xAxisTimeMax: number
  timeZoneString: string
  timeZoneOffsetMin: number
}

const mapStateToProps = ({ navigator, appControls }: IApplicationState): StoreProps => {
  const dayStart = navigator.selectedDayStartUtc
  const activeTimeGrains = selectBeamsByKeys(navigator.navBeams, navigator.activeBeamKeys)// NOTE: multi-beam navigator interface carries time granularity information!
  const lowestGrain = getLast(activeTimeGrains)

  return {
    navBeams: navigator.navBeams,
    activeTimeGrainTypes: navigator.activeBeamKeys,
    selectedDayStartUtc: dayStart,
    minTimeAllowed: dayStart + lowestGrain.beamStart,
    maxTimeAllowed: dayStart + lowestGrain.beamStart + lowestGrain.beamSize,
    selectedSymbolKey: appControls.selectedSymbolKey,
    currentTimeGrainType: navigator.topVisibleBeamKey,
    topLvlRiderStartAbs: navigator.topLvlRiderStartAbs,
    topLvlRiderEndAbs: navigator.topLvlRiderEndAbs,
    xAxisTimeMin: dayStart + navigator.topLvlRiderStartAbs,
    xAxisTimeMax: dayStart + navigator.topLvlRiderEndAbs,
    timeZoneString: appControls.timeZoneString,
    timeZoneOffsetMin: appControls.timeZoneOffsetMin,
  }
}

type DispatchProps = {
  drillDownToView: IDrillDownToViewAction
  setTimeZone: SetTimeZoneAction
}

const mapDispatchToProps = (dispatch: Dispatch): DispatchProps => ({
  drillDownToView: createDrillDownToViewAction(dispatch),
  setTimeZone: createSetTimeZoneAction(dispatch),
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RangeDisplay);