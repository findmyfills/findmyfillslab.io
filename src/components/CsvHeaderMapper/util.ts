import { IHeaderMapping } from '.';
import { AUTO_GENERATED_HEADER_MAPPINGS, MANDITORY_HEADER_MAPPINGS } from '../../models';
import { CsvMapperHeaders, findAllowedHeaderKeyForUserCsvColKey } from '../../records';
import { CSV_HEADER_MAPPING_OPTNS } from '../../schemas/header-mapping';
import { CsvConfigStatusTypes, CsvSideType, IOption } from '../../types';
import { isNumber } from '../../util';
import { CsvParams } from '../CsvFileTable';


export function getEnabledHeaderMappings(headerMappings: IHeaderMapping[]){
  return headerMappings.filter(({ isEnabled }) => isEnabled)
}


export function getDisabledHeaderMappings(headerMappings: IHeaderMapping[]){
  return headerMappings.filter(({ isEnabled }) => !isEnabled)
}


export function initMappingsFromHeaderKeys(headerKeys: string[]){
  return headerKeys.map((key, i) => createHeaderSchema(key, i))
}


export function updateHeaderMapping(headerMappings: IHeaderMapping[], newMapping: IHeaderMapping){
  return headerMappings.map((mapping, i) => { // swap in updated schema...
    return i === newMapping.indexInSet ? newMapping : mapping
  })
}


export function createHeaderMap(validatedHeaderMappings: IHeaderMapping[]): Record<string, string>{
  const enabledHeaderMappings = getEnabledHeaderMappings(validatedHeaderMappings)
  return enabledHeaderMappings.reduce((prev: Record<string, string>, headerMapping) => {
    const { headerOptnMappedTo, originalKey } = headerMapping
    prev[originalKey] = String(headerOptnMappedTo.value)
    return prev
  }, {})
}


export function normalizeCsv(fileInfo: CsvParams, mapToNormalizedHeader: Record<string, string>){
  console.log('mapToNormalizedHeader', mapToNormalizedHeader)
  const { originalCsv, originalHeaderKeys, headersToGenerate } = fileInfo

  // 1. Create list of normalized headers and collect corresponding column index...
  const columnIndexesToKeep: number[] = []
  const normalizedHeaderKeys = originalHeaderKeys.reduce((prev: string[], originalHeaderKey, indexInSet) => {

  const normalizedHeaderKey = mapToNormalizedHeader[originalHeaderKey]
    if (normalizedHeaderKey !== undefined) {
      prev.push(normalizedHeaderKey)
      columnIndexesToKeep.push(indexInSet) // register index
    }
    return prev
  }, [])
  // console.log('columnIndexesToKeep', columnIndexesToKeep)

  // 2. User collected whitelist of column indexes to filter out valid csv data for every row...
  const normalizedCsv = originalCsv.reduce((prev: string[][], csvRow, i) => {
    if (i === 0) { // header row
      prev.push(normalizedHeaderKeys)
    } else {
      prev.push(
        columnIndexesToKeep.map(colindex => csvRow[colindex]) // keep only enabled headers
      )
    }
    return prev
  }, [])

  // 3. Generate non-manditory headers required by backend and fill in column value with blanks...
  headersToGenerate.forEach((headerKeyToAdd)=>{
    const [ headerRow, ...bodyRows ] = normalizedCsv
    headerRow.push(headerKeyToAdd)
    bodyRows.forEach(row => row.push(''))
  })
  return normalizedCsv
}


export function isMappingDefined(headerOptnMappedTo: IOption){
  return headerOptnMappedTo.value !== CsvMapperHeaders.UNDEFINED
}


export function normalizeUserHeaderStr(str: string){
  return str.replace(/[^A-Za-z]+/g, '').toUpperCase() // keep only alphabets
}

export function attemptHeaderNormalization(str: string): IOption {
  // console.log('$c----attemptHeaderNormalization-----','color:green')
  const normalizedString = normalizeUserHeaderStr(str)
  // console.log(str, '-->' ,normalizedString )
  const normalizedHeaderKey = findAllowedHeaderKeyForUserCsvColKey(normalizedString)

  let headerOptnMappedTo = getHeaderMappingOptnByKey(CsvMapperHeaders.UNDEFINED)// defaults to 'Undefined'
  if(normalizedHeaderKey === undefined){
    // TODO: if not direct matches, try more advanced ways of guessing header type...
  } else {
    const matchedOptn = CSV_HEADER_MAPPING_OPTNS.find(({ value }) => value === normalizedHeaderKey)
    if (matchedOptn !== undefined){
      headerOptnMappedTo = matchedOptn
    }
  }
  return headerOptnMappedTo!
}


export function getHeaderMappingOptnByKey(headerKey: CsvMapperHeaders){
  return CSV_HEADER_MAPPING_OPTNS.find(({ value }) => value === headerKey)
}


export function createHeaderSchema(headerKey: string, indexInSet: number): IHeaderMapping {
  const headerOptnMappedTo = attemptHeaderNormalization(headerKey)
  return {
    originalKey: headerKey,
    indexInSet,
    headerOptnMappedTo,
    isEnabled: headerOptnMappedTo.value !== CsvMapperHeaders.UNDEFINED, // initially disabled all headers that failed to be mapped
  }
}


export function checkIfHasHeaderMapping(headerMappings: IHeaderMapping[], headerKey: CsvMapperHeaders){
  for (let i = 0; i < headerMappings.length; i++) {
    const { headerOptnMappedTo } = headerMappings[i]
    if (headerOptnMappedTo.value === headerKey) {
      return true
    }
  }
  return false
}


export function checkForMissingHeaderMappings(
  headerMappings: IHeaderMapping[],
  headerKeys: (CsvMapperHeaders)[] = []
){
  const missingHeaders = headerKeys.slice() // initially assume all headers missings
  headerMappings.forEach(({ headerOptnMappedTo }) => {
    const indexInRequiredHeaders = missingHeaders.findIndex(headerKey => headerKey === headerOptnMappedTo.value)
    if(indexInRequiredHeaders !== -1){ // If header not missing...
      missingHeaders.splice(indexInRequiredHeaders, 1) // remove found headers from missing list
    }
  })
  return missingHeaders
}


export function sampleCsvDateValue(csvBody: string[][], headerMappings: IHeaderMapping[]){
  const timeHeaderMapping = headerMappings.find(({ headerOptnMappedTo }) => headerOptnMappedTo.value === CsvMapperHeaders.TIME)
  let sampleDateValueInCsv: string | number | undefined
  if(
    timeHeaderMapping !== undefined
    && csvBody[0] !== undefined
  ){
    const columnVal = csvBody[0][timeHeaderMapping.indexInSet]
    if(isNumber(columnVal)){
      sampleDateValueInCsv = Number(columnVal)
    } else {
      sampleDateValueInCsv = columnVal
    }
  }
  return sampleDateValueInCsv
}


export function sampleCsvSideValue(csvBody: string[][], headerMappings: IHeaderMapping[]): CsvSideType {
  const timeHeaderMapping = headerMappings.find(({ headerOptnMappedTo }) => headerOptnMappedTo.value === CsvMapperHeaders.SIDE)
  let sampleSideValueInCsv: CsvSideType = CsvSideType.BUY // default value if no valid value found in csv
  if(
    timeHeaderMapping !== undefined
    && csvBody[0] !== undefined
  ){
    const columnVal = csvBody[0][timeHeaderMapping.indexInSet]
    switch(columnVal.toUpperCase()){
      case CsvSideType.BUY: sampleSideValueInCsv = CsvSideType.BUY;
      case CsvSideType.SELL: sampleSideValueInCsv = CsvSideType.SELL;
    }
  }
  return sampleSideValueInCsv
}


export function getUnqiueSymbolsInCsv(csvBody: string[][], headerMappings: IHeaderMapping[]){
  const symbolHeaderMapping = headerMappings.find(({ headerOptnMappedTo }) => headerOptnMappedTo.value === CsvMapperHeaders.SYMBOL)

  let uniqueSymbolsInCsv: string[] = []
  if(symbolHeaderMapping !== undefined){
    const { indexInSet } = symbolHeaderMapping
    csvBody.forEach(row => {
      const rowSymbolValue = row[indexInSet]
      if(isNaN(+rowSymbolValue)){ // if row value is string only
        const isSymbolAlreadyInList = uniqueSymbolsInCsv.findIndex(symbKey => symbKey === rowSymbolValue) !== -1
        if(!isSymbolAlreadyInList){
          uniqueSymbolsInCsv.push(rowSymbolValue)
        }
      }
    })
  }
  // console.log('uniqueSymbolsInCsv', uniqueSymbolsInCsv)
  return uniqueSymbolsInCsv
}


export function surveyForCsvParams(csvBody: string[][], headerMappings: IHeaderMapping[]){

  // if a mapping defined for the 'symbol' header, check parsed csv to make sure every value under 'symbol' column pertains to a single symbol...
  const enabledMappings = getEnabledHeaderMappings(headerMappings)
  const uniqueSymbolsInCsv = getUnqiueSymbolsInCsv(csvBody, enabledMappings)

  const missingHeaders = checkForMissingHeaderMappings(enabledMappings, MANDITORY_HEADER_MAPPINGS)
  const headersToGenerate = checkForMissingHeaderMappings(enabledMappings, AUTO_GENERATED_HEADER_MAPPINGS)
  const targetSymbol = uniqueSymbolsInCsv.length >= 1 ? uniqueSymbolsInCsv[0] : undefined // default select first symbol if available

  const sampleDateValueInCsv = sampleCsvDateValue(csvBody, enabledMappings)
  const targetSide = sampleCsvSideValue(csvBody, enabledMappings)

  return {
    missingHeaders,
    headersToGenerate,
    uniqueSymbolsInCsv,
    sampleDateValueInCsv,
    targetSide,
    targetSymbol,
  }
}


export function checkIfConfigsAreValid(configStatus: keyof typeof CsvConfigStatusTypes){
  return (
    configStatus === CsvConfigStatusTypes.ALL_CONFIGS_VALID
    || configStatus === CsvConfigStatusTypes.MULTIPLE_SYMBOLS_IN_CSV
  )
}


export function getCsvConfigStatus(csvInfo: CsvParams){
  const { uniqueSymbolsInCsv, headerMappings, missingHeaders, targetDate } = csvInfo
  const enabledHeaderMappings = getEnabledHeaderMappings(headerMappings)
  const csvHasNoSymbol = uniqueSymbolsInCsv.length === 0
  const csvHasMoreThanOneSymbol = uniqueSymbolsInCsv.length > 1
  const hasSymbolHeaderBeenMapped = checkIfHasHeaderMapping(enabledHeaderMappings, CsvMapperHeaders.SYMBOL)

  // NOTE: below ordering of conditionals is non-trivial, the most important checks are placed first...

  // -------------------
  // Check symbol mappings...
  if(hasSymbolHeaderBeenMapped && csvHasNoSymbol){
    return CsvConfigStatusTypes.INCORRECT_SYMBOL_HEADER_MAPPING
  }

  // -------------------
  // Check header mappings...
  if(
    enabledHeaderMappings.length === 0
    || missingHeaders.length !== 0
  ){
    return CsvConfigStatusTypes.INCOMPLETE_HEADER_MAPPINGS
  }
  if(checkIfHasHeaderMapping(enabledHeaderMappings, CsvMapperHeaders.UNDEFINED)){
    return CsvConfigStatusTypes.HAS_UNDEFINED_HEADER_MAPPINGS
  }

  // -------------------
  // Check if specified date (with regards to symbol fill data) is valid
  if(targetDate.split('-').find(val => isNaN(+val)) !== undefined){ // if any date string parts is a invalid number
    return CsvConfigStatusTypes.INVALID_DATE_SELECTION
  }

  // -------------------
  // below are considered valid configs
  if(hasSymbolHeaderBeenMapped && csvHasMoreThanOneSymbol){
    return CsvConfigStatusTypes.MULTIPLE_SYMBOLS_IN_CSV
  }

  return CsvConfigStatusTypes.ALL_CONFIGS_VALID
}
