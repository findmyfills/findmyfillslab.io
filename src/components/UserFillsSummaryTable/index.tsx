import React from 'react';
import { EMPTY_SUMMARY_METRICS, formatCsvSummaryMetrics, getAdditonalSummaryMetricsFromFills, PayloadCsvSummary } from '../../models';
import { PayloadCsvFill } from '../../records';


type Props = {
  csvSummary: PayloadCsvSummary| undefined
  userFills: PayloadCsvFill[] | undefined
}

type State = {
  summaryMetrics: Record<string, string>
}

class UserFillsSummaryTable extends React.Component<Props, State> {

  constructor(props: Props) {
    super(props);
    this.state = {
      summaryMetrics: EMPTY_SUMMARY_METRICS,
    }
  }

  componentWillReceiveProps(nextProps: Props){
    const { csvSummary, userFills} = nextProps
    if (
      csvSummary !== this.props.csvSummary
      || userFills!== this.props.userFills
    ){
      if(
        csvSummary !== undefined
        && userFills!== undefined
      ){
        this.setFormattedMetrics(csvSummary, userFills)

      } else {
        this.setState({
          summaryMetrics: EMPTY_SUMMARY_METRICS,
        })
      }
    }
  }

  setFormattedMetrics(csvSummary: PayloadCsvSummary, userFills: PayloadCsvFill[] ){
    const fillsSummary = formatCsvSummaryMetrics(csvSummary)
    const additionSummary = getAdditonalSummaryMetricsFromFills(userFills)
    this.setState({
      summaryMetrics: Object.assign(
        fillsSummary,
        additionSummary,
      )
    })
  }

  render(){
    const { summaryMetrics } = this.state

    return (
      <table className="bp3-html-table bp3-html-table-striped bp3-small">
        <style jsx>{`
          table { width: 100%; }
        `}</style>
        <tbody>
          {
            Object.keys(summaryMetrics).map((key)=> (
              <tr key={key}>
                <td>{key}</td>
                <td>{summaryMetrics[key]}</td>
              </tr>
            ))
          }
        </tbody>
      </table>
    )
  }
}


export default UserFillsSummaryTable


