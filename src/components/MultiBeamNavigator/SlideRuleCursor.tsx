import React from 'react';

type Props = {
  className?: string
  x: number
  y: number
  w: number
  h: number
  textStart: string
  textEnd: string
  fontColor: string
  backColor: string
}

const SlideRuleCursor = ({
  className = '',
  x, y, w, h,
  textStart,
  textEnd,
  fontColor,
  backColor,
}: Props) => (
  <g className={className}>
    <style jsx>{`
      .cursor-anno-text {
        font-size: 13px;
      }
    `}</style>

    <g className="cursor-annotation-right">
      <rect
        className="cursor-anno-background"
        x={x + w - 60/2}
        y={y}
        width={'60px'}
        height={h}
        fill={backColor}
        />
      <text
        className="cursor-anno-text"
        x={x + w}
        y={y + 2}
        alignmentBaseline="hanging"
        textAnchor="middle"
        fill={fontColor}
        >
        {textEnd}
      </text>
    </g>
    <g className="cursor-annotation-left">
      <rect
        className="cursor-anno-background"
        x={x - 60/2}
        y={y}
        width={'60px'}
        height={h}
        fill={backColor}
        />
      <text
        className="cursor-anno-text"
        x={x}
        y={y + 2}
        alignmentBaseline="hanging"
        textAnchor="middle"
        fill={fontColor}
        >
        {textStart}
      </text>
    </g>
  </g>
)

export default SlideRuleCursor

