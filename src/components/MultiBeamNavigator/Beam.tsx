import moment from 'moment';
import React from 'react';
import { Colors } from '../../constants';
import { findBEGrainTypeForFEGrainType } from '../../records';
import { INavLayout } from '../../reducers';
import { FEGrainType, HcDataFormat } from '../../types';
import PreviewGraph from './PreviewGraph';
import Rider from './Rider';
import SlideRule from './SlideRule';
import SlideRuleCursor from './SlideRuleCursor';
import { convertPxDeltaToValDelta } from './util';
import { number } from 'prop-types';



const MIN_BEAM_BAR_W = 2


const DEFAULT_MOUSE_DOWN_STATES: MouseState = {
  isMouseDown: false,
  mdBeamComponent: undefined,
  mouseDownX: undefined,
  mdBeamStartAbs: undefined,
  mdBeamSize: undefined,
  mdRiderStart: undefined,
  mdRiderEnd: undefined,
  mdRiderSize: undefined,
}

function capBarMinW(barW: number) {
  return Math.max(barW, MIN_BEAM_BAR_W)
}


type Props = {
  beamColor: string
  riderColor: string
  childBeamRiderColor: string

  isTopVisibleBeam: boolean

  onRiderMoveEnd: (beamKey: FEGrainType, isTopVisibleBeam: boolean) => void
  onRiderBarMove: (beamKey: FEGrainType, riderStart: number, riderEnd: number) => void
  onRiderHandleMove: (beamKey: FEGrainType, riderStart: number, riderEnd: number) => void

  beamKey: FEGrainType
  beamSize: number
  beamStart: number
  timeStartAbs: number
  tickIntervalSize: number

  riderSize: number
  riderStart: number
  childBeamRiderSize: number
  childBeamRiderStart: number

  // withPreviewData...
  previewData: any[] | undefined
  previewDataFormat: HcDataFormat | undefined

} & INavLayout


type State = {

} & MouseState

type MouseState = {
  isMouseDown: boolean
  mdBeamComponent: string | undefined
  mouseDownX: number | undefined
  mdBeamStartAbs: number | undefined
  mdBeamSize: number | undefined
  mdRiderStart: number | undefined
  mdRiderEnd: number | undefined
  mdRiderSize: number | undefined
}




class NavigatorBeam extends React.Component<Props, State>{

  private debounce: boolean

  constructor(props: Props) {
    super(props)
    this.debounce = false
    this.state = Object.assign({}, DEFAULT_MOUSE_DOWN_STATES, {
      // additional states...
    })
  }

  handleMdOnRiderBar = (e: React.MouseEvent<SVGElement>): void => {
    e.stopPropagation()
    this.setState({
      isMouseDown: true,
      mdBeamComponent: 'rider-bar',
      mouseDownX: e.clientX,
      // snapshot state on mousedown before moving
      mdBeamStartAbs: this.props.beamStart,
      mdBeamSize: this.props.beamSize,
      mdRiderStart: this.props.riderStart,
      mdRiderEnd: this.props.riderStart + this.props.riderSize,
      mdRiderSize: this.props.riderSize,
    })
  }

  handleMdOnRiderHandle = (leftRight: string, e: React.MouseEvent<SVGElement>): void => {
    e.stopPropagation()
    this.setState({
      isMouseDown: true,
      mdBeamComponent: `rider-handle-${leftRight}`,
      mouseDownX: e.clientX,
      mdBeamStartAbs: this.props.beamStart,
      mdBeamSize: this.props.beamSize,
      mdRiderStart: this.props.riderStart,
      mdRiderEnd: this.props.riderStart + this.props.riderSize,
      mdRiderSize: this.props.riderSize,
    })
  }

  handleMouseMove = (e: React.MouseEvent<SVGRectElement>) => {
    const { beamKey, beamBarW } = this.props
    const { isMouseDown, mdBeamComponent, mouseDownX, mdRiderStart, mdBeamSize, mdRiderSize, mdRiderEnd } = this.state

    // TODO: implement better debounce feature!!!
    if (this.debounce) {
      return // exit
    } else {
      this.debounce = true
      setTimeout(() => this.debounce = false, 66)
    }

    if (
      isMouseDown
      && mouseDownX !== undefined
      && mdBeamComponent !== undefined
      && mdRiderStart !== undefined
      && mdRiderEnd !== undefined
      && mdRiderSize !== undefined
      && mdBeamSize !== undefined
    ) { // only register mouse motion when something is clicked and being dragged...
      // Calculate mouse travel as percentage of
      const xTravelMouse = e.clientX - mouseDownX
      const xTravelInTimeSeriesValue = Math.floor(convertPxDeltaToValDelta(xTravelMouse, beamBarW, mdBeamSize))
      switch (mdBeamComponent) {
        case 'rider-bar': {
          const newRiderStart = mdRiderStart + xTravelInTimeSeriesValue
          const newRiderEnd = newRiderStart + mdRiderSize
          this.props.onRiderBarMove(
            beamKey,
            newRiderStart,
            newRiderEnd,
          )
        } break;
        case 'rider-handle-left': {
          const newRiderStart = mdRiderStart + xTravelInTimeSeriesValue
          this.props.onRiderHandleMove(
            beamKey,
            newRiderStart,
            mdRiderEnd,
          )
        } break;
        case 'rider-handle-right': {
          const newRiderEnd = mdRiderEnd + xTravelInTimeSeriesValue
          this.props.onRiderHandleMove(
            beamKey,
            mdRiderStart, // does not change
            newRiderEnd,
          )
        } break;
      }
    }
  }

  clearMouseDownState() {
    this.setState(DEFAULT_MOUSE_DOWN_STATES)
  }

  getPxVal(timeVal: number) {
    const { beamSize, beamBarW } = this.props
    return Math.floor(timeVal / beamSize * beamBarW)
  }

  handleMouseDownOrLeaveOnSvg = (e: React.MouseEvent<SVGSVGElement>) => {
    const { beamKey, isTopVisibleBeam } = this.props
    const { isMouseDown } = this.state
    if (isMouseDown) {
      this.props.onRiderMoveEnd(beamKey, isTopVisibleBeam)
      this.clearMouseDownState()
    }
  }

  render(){
    const { beamColor, riderColor, childBeamRiderColor, beamContainerW, beamContainerH, isTopVisibleBeam, riderStart, riderSize, childBeamRiderStart, childBeamRiderSize, riderBarHandleH, riderBarHandleW, beamBarX, beamBarW, beamBarH, beamBarY, beamSize, beamStart, tickIntervalSize, timeStartAbs, beamTicksH, beamTicksLabelH, beamKey, previewData, previewDataFormat } = this.props

    const { isMouseDown } = this.state

    const riderBarW = this.getPxVal(riderSize)
    const riderBarX = beamBarX + this.getPxVal(riderStart)

    const childBeamRiderBarW = this.getPxVal(childBeamRiderSize)
    const childBeamRiderBarX = beamBarX + this.getPxVal(childBeamRiderStart)

    const tBeamStartAbs = timeStartAbs + beamStart
    const tBeamEndAbs = timeStartAbs + beamStart + beamSize
    const tRiderStartAbs = timeStartAbs + beamStart + riderStart
    const tRiderEndAbs = timeStartAbs + beamStart + riderStart + riderSize

    return (
      <g id="svg-beam-group"
        onMouseLeave={this.handleMouseDownOrLeaveOnSvg}
        onMouseUp={this.handleMouseDownOrLeaveOnSvg}
        onMouseMove={this.handleMouseMove}
        >
        <g className="beam-and-rider-container">
          <rect
            className="nav-beam-bar"
            x={beamBarX}
            y={beamBarY}
            width={beamBarW}
            height={beamBarH}
            fill={Colors.NAV_BEAM_TRACK}
            />
          <PreviewGraph
            className="nav-beam-data-preview"
            grainType={findBEGrainTypeForFEGrainType(beamKey)}
            previewData={previewData}
            previewDataFormat={previewDataFormat || ''}
            previewTimeMin={tBeamStartAbs}
            previewTimeMax={tBeamEndAbs}
            x={beamBarX}
            y={beamBarY}
            w={beamBarW}
            h={beamBarH}
            />

          {
            !isTopVisibleBeam ? ( // for top most beam, no child beam rider bar is rendered, only its own rider bar, as there's no more beam above to toggle...
              <Rider
                className="child-beam-rider-bar"
                beamX={beamBarX}
                beamW={beamBarW}
                riderX={childBeamRiderBarX}
                riderW={capBarMinW(childBeamRiderBarW)}
                y={beamBarY}
                h={beamBarH}
                onRiderMouseDown={this.handleMdOnRiderBar}
                enableHandles={false}
                handleW={riderBarHandleW}
                handleH={riderBarHandleH}
                shouldRenderMask={true}
                maskColor={Colors.NAV_BEAM_TRACK_MASK}
                riderColor={Colors.NAV_CHILD_RIDER_TRACK}
                />
            ) : null
          }

          <Rider
            className="nav-beam-rider-bar"
            beamX={beamBarX}
            beamW={beamBarW}
            riderX={riderBarX}
            riderW={capBarMinW(riderBarW)}
            y={beamBarY}
            h={beamBarH}
            onRiderMouseDown={this.handleMdOnRiderBar}
            onHandleMouseDown={this.handleMdOnRiderHandle}
            enableHandles={isTopVisibleBeam}
            handleW={riderBarHandleW}
            handleH={riderBarHandleH}
            handleColor={Colors.NAV_RIDER_HANDLE}
            shouldRenderMask={true}
            shouldRenderRiderBorder={true}
            maskColor={Colors.NAV_BEAM_TRACK_MASK}
            riderColor={
              isMouseDown ? Colors.NAV_RIDER_BAR_MOUSEDOWN: Colors.NAV_RIDER_BAR
            }
            />

        </g>
        <g className='slider-rule-and-cursor-container'>
          <SlideRule
            className="slide-rule-container"
            x={beamBarX}
            w={beamBarW}
            y={beamBarH}
            ticksH={beamTicksH}
            ticksLabelH={beamTicksLabelH}
            tickIntervalSize={tickIntervalSize}
            color={Colors.NAV_RULER_TEXT}
            tStart={tBeamStartAbs}
            tSize={beamSize}
            />
          {
            !isMouseDown ? (
              <line
                className="slide-rule-cursor-bar"
                x1={riderBarX}
                x2={riderBarX + riderBarW}
                y1={beamBarH + 1}
                y2={beamBarH + 1}
                stroke={Colors.NAV_SLIDERULE_CURSOR}
                strokeWidth="2px"
                />
            ) : null
          }
          {
            isMouseDown ? (
              <SlideRuleCursor
                className="slide-rule-cursor-container"
                x={riderBarX}
                w={riderBarW}
                y={beamBarH}
                h={beamTicksH + beamTicksLabelH}
                textStart={moment(tRiderStartAbs).format('H:mm:ss')}
                textEnd={moment(tRiderEndAbs).format('H:mm:ss')}
                fontColor="#fff"
                backColor={Colors.NAV_SLIDERULE_CURSOR}
                />
            ) : null
          }
        </g>
      </g>
    )
  }
}

export default NavigatorBeam
