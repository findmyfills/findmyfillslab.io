import React from 'react';


enum LeftRight {
  LEFT = 'left',
  RIGHT = 'right',
}

type Props = {
  className?: string
  x: number
  y: number
  w: number
  h: number
  color: string
  leftRight: LeftRight
  onMouseDown: (leftRight: LeftRight, e: React.MouseEvent<SVGElement>) => void
}

const RiderHandle = ({
  className = '',
  x, y, w, h,
  onMouseDown,
  color,
  leftRight,
}: Props) => (
  <g className={className}>
    <style jsx>{`
      .handle-hit-region {
        cursor: col-resize;
      }
    `}</style>
    <rect className="handle-hit"
      x={x + w * 0.7 /2}
      y={y}
      width={w * 0.3}
      height={h}
      onMouseDown={(e) => onMouseDown(leftRight, e)}
      fill={color}
      />
    <rect className="handle-hit-region"
      x={x}
      y={y}
      width={w}
      height={h}
      onMouseDown={(e) => onMouseDown(leftRight, e)}
      fill="rgba(255,0,0,0.0)"
      />
  </g>
)

export default RiderHandle

