import React from 'react';
import RiderHandle from './RiderHandle';

const WARNING_COLOR = 'rgba(255,0,0,.25)'

type Props = {
  className?: string
  riderX: number
  riderW: number
  beamX: number
  beamW: number
  y: number
  h: number
  riderColor: string
  onRiderMouseDown: (e: React.MouseEvent<SVGElement>) => void
  enableHandles: boolean
  handleW?: number
  handleH?: number
  handleColor?: string
  shouldRenderMask?: boolean
  shouldRenderRiderBorder?: boolean
  maskColor?: string
  onHandleMouseDown?: (leftRight: string, e: React.MouseEvent<SVGElement>) => void
}

const Rider = ({
  className = '',
  riderX,
  riderW,
  beamX,
  beamW,
  y,
  h,
  riderColor,
  onRiderMouseDown,
  enableHandles,
  handleW = 0,
  handleH = 0,
  handleColor = WARNING_COLOR,
  shouldRenderMask = false,
  shouldRenderRiderBorder = false,
  maskColor = WARNING_COLOR,
  onHandleMouseDown = () => {},
}: Props) => (
  <g className={className}>
    <style jsx>{`
      .rider-bar {
        cursor: ew-resize;
      }
    `}</style>
    {
      shouldRenderMask ? (
        <g className="rider-masks">
          <rect
            x={beamX}
            y={y}
            width={riderX - beamX}
            height={h}
            fill={maskColor}
            />
          <rect
            x={riderX + riderW}
            y={y}
            width={beamW - (riderX + riderW) + beamX}
            height={h}
            fill={maskColor}
            />
        </g>
      ) : null
    }
    <rect
      className="rider-bar"
      x={riderX}
      y={y}
      width={riderW}
      height={h}
      fill={riderColor}
      onMouseDown={onRiderMouseDown}
      />
    {
      shouldRenderRiderBorder ? (
        <g className="rider-borders">
          <line
            x1={riderX}
            x2={riderX}
            y1={y}
            y2={h}
            stroke="rgba(255,255,255,0.5)"
            strokeWidth="1px"
            />
          <line
            x1={riderX + riderW}
            x2={riderX + riderW}
            y1={y}
            y2={h}
            stroke="rgba(255,255,255,0.5)"
            strokeWidth="1px"
            />
        </g>
      ) : null
    }
    {
      enableHandles ? (
        <g className="rider-handle-container">
          <RiderHandle
            className="rider-handle"
            leftRight="left"
            x={riderX - handleW / 2}
            y={(h - handleH) / 2}
            w={handleW}
            h={handleH}
            color={handleColor}
            onMouseDown={onHandleMouseDown}
            />
          <RiderHandle
            className="rider-handle"
            leftRight="right"
            x={riderX - handleW / 2 + riderW}
            y={(h - handleH) / 2}
            w={handleW}
            h={handleH}
            color={handleColor}
            onMouseDown={onHandleMouseDown}
            />
        </g>
      ) : null
    }
  </g>
)

export default Rider