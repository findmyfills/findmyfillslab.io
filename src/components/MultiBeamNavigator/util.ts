import config from '../../app.config';
import { Colors } from '../../constants';
import { selectBeamIndexByKey, selectBeamsByKeys } from '../../reducers';
import { INavBeam } from '../../schemas/navigator';
import { FEGrainType } from '../../types';
import { frameRangeAroundCenter, getItemsBeforeAfter, getLast, toRgbaString } from '../../util';


export function configureNavigatorBeams(navBeams: INavBeam[], topLvlRiderStart?: number, topLvlRiderSize?: number): void{
  // beams must be ascending: 5s --> 1min --> hr...
  // must implement in below order
  configureBeamRiders(navBeams, topLvlRiderStart, topLvlRiderSize)
  configureBeams(navBeams)
  configureChildBeamRiders(navBeams)
  configureBeamColors(navBeams, Colors.NAV_BEAM_RGB_VAL_LIGHTEST)
}


export function configureBeamRiders(navBeams: INavBeam[], topLvlRiderStart?: number, topLvlRiderSize?: number): void {
  // rider star/end can be implemented independedly, since only param it requires to know is beam size, which are fixed.
  for (let i = 0; i < navBeams.length; i++) {
    const thisBeam = navBeams[i]
    const beamAbove = navBeams[i - 1]
    if (i === 0) {
      // Top most granularity...
      let riderStart = topLvlRiderStart || 0
      const riderSize = topLvlRiderSize || thisBeam.beamSize * config.DEFAULT_BEAM_RIDER_PCT
      // const riderSize = topLvlRiderSize || thisBeam.beamSize * config.DEFAULT_BEAM_RIDER_PCT
      // Make sure rider doesn't overshoot beam bar...
      if (riderStart + riderSize > thisBeam.beamSize) {
        riderStart = thisBeam.beamSize - riderSize
      }
      Object.assign(thisBeam, {
        label: thisBeam.label,
        riderStart,
        riderSize,
        riderEnd: riderStart + riderSize,
      })
     } else if (i === navBeams.length - 1) {
      // Bottom most granularity...
      let riderStart = thisBeam.riderStart || 0 // try to keep start...
      const riderSize = beamAbove ? beamAbove.beamSize : thisBeam.beamSize * config.DEFAULT_BEAM_RIDER_PCT
      // Make sure rider doesn't overshoot beam bar...
      if (riderStart + riderSize > thisBeam.beamSize){
        riderStart = thisBeam.beamSize - riderSize
      }
      Object.assign(thisBeam, { // mutate template on the fly, as it is needed in next iteration to construct above navBeams...
        label: thisBeam.label,
        riderStart,
        riderSize,
        riderEnd: riderStart + riderSize,
      })
    } else {
      // Other granularities in between...
      let riderStart = thisBeam.riderStart || 0
      const riderSize = beamAbove.beamSize
      // Make sure rider doesn't overshoot beam bar...
      if (riderStart + riderSize > thisBeam.beamSize) {
        riderStart = thisBeam.beamSize - riderSize
      }
      Object.assign(thisBeam, {
        label: thisBeam.label,
        riderStart,
        riderSize,
        riderEnd: riderStart + riderSize,
      })
    }
  }

}


export function configureBeams(navBeams: INavBeam[]): void {
  for (let i = navBeams.length - 1; i >= 0; i--) { // must iterate in reverse order since beam start depending beam values of beam below...
    const thisBeam = navBeams[i]
    const beamBelow = navBeams[i + 1] // +1 because array is sorted asc
    if (i === navBeams.length - 1) {
      // Bottom most granularity...
      Object.assign(thisBeam, { // mutate template on the fly, as it is needed in next iteration to construct above navBeams...
        beamStart: thisBeam.beamStart || 0,
        beamSize: thisBeam.beamSize,
      })
    } else if (i === 0) {
      // Top most granularity...
      Object.assign(thisBeam, {
        beamStart: beamBelow ? beamBelow.beamStart + beamBelow.riderStart : thisBeam.beamStart,
        beamSize: thisBeam.beamSize,
      })
    } else {
      // Other granularities in between...
      Object.assign(thisBeam, {
        beamStart: beamBelow.beamStart + beamBelow.riderStart,
        beamSize: thisBeam.beamSize,
      })
    }
  }
}


export function configureChildBeamRiders(navBeams: INavBeam[]): void {
  for (let i = 0; i < navBeams.length; i++) {
    const thisBeam = navBeams[i]
    const beamAbove = navBeams[i - 1]
    if (i === 0) {
      // Top most granularity...
      Object.assign(thisBeam, {
        childBeamRiderStart: 0,
        childBeamRiderSize: 0,
      })
    } else {
      // Bottom & other granularities in between...
      Object.assign(thisBeam, {
        childBeamRiderStart: beamAbove ? thisBeam.riderStart + beamAbove.riderStart : 0,
        childBeamRiderSize: beamAbove ? beamAbove.riderSize : 0, // no gran above indicates that navigator contains only one granularity...
      })
    }
  }
}


export function configureBeamColors(navBeams: INavBeam[], lightest: number): void {
  const stepSize = 15
  let base = lightest
  // console.log('---------> base',base)
  for (let i = navBeams.length - 1; i >= 0; i--) {
    base -= stepSize * (navBeams.length - 1 - i)
    // console.log('color', i, base)
    const thisBeam = navBeams[i]
    if (i === 0) {
      // Top most granularity...
      Object.assign(thisBeam, {
        beamColor: toRgbaString(base, 0.75),
        riderColor: toRgbaString(base - stepSize, 0.75),
        childBeamRiderColor: toRgbaString(base - stepSize * 2, 0.75),
      })
    } else {
      // Bottom & other granularities in between...
      Object.assign(thisBeam, {
        beamColor: toRgbaString(base, 0.75),
        riderColor: toRgbaString(base - stepSize, 0.75),
        childBeamRiderColor: toRgbaString(base - stepSize * 2, 0.75),
      })
    }
  }
}


export function getUpdatedNavBeamsSet(
  navBeams: INavBeam[],
  activeBeamKeys: (FEGrainType)[],
  keyUpdatedBeam: string,
  newRiderStart: number,
  newRiderEnd: number,
  newBeamStart?: number,
) {
  const navBeamsCopy = navBeams.slice()
  const activeBeamsSet = selectBeamsByKeys(navBeams, activeBeamKeys)
  const updatedBeamIndex = selectBeamIndexByKey(activeBeamsSet, keyUpdatedBeam)


  if (updatedBeamIndex !== -1) {
    const updatedBeam = activeBeamsSet[updatedBeamIndex]
    // Collect index information...
    const beamIndexes = activeBeamsSet.map((lvl, i) => i)
    const {
      itemsBefore: indexesBefore, // Before and NOT including
      itemsAfter: indexesAfter, // After and including
    } = getItemsBeforeAfter<number>(beamIndexes, updatedBeamIndex)

    // Calulate and implement current beam change...
    implemLocalUpdatesInBeam(updatedBeam, newRiderStart, newRiderEnd, activeBeamsSet, newBeamStart)

    // Calculate and implement Downstreams changes...
    implemUpdatesInDownStreamBeams(activeBeamsSet, indexesAfter)

    // Calulate and implement Upstream changes...
    implemUpdatesInUpStreamBeams(activeBeamsSet, indexesBefore)

    return navBeamsCopy
  }
}


function implemLocalUpdatesInBeam(
  beamToUpdate: INavBeam,
  newRiderStart: number,
  newRiderEnd: number,
  beamsSet: INavBeam[],
  newBeamStart? :number // optional beam update
) {
  const riderStartDelta = newRiderStart - beamToUpdate.riderStart
  const botMostBeam = getLast(beamsSet)

  let beamStart: number
  if(newBeamStart !== undefined){
    beamStart = tryToUpdateBeamStart(beamToUpdate, botMostBeam, newBeamStart) // make sure new value is within navigator bounds...
  } else {
    beamStart = beamToUpdate.beamStart // retain existing value
  }

  Object.assign(beamToUpdate, {
    riderStart: newRiderStart,
    riderSize: newRiderEnd - newRiderStart,
    riderEnd: newRiderEnd,
    beamStart,
    childBeamRiderStart: beamToUpdate.childBeamRiderStart + riderStartDelta,
  })
}

function implemUpdatesInDownStreamBeams(
  navBeams: INavBeam[],
  downSteamIndexes: number[], // includes updated beam index, filtered out below...
){
  // Handle changes coming from a beam above...
  for (let i = 0; i < downSteamIndexes.length; i++) {
    if (i >= 1) { // skipping source of update, which then drives all other downstream and upstream changes...
      const thisIndex = downSteamIndexes[i]
      const thisBeam = navBeams[thisIndex]
      const beamAbove = navBeams[thisIndex - 1]
      const botMostBeam = getLast(navBeams)
      const aboveBeamStart = beamAbove.beamStart
      const thisBeamStart = thisBeam.beamStart
      const thisBeamEnd = thisBeamStart + thisBeam.beamSize
      const thisRiderStartAbs = thisBeam.beamStart + thisBeam.riderStart

      // ------------------
      // Check for mismatch between above beam and this beam...
      if(
        aboveBeamStart < thisBeamStart
        || aboveBeamStart >= thisBeamEnd
      ){ // If above beam position is out-of-bounds in relation to current...
        centerBeamBasedOnBeamAbove(thisBeam, beamAbove, botMostBeam)
      } else { // If above beam bounds IS WITHIN this level beam bounds...
        if(aboveBeamStart !== thisRiderStartAbs){ // BUT above beam position/size no longer matches this level rider position/size...
          matchRiderSizeAndPosToBeamAbove(thisBeam, beamAbove)
        } else { // above beam position/size MATCHES this level rider position/size..
          // No action necessary
        }
      }

      // ------------------
      // Configure child beam rider based on above beam rider size/position...
      matchChildRiderSizeAndPosToRiderAbove(thisBeam, beamAbove)
    }
  }
}

function implemUpdatesInUpStreamBeams(
  navBeams: INavBeam[],
  upSteamIndexes: number[],
  ) {
  // Handle changes coming from a beam below...
  for (let i = upSteamIndexes.length - 1; i >= 0; i--) { // Must loop through in reverse order to propagate changes upwards...
    const thisIndex = upSteamIndexes[i]
    const thisBeam = navBeams[thisIndex]
    const beamBelow = navBeams[thisIndex + 1]
    thisBeam.beamStart = beamBelow.beamStart + beamBelow.riderStart

    const belowRiderStartAbs = beamBelow.beamStart + beamBelow.riderStart
    if(thisBeam.beamStart !== belowRiderStartAbs){
      thisBeam.beamStart = belowRiderStartAbs
    }
  }
}



export function centerBeamBasedOnBeamAbove(beam: INavBeam, beamAbove: INavBeam, botMostBeam: INavBeam): void {

  const aboveBeamCenter =  getBeamCenter(beamAbove)
  const {
    start: unsafeBeamStart, // Center beam around above new center, this value might be out of navigator bounds, must implement boundary-guarding (as below)...
  } = frameRangeAroundCenter(
    aboveBeamCenter,
    beam.beamSize
  )
  const newBeamStart = tryToUpdateBeamStart(beam, botMostBeam, unsafeBeamStart) // make sure new value is within navigator bounds...

  beam.beamStart = newBeamStart
  beam.riderStart = beamAbove.beamStart - beam.beamStart // rider value is relative to beam start!
  beam.riderEnd = beam.riderStart + beamAbove.beamSize
}

export function convertPxDeltaToValDelta(pxDelta: number, beamPxW: number, beamSize: number){
  const xTravelAsPctOfBeamW = pxDelta / beamPxW
  const xTravelInBeamValue = xTravelAsPctOfBeamW * beamSize
  return xTravelInBeamValue
}

export function matchRiderSizeAndPosToBeamAbove(beam: INavBeam, beamAbove: INavBeam): void{
  beam.riderStart = beamAbove.beamStart - beam.beamStart
  beam.riderSize = beamAbove.beamSize
  beam.riderEnd = beam.riderStart + beam.riderSize
}

export function matchChildRiderSizeAndPosToRiderAbove(beam: INavBeam, beamAbove: INavBeam): void{
  beam.childBeamRiderStart = beam.riderStart + beamAbove.riderStart
  beam.childBeamRiderSize = beamAbove.riderSize
}

export function getBeamRiderCenter(beam: INavBeam): number{
  return beam.riderStart + beam.riderSize / 2
}
export function getBeamCenter(beam: INavBeam): number{
  return beam.beamStart + beam.beamSize / 2
}

export function centerBeamAndRiderToView(
  beamsSet: INavBeam[],
  beam: INavBeam,
  viewStart:number, // relative to nav time start abs
  viewEnd:number, // relative to nav time start abs
){
  const viewSize = viewEnd - viewStart
  const viewCenter = viewStart + viewSize / 2
  const botMostBeam = getLast(beamsSet)

  // Center beam around drilldown center...
  const {
    start: unsafeBeamStart // Center beam around above new center, this value might be out of navigator bounds, must implement boundary-guarding (as below)...
  } = frameRangeAroundCenter(
    viewCenter,
    beam.beamSize
  )

  const newBeamStart = tryToUpdateBeamStart(beam, botMostBeam, unsafeBeamStart)

  // Center rider around drilldown center
  const {
    start: newRiderStart,
    end: newRiderEnd,
  } = frameRangeAroundCenter(
    viewCenter - newBeamStart, // value in rider timeframe
    viewSize // set new rider size to drilldown time range
  )
  return {
    newBeamStart,
    newRiderStart,
    newRiderEnd,
  }
}

function tryToUpdateBeamStart(beam: INavBeam, botMostBeam: INavBeam, newBeamStartToTest: number){
  const navigatorStart = 0
  const navigatorEnd = 0 + botMostBeam.beamSize
  let newBeamStart: number
  if(newBeamStartToTest < navigatorStart){
    newBeamStart = navigatorStart // no beamStart goes below 0, which is the time start of the navigator
  } else if(newBeamStartToTest + beam.beamSize > navigatorEnd){
    newBeamStart = navigatorEnd - beam.beamSize
  } else { // with in navigator bounds...
    newBeamStart = newBeamStartToTest
  }
  return newBeamStart
}