import React from 'react';


function getTrianglePts(x: number, y: number, w: number, h: number, dir: 'left' | 'right', ): string {
  switch (dir) {
    case 'left':
      return [
        `${x + w},${y}`,
        `${x},${y + h / 2}`,
        `${x + w},${y + h}`
      ].join(' ')
    case 'right':
      return [
        `${x},${y}`,
        `${x + w},${y + h / 2}`,
        `${x},${y + h}`
      ].join(' ')
  }
}


type Props = {
  x: number
  y: number
  w: number
  h: number
  dir: 'left' | 'right'
  cornerRadius?: number
  xPad?: number
  yPad?: number
  fill?: string
  stroke?: string
  strokeWidth?: string
  isDisabled?: boolean
  onClick?: (e: React.MouseEvent<SVGElement>) => void
}

const SvgStepArrow = ({
  x, y, w, h,
  cornerRadius = 0,
  dir,
  xPad = 0,
  yPad = 0,
  fill = 'none',
  stroke = 'red',
  strokeWidth = '1px',
  isDisabled = false,
  onClick = () => {}
}: Props) => (
  <g className="beam-step-arrow-container"
    onClick={onClick}
    >
    <style jsx>{`
      g {
        cursor: pointer;
      }
      g:hover rect {
        fill: ${stroke};
      }
      g:hover polyline {
        stroke: white;
      }
    `}</style>
    <rect
      x={x}
      y={y}
      width={w}
      height={h}
      strokeLinejoin="round"
      rx={cornerRadius}
      ry={cornerRadius}
      fill="rgba(255,0,0, 0.0)"
      />
    <polyline
      points={getTrianglePts(x + xPad, y + yPad, w - xPad * 2, h - yPad * 2, dir)}
      fill={fill}
      stroke={stroke}
      strokeWidth={strokeWidth}
      />
  </g>
)

export default SvgStepArrow