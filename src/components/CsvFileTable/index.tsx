import React from 'react';
import { CsvMapperHeaders } from '../../records';
import { CsvSideType, CsvTimezoneType } from '../../types';
import { IHeaderMapping } from '../CsvHeaderMapper';
import TableRow from './TableRow';

export type CsvParams = {
  uploadTime: number
  fileName: string
  fileId: string
  originalCsv: string[][]
  originalHeaderKeys: string[]
  normalizedCsv: undefined | string[][]
  headerMappings: IHeaderMapping[]
  isCsvConfigValid: boolean
  missingHeaders: (CsvMapperHeaders)[]
  headersToGenerate: (CsvMapperHeaders)[]
  // surveyed values from csv...
  uniqueSymbolsInCsv: string[]
  sampleDateValueInCsv: string | number | undefined
  // normalized values for server call...
  targetSymbol: string | undefined
  targetTimezone: CsvTimezoneType
  targetDate: string
  targetSide: CsvSideType
}

type Props = {
  shouldRenderRowsReversed: boolean | undefined
  csvInfos: CsvParams[]
  onFileRemove: (fileIndex: number) => void
  onFileEdit: (fileIndex: number) => void
  onUploadCsvToBackend: (fileIndex: number) => void
}

type State = {
}



class FileTable extends React.Component<Props, State> {

  constructor(props: Props){
    super(props)
    this.state = {

    }
  }

  renderTableRows(){
    const { csvInfos } = this.props
    const rowComponents = csvInfos.map(( file, index ) => {
      return (
        <TableRow
          key={index}
          onFileRemove={this.props.onFileRemove}
          onFileEdit={this.props.onFileEdit}
          onUploadCsvToBackend={this.props.onUploadCsvToBackend}
          csvInfo={file}
          csvIndex={index}
          />
      )
    })

    if(this.props.shouldRenderRowsReversed){
      return rowComponents.slice().reverse()
    } else {
      return rowComponents
    }
  }

  render(){
    return (
      <div className="csv-file-table">
        <style jsx>{`
          .csv-file-table {
            height: 150px;
            overflow: auto;
            padding: 10px;
          }
        `}</style>
        {this.renderTableRows()}
      </div>
    )
  }
}

export default FileTable