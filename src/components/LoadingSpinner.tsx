import { Spinner } from '@blueprintjs/core';
import React from 'react';
import { connect } from 'react-redux';

export const styledJsxSpinner = `
.relative {
  position: relative;
}

.blur {
  -webkit-filter: blur(3px);
  -moz-filter: blur(3px);
  -ms-filter: blur(3px);
  -o-filter: blur(3px);
  filter: blur(3px);
}
`

export function withLoadingSpinner<PassedProps extends object, AppStoreStates>(
  WrappedComponent: React.ComponentType<PassedProps>,
  selectSpinnerState: (a: AppStoreStates) => boolean,
): React.ComponentType<PassedProps>{

  type HocProps = {
    spinnerSize?: number
  } & StoreProps

  class SpinnerHoc extends React.Component<PassedProps & HocProps>{

    render(){
      const {
        isSpinnerActive,
        spinnerSize,
        children,
        ...restProps
      } = this.props

      return (
        <div className="relative full-height">
          <div className={isSpinnerActive ? 'blur full-height' : 'full-height'}>
            <WrappedComponent
              {...restProps as PassedProps}
              >
              {children}
            </WrappedComponent>
          </div>
          <LoadingSpinner
            isActive={isSpinnerActive}
            size={spinnerSize}
            />
        </div>
      )
    }
  }

  type StoreProps = {
    isSpinnerActive: boolean
  }

  const mapStateToProps = (state: AppStoreStates) => ({
    isSpinnerActive: selectSpinnerState(state),
  })

  return connect(
    mapStateToProps,
    null
  )(SpinnerHoc)

}


type Props = {
  isActive: boolean
  size?: number
}

const LoadingSpinner = ({
  isActive,
  size = Spinner.SIZE_STANDARD,
}: Props) => isActive ? (
  <div className="container-spinner">
    <style jsx>{`
      .container-spinner {
        position: absolute;
        width: 100%;
        height: 100%;
        top: 0;
        right: 0;
      }
      div :global(.bp3-spinner){
        height: 100%;
      }
    `}</style>
    <Spinner
      intent="none"
      size={size}
      />
  </div>
) : null


export default LoadingSpinner