import { Button } from '@blueprintjs/core';
import React from 'react';
import { ICsv } from '../../types';
import CsvPreviewTable from '../CsvPreviewTable';
import CsvParser from './CsvParser';
import ImporterWarning from './ImporterWarning';



type Props = {
  onValidCsvCreated: (csvHeader: string[], csvBody: string[][], fileName: string) => void
}

type State = {
  parsedCsvFileName: string | undefined
  parsedCsvHeaders: string[] | undefined
  parsedCsvBody: string[][] | undefined
  isUploadedFileValidCsv: boolean
  parserOptions: Record<string, any>
}


class UploadCsvWidget extends React.Component<Props, State> {

  constructor(props: Props){
    super(props)
    this.state = {
      parsedCsvFileName: undefined,
      parsedCsvHeaders: undefined,
      parsedCsvBody: undefined,
      isUploadedFileValidCsv: false,
      parserOptions: {
        // https://www.papaparse.com/docs#config
        header: false,
        skipEmptyLines: 'greedy',
        error: this.handleCsvReaderError,
      }
    }
  }

  handleCsvReaderError = () => {
    console.error('CsvReader something went wrong!')
  }

  handleCsvLoaded = (parsedCsv: ICsv, fileName: string) => {
    console.log(`${fileName} uploaded and parsed:`, parsedCsv)

    if(parsedCsv.length < 2){
      this.setState({
        isUploadedFileValidCsv: false,
      })
    } else {
      const [
        parsedCsvHeaders,
        ...parsedCsvBody
      ] = parsedCsv
      // console.log('parsedCsvHeaders', parsedCsvHeaders)
      // console.log('parsedCsvBody', parsedCsvBody)
      this.setState({
        isUploadedFileValidCsv: true,
        parsedCsvFileName: fileName,
        parsedCsvHeaders,
        parsedCsvBody,
      })
    }
  }


  handleConfirmParsedCsv = () => {
    const { parsedCsvBody, parsedCsvHeaders, parsedCsvFileName } = this.state
    if(
      parsedCsvHeaders !== undefined
      && parsedCsvBody !== undefined
      && parsedCsvFileName !== undefined
    ) {
      this.props.onValidCsvCreated(parsedCsvHeaders, parsedCsvBody, parsedCsvFileName)
    }
  }

  render(){
    const { isUploadedFileValidCsv, parsedCsvBody, parsedCsvHeaders, parsedCsvFileName, parserOptions } = this.state

    const hasParsedCsv = parsedCsvHeaders !== undefined && parsedCsvBody !== undefined

    return (
      <div className="dialog-content-container">
        <ImporterWarning/>

        <CsvParser
          className="csv-reader-component"
          onFileLoaded={this.handleCsvLoaded}
          uploadedFileName={parsedCsvFileName}
          parserOptions={parserOptions}
          />
        {
          hasParsedCsv ? (
            <>
              <CsvPreviewTable
                headerRow={parsedCsvHeaders}
                bodyRows={parsedCsvBody}
                columnW={100}
                />
              <Button
                icon="import"
                intent="primary"
                text="Import CSV"
                minimal={true}
                disabled={!isUploadedFileValidCsv}
                onClick={this.handleConfirmParsedCsv}
                />
            </>
          ) : null
        }
      </div>
    )
  }
}

export default UploadCsvWidget