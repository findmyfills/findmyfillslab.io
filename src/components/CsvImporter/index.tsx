import { Button, ButtonGroup, Dialog, Intent } from '@blueprintjs/core';
import React from 'react';
import PasteCsvWidget from './PasteCsvWidget';
import UploadCsvWidget from './UploadCsvWidget';


type Props = {
  onCsvImported: (csvHeader: string[], csvBody: string[][], fileName: string) => void
}

type State = {
  isPasteCsvWidgetActive: boolean
  isUploadCsvWidgetActive: boolean
}

class CsvImporter extends React.Component<Props, State> {

  constructor(props: Props){
    super(props)
    this.state = {
      isPasteCsvWidgetActive: false,
      isUploadCsvWidgetActive: false,
    }
  }

  handleValidCsvCreated = (csvHeader: string[], csvBody: string[][], fileName: string) => {
    this.props.onCsvImported(csvHeader, csvBody, fileName)
    this.closeCsvImportWidgets()
  }

  openCsvUploadWidget = () => {
    this.setState({
      isUploadCsvWidgetActive: true,
    })
  }

  openCsvPasteTextWidget = () => {
    this.setState({
      isPasteCsvWidgetActive: true,
    })
  }

  closeCsvImportWidgets = () => {
    this.setState({
      isPasteCsvWidgetActive: false,
      isUploadCsvWidgetActive: false,
    })
  }

  render(){
    const { isPasteCsvWidgetActive, isUploadCsvWidgetActive } = this.state
    return (
      <div className="csv-importer-container">
        <ButtonGroup
          minimal={false}
          fill={true}
          >
          <Button
            intent={Intent.NONE}
            icon="upload"
            text="Upload CSV"
            minimal={false}
            onClick={this.openCsvUploadWidget}
            />
          <Button
            intent={Intent.NONE}
            icon="clipboard"
            text="Paste CSV"
            minimal={false}
            onClick={this.openCsvPasteTextWidget}
            />

          <Button
            icon="endorsed"
            text="Get Sample"
            onClick={()=>{
              window.open("/static/assets/sample_UBNT.csv")
            }}
            />
        </ButtonGroup>

        <Dialog
          icon="upload"
          onClose={this.closeCsvImportWidgets}
          isOpen={isUploadCsvWidgetActive}
          title="Upload New CSV"
          >
          <UploadCsvWidget
            onValidCsvCreated={this.handleValidCsvCreated}
            />
        </Dialog>

        <Dialog
          icon="clipboard"
          onClose={this.closeCsvImportWidgets}
          isOpen={isPasteCsvWidgetActive}
          title="Paste New CSV"
          >
          <PasteCsvWidget
            onValidCsvCreated={this.handleValidCsvCreated}
            />
        </Dialog>
      </div>
    )
  }
}

export default CsvImporter