import { Callout } from '@blueprintjs/core';



const ImporterWarning = (

) => (
  <Callout
    icon="warning-sign"
    title="Important"
    >
    Please make sure your CSV data contain a header row!
  </Callout>
)


export default ImporterWarning