import { SelectionModes } from '@blueprintjs/table';
import React from 'react';
import TableInteractive, { TableAlignType, TableDataType, UserColSchema } from './TableInteractive';

type Props = {
  headerRow: string[] | undefined
  bodyRows: string[][] | undefined
  columnW?: number
}

type State = {
}


class CsvPreviewTable extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props)
  }
  render(){
    const { bodyRows, headerRow, columnW } = this.props
    if (headerRow === undefined || bodyRows === undefined) return null

    const columns: UserColSchema[] = headerRow.map(headerKey =>({
      label: headerKey,
      key: headerKey,
      valType: TableDataType.NO_TYPE,
      alignment: TableAlignType.LEFT,
    }))

    const rowsData = bodyRows.map(row => {
      return columns.reduce((prev: Record<string, string>, schema, colIndex) => {
        prev[schema.key] = row[colIndex]
        return prev
      }, {})
    })

    return (
      <TableInteractive
        columnSchemas={columns}
        rowsData={rowsData}
        bp3Options={{
          numRows: rowsData.length,
          defaultColumnWidth: columnW,
          selectionModes: SelectionModes.COLUMNS_AND_CELLS,
        }}
        />
    )
  }
}

export default CsvPreviewTable