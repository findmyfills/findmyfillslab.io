import React from 'react';
import { Colors } from '../constants';
type Props = {
  title?: string
  padding?: string
  overflow?: string
  border?: string
  borderTop?: boolean
  direction?: string
  shouldRenderContent?: boolean
  children?: JSX.Element | JSX.Element[] | null
}

const Section: React.SFC<Props> = ({
  title,
  overflow = 'unset',
  padding = '10px',
  shouldRenderContent = true,
  direction = 'column',
  border = '1px solid rgba(16, 22, 26, 0.15)',
  borderTop = false,
  children
}) => shouldRenderContent ? (
  <div className="app-section">
    <style jsx>{`
      .app-section {
        border: ${border};
        ${borderTop ? 'border-top: 2px solid' : ''};
        width: 100%;
        display: flex;
        flex-direction: ${direction};
        overflow: ${overflow};
        margin-bottom: 15px;
      }
      .section-title {
        margin:0;
        font-size: 14px;
        font-weight: 400;
        background: ${Colors.ACCENT_1};
        color: white;
        padding: 4px 6px;
      }
      .section-content {
        padding: ${padding};
        overflow: ${overflow};
      }
    `}</style>
    {
      title !== undefined ? (
        <p className="section-title">
          {title}
        </p>
      ) : null
    }
    <div className="section-content">
      {children}
    </div>
  </div>
) : null

export default Section