import { Position } from '@blueprintjs/core';
import { DateInput as Bp3DateInput } from '@blueprintjs/datetime';
import React from 'react';
import { DATEPICKER_MAX_DATE, DATEPICKER_MIN_DATE } from '../constants';
import { createUTCDate, getMMDDYYYY } from '../util';



const isSun = (date: Date) => date.getDay() === 0
const isSat = (date: Date) => date.getDay() === 6
const modifiers = { 'isSunday': isSun, 'isSaturday': isSat } // name of modifier function becomes the suffix for the CSS class above

type Props = {
  dateValue: Date | null | undefined
  onDateSelect: (date: Date) => void
}
type State = {

}


class DatePicker extends React.Component<Props, State> {

  constructor(props: Props){
    super(props)
  }

  componentWillReceiveProps(nextProps: Props){
    // const { selectedDayStartUtc } = nextProps
    // if (selectedDayStartUtc !== this.props.selectedDayStartUtc){
    // }
  }

  handleDateChange = (selectedDate: Date | null, isUserChange: boolean) => {

    console.log('selectedDate',selectedDate)
    if(selectedDate === null) return
    if(isNaN(selectedDate.valueOf())) return

    if(isUserChange){ // if the user clicked on a date in the calendar, changed the input value, or cleared the selection

      const [ yyyy, mm, dd ] = getMMDDYYYY(selectedDate).split('-')
      const utcDate = createUTCDate(yyyy, mm, dd)
      // console.log('selectedDate.toUTCString(()',utcDate.valueOf(),utcDate.toUTCString())
      // if (!isSun(utcDate) && !isSat(selectedDate)) {
      this.props.onDateSelect(utcDate)
      // }
    } else { // if the date was changed by choosing a new month or year.

    }
  }

  /**
   * Called when the user finishes typing in a new date and the date causes an error state. If the date is invalid, new Date(undefined) will be returned. If the date is out of range, the out of range date will be returned (onChange is not called in this case).
   */
  handleDateError = (errorDate: Date) => {
    console.log('handleDateError',errorDate.valueOf())
  }

  /**
   * Function to deserialize user input text to a JavaScript Date object. Return false if the string is an invalid date. Return null to represent the absence of a date. Optional locale argument comes directly from the prop on this component.
   */
  parseDate(userDateString: string, locale?: string): Date | false | null {
    if(userDateString === '') return false
    const [ MM, DD, YYYY ] = userDateString.split('-')
    if(MM === undefined || DD === undefined || YYYY === undefined) return false
    const date = createUTCDate(YYYY, MM, DD)
    // console.log('parseDate --> ',date.valueOf(), date.toUTCString(), )
    return date
  }

  render() {
    const { dateValue } = this.props
    return (
      <div className="date-picker-container">
        <Bp3DateInput
          formatDate={getMMDDYYYY}
          onChange={this.handleDateChange}
          onError={this.handleDateError}
          parseDate={this.parseDate}
          placeholder="MM-DD-YYYY"
          value={dateValue}
          minDate={DATEPICKER_MIN_DATE}
          maxDate={DATEPICKER_MAX_DATE}
          showActionsBar={false}
          reverseMonthAndYearMenus={true}
          clearButtonText=""
          modifiers={modifiers}
          popoverProps={{
            position: Position.LEFT_BOTTOM
          }}
          />
      </div>
    )
  }
}

export default DatePicker
