import { Button } from '@blueprintjs/core';

type Props = {
  onSignOut?: () => void
}

const LogOutButton = ({
  onSignOut,
}: Props) => onSignOut !== undefined ? (
  <Button
    icon="log-out"
    text="Log Out"
    onClick={onSignOut}
    style={{
      height: '38px',
      marginLeft: '15px',
    }}
    />
) : null

export default LogOutButton