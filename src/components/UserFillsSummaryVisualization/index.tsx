import React from 'react';
import PieChart from '../../components/PieChart';
import { Colors } from '../../constants';
import { getVenueBreakdownFromUserFills } from '../../models';
import { findColorForVenue, PayloadCsvFill } from '../../records';
import { BEVenueTypes, HcPieData } from '../../types';


function createPieChartData(userFills: PayloadCsvFill[] | undefined): State {
  const {
    venueBreakdowns,
    sharesCountTotal,
    fillsCountTotal,
    fillsCountMatched,
  } = getVenueBreakdownFromUserFills(userFills)

  if(
    fillsCountTotal === 0
    && sharesCountTotal === 0
  ) {
    return {
      venueKeys: [BEVenueTypes.UNKNOWN],
      fillsBreakdownData:[{
        id: BEVenueTypes.UNKNOWN,
        name: BEVenueTypes.UNKNOWN,
        color: findColorForVenue(BEVenueTypes.UNKNOWN),
        y: 1.0,
      }],
      sharesBreakdownData:[{
        id: BEVenueTypes.UNKNOWN,
        name: BEVenueTypes.UNKNOWN,
        color: findColorForVenue(BEVenueTypes.UNKNOWN),
        y: 1.0,
      }],
      fillsMatchedData:[{
        id: 'Unmatched Fills',
        name: 'Unmatched Fills',
        color: Colors.VENUE_UNKNOWN,
        y: 1.0,
      }],
    }
  }

  const fillsBreakdownData: HcPieData[]  = []
  const sharesBreakdownData: HcPieData[]  = []
  const fillsMatchedData: HcPieData[] = [{
    id: 'Matched Fills',
    name: 'Matched Fills',
    color: Colors.HC_SCATTER_FILLS_MATCHED,
    y: fillsCountMatched / fillsCountTotal,
  },{
    id: 'Unmatched Fills',
    name: 'Unmatched Fills',
    color: Colors.VENUE_UNKNOWN,
    y: 1 - fillsCountMatched / fillsCountTotal,
  }]

  const venueKeys: (BEVenueTypes)[] = []
  if(fillsCountTotal > 0 && sharesCountTotal > 0){
    const keys = Object.keys(venueBreakdowns) as (BEVenueTypes)[]
    keys.forEach((venueKey)=>{
      venueKeys.push(venueKey)
      const { fillsCount, sharesCount } = venueBreakdowns[venueKey]
      fillsBreakdownData.push({
        id: venueKey,
        name: venueKey,
        color: findColorForVenue(venueKey),
        y: fillsCount / fillsCountTotal
      })
      sharesBreakdownData.push({
        id: venueKey,
        name: venueKey,
        color: findColorForVenue(venueKey),
        y: sharesCount / sharesCountTotal,
      })
    })
  }
  return {
    fillsBreakdownData,
    sharesBreakdownData,
    fillsMatchedData,
    venueKeys,
  }
}


export type VenueBreakdown = {
  fillsCount: number
  sharesCount: number
}

type Props = {
  containerW: number
  // containerH: number
  userFills: PayloadCsvFill[] | undefined
}

type State = {
  venueKeys: (BEVenueTypes)[]
  fillsBreakdownData: HcPieData[]
  sharesBreakdownData: HcPieData[]
  fillsMatchedData: HcPieData[]
}

class UserFillsSummaryVisualization extends React.Component<Props, State> {

  constructor(props: Props) {
    super(props)
    this.state = {
      ...createPieChartData(props.userFills)
    }
  }

  componentWillReceiveProps(nextProps: Props) {
    const { userFills } = nextProps
    if (userFills !== this.props.userFills) {
      this.setState({
        ...createPieChartData(userFills)
      })
    }
  }

  render() {
    const { containerW } = this.props
    const { fillsBreakdownData, sharesBreakdownData, fillsMatchedData, venueKeys } = this.state

    const columnW = containerW / 4
    const chartWidth = columnW * 1
    const legendWidth = columnW * 1

    // console.log('%c Stock Chart rendering...', 'color:red')
    return (
      <div id="container-fills-summary-visualizations">
        <style jsx>{`
          #container-pie-charts-and-title {
            display: flex;
          }
          #container-pie-charts-legends{
            width: ${legendWidth}px;
          }
          #container-pie-charts-legends > div {
            margin: 0 3px 2px 0px;
          }
        `}</style>
        <div id="container-pie-charts-and-title">
          <div id="container-pie-charts-legends">
            {venueKeys.map((venueKey)=>(
              <div key={venueKey}
                className="small-tag"
                style={{ backgroundColor: findColorForVenue(venueKey) }}
                >
                {venueKey}
              </div>
            ))}
          </div>
          <div>
            <p className="text-small text-center">
              by fills
            </p>
            <PieChart
              containerW={chartWidth}
              containerH={chartWidth}
              data={fillsBreakdownData}
              />
          </div>
          <div>
            <p className="text-small text-center">
              by shares
            </p>
            <PieChart
              containerW={chartWidth}
              containerH={chartWidth}
              data={sharesBreakdownData}
              />
          </div>
          <div>
            <p className="text-small text-center">
              matched fills
            </p>
            <PieChart
              containerW={chartWidth}
              containerH={chartWidth}
              data={fillsMatchedData}
              />
          </div>
        </div>
      </div>
    )
  }
}

export default UserFillsSummaryVisualization