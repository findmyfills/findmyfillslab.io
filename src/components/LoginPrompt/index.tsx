import { SessionStatus } from '../../types';

function getUserPromptForStatus(status: SessionStatus){
  return MAP_SESSION_STATUS_TO_USER_PROMPT[status]
}

const MAP_SESSION_STATUS_TO_USER_PROMPT = {
  [SessionStatus.SIGNED_IN]: 'Signed in.',
  [SessionStatus.SIGNED_OUT]: 'Please sign in using one of the options below.',
  [SessionStatus.PENDING]: 'Please wait while we validate your credentials...',
}



type Props = {
  status: SessionStatus
  statusTime: number | undefined
}
const LoginPrompt = ({
  status,
  statusTime,
}: Props) => (
  <div id="login-prompt-container">
    <style jsx>{`
      #login-prompt-container {
        width: 500px;
        margin: 20px auto 35px auto;
        text-align: center;
      }
      h4 { font-size: 16px; font-weight: 200; }
      p { font-size: 12px; font-weight: 100; font-style: italic; color:#aaa }
      #logo { width: 100px; margin-bottom: 20px; }
      #banner {
        position: relative;
        height: 220px;
        overflow: hidden;
        margin-bottom: 75px;
      }
      #banner h1 {
        text-shadow: 0px 0px 5px rgba(29, 29, 37, 0.5);
        line-height: 220px;
        color: #fff;
        font-size: 59px;
        font-weight: 300;
        text-align: center;
        margin: 0;
      }
      #banner-img-container {
        position: absolute;
        left:0; right: 0;
        z-index: -99;
      }
      #fmf-image {
        width: 500px;
        height: auto;
      }
    `}</style>

    <img id="logo" src="/static/assets/img/proof_logo_full_312x312.jpg"/>
    <div id="banner">
      <div id="banner-img-container">
        <img id="fmf-image" src="/static/assets/img/optical_cabinet_solar_microscope.jpg"/>
      </div>
      <h1 id="app-title">FIND MY FILLS</h1>
    </div>

     <h4>{getUserPromptForStatus(status)}</h4>
     {
       statusTime !== undefined ? (
         <p>{`${status} at: ${new Date(statusTime).toString()}`}</p>
       ) : null
     }
  </div>
)

export default LoginPrompt