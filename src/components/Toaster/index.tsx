import { IToastProps, Position, Toaster } from '@blueprintjs/core';
import React from 'react';
import { connect } from 'react-redux';
import { IApplicationState } from '../../reducers';
import { getLast } from '../../util';

export * from './toast-creators';
export * from './withToasts';

type Props = {
  toasts: IToastProps[]
} & StoreProps

class ToasterComponent extends React.Component<Props> {
  private toaster: Toaster | null

  constructor(props: Props) {
    super(props)
    this.toaster = null
  }

  componentWillReceiveProps(nextProps: Props){
    const { toasts } = nextProps
    if (toasts.length !== this.props.toasts.length && toasts.length > 0){
      this.showToast(getLast(toasts))
    }
  }

  private showToast(toastProps: IToastProps) {
    const { toaster } = this
    if (toaster !== null){
      toaster.show(toastProps)
    }
  }

  render() {
    const { } = this.props
    return (
      <Toaster
        autoFocus={false}
        canEscapeKeyClear={true}
        position={Position.TOP_RIGHT}
        ref={(ref: Toaster) => (this.toaster = ref)}
        />
    )
  }
}

type StoreProps = {
  toasts: IToastProps[]
}

const mapStateToProps = (state: IApplicationState) => ({
  toasts: state.appControls.toasts,
})

export default connect(
  mapStateToProps,
  null
)(ToasterComponent)




