import moment from 'moment';
import config from '../../app.config';
import { Colors } from '../../constants';
import { findBEGrainTypeForFEGrainType, findDurationForBEGrainType } from '../../records';
import { extractParamsFromKey } from '../../reducers';
import { BEDataType, FEGrainType, HcAxisType, HcScatterData, HcSeriesType } from '../../types';
import { toPctString } from '../../util';


export function minToMs(unix: number){
  return unix * 60 * 1000
}

export function createChartTitle(symbol: string) {
  return symbol === '' ? 'No symbol selected...' : `${symbol} Stock Price`
}

export function isXAxisRangeWithinDataBounds(xMin: number, xMax: number, dataMin: number, dataMax: number): boolean {
  return xMin >= dataMin && xMax <= dataMax
}

export function getCandlestickBarSize(candlestickGrainType: FEGrainType){
  const backendGrainType = findBEGrainTypeForFEGrainType(candlestickGrainType)
  return findDurationForBEGrainType(backendGrainType)
}

export function toggleVolumeYAxis(yAxisDefault: any, yAxisVolume: any, shouldRenderVolumeYAxis: boolean){

  if(shouldRenderVolumeYAxis){
    // default y-axis
    yAxisDefault.options.height = toPctString(1.0 - config.HC_VOL_BAR_HEIGHT_PCT - 0.05)
    // volume y-axis
    yAxisVolume.options.visible = true
    yAxisVolume.options.offset = 0
    yAxisVolume.options.top =  toPctString(1.0 - config.HC_VOL_BAR_HEIGHT_PCT)
    yAxisVolume.options.height = toPctString(config.HC_VOL_BAR_HEIGHT_PCT)
    // force update
    yAxisDefault.isDirty = true
    yAxisVolume.isDirty = true
  } else {
    // default y-axis
    yAxisDefault.options.height = toPctString(1.0)
    // volume y-axis
    yAxisVolume.options.visible = false
    // force update
    yAxisDefault.isDirty = true
    yAxisVolume.isDirty = true
  }
}

export function scatterTooltipPtFormatter(this: Highcharts.Point): string {
  const {x, y, color} = this
  const [sec, subSec] = String(x / 1000).split('.')
  const HHmmss = moment(+sec * 1000).format('HH:mm:ss')
  // console.log(x, sec, subSec)
  return `
    <p>price: <b>${y}</b></p><br/>
    <p>time: <b>${HHmmss}.${subSec || 0}</b></p><br/>
  `
}

export function arearangeTooltipPtFormatter(this: Highcharts.Point): string {
  const {x, low, high, szBid, exBid, szAsk, exAsk, color} = this
  const [sec, subSec] = String(x / 1000).split('.')
  const HHmmss = moment(+sec * 1000).format('HH:mm:ss')
  // console.log(x, sec, subSec)
  return `
    <p>ask: <b>${high}</b> - ${szAsk} at ${exAsk}</p><br/>
    <p>bid: <b>${low}</b> - ${szBid} at ${exBid}</p><br/>
    <p>time: <b>${HHmmss}.${subSec || 0}</b></p><br/>
  `
}

export const createSeriesOptnFromKeys = (
  allDataSlices: Record<string, any[]> ,
  sliceKeys: string[],
  onCandlesitckClick: Highcharts.SeriesClickCallbackFunction, // NOTE: double-click event requires 'highcharts-custom-events'
) => sliceKeys.map((sliceKey) => {
  const { symbol, hcSeriesType, dataType } = extractParamsFromKey(sliceKey)
  const data = allDataSlices[sliceKey]
  const type = hcSeriesType.toLowerCase()
  switch (dataType) {
    case BEDataType.VOLUME:
      return {
        type,
        name: `${dataType.toLowerCase()}`,
        yAxis: HcAxisType.Y_AXIS_VOLUME,
        data,
        color: Colors.HC_VOLUME_BAR,
        point: { events:{ click: undefined }},
        dataGrouping: {
          enabled: true,
          approximation: 'sum',
        },
        turboThreshold: config.HC_TURBO_THRESHOLD,
        cursor: undefined,
        tooltip:{
          // xDateFormat: '%H:%M:%S',
          pointFormatter: undefined,
        },
      }
    case BEDataType.TICKS:
      switch (hcSeriesType) {
        case HcSeriesType.LINE:
        case HcSeriesType.CANDLESTICK:
          return {
            type,
            name: `${symbol}`,
            yAxis: HcAxisType.Y_AXIS_DEFAULT,
            data,
            color: Colors.HC_CANDLESTICK_DOWN,
            lineColor:Colors.HC_CANDLESTICK_DOWN,
            upColor: Colors.HC_CANDLESTICK_UP,
            upLineColor: Colors.HC_CANDLESTICK_UP,
            tooltip:{
              // xDateFormat: '%H:%M:%S',
              pointFormatter: undefined,
            },
            turboThreshold: config.HC_TURBO_THRESHOLD,
            cursor: 'zoom-in',
            point:{
              events:{
                click: onCandlesitckClick,
              },
            },
            dataGrouping: { // must re-set params here after switching to scatter plot and back...
              enabled: true,
              approximation: 'ohlc',
            }
          }
        case HcSeriesType.SCATTER:
          return createHcScatterSeries(data, `${symbol} - ${dataType.toLowerCase()}`)
      }
      break;
    case BEDataType.NBBO:
      return {
        type,
        name: `${symbol} - ${dataType}`,
        yAxis: HcAxisType.Y_AXIS_DEFAULT,
        step: true,
        data,
        dataGrouping: {
          enabled: true,
          approximation: 'range',
        },
        point:{ events:{ click: undefined }},
        turboThreshold: config.HC_TURBO_THRESHOLD,
        cursor: undefined,
        tooltip:{
          enabled: false,
          headerFormat: '<b>{point.key}</b><br>',
          pointFormatter: arearangeTooltipPtFormatter,
          // xDateFormat: '%H:%M:%S.%L',
        },
        lineColor: Colors.HC_AREARANGE_NBBO_STROKE,
        fillColor: Colors.HC_AREARANGE_NBBO_FILL,
      }
  }
})



export const createHcScatterSeries = (
  seriesData: HcScatterData[],
  seriesName: string,
  onClick?: Highcharts.SeriesClickCallbackFunction,
) => ({
  type: HcSeriesType.SCATTER.toLowerCase(),
  name: seriesName,
  yAxis: HcAxisType.Y_AXIS_DEFAULT,
  turboThreshold: config.HC_TURBO_THRESHOLD,
  cursor: onClick && 'zoom-in',
  marker: {
    symbol: 'circle'
  },
  point:{
    events:{
      click: onClick || (() => {}),
    },
  },
  dataGrouping: {
    enabled:false,
    approximation: undefined,
  },
  tooltip:{
    headerFormat: '<b>{point.key}</b><br>',
    pointFormatter: scatterTooltipPtFormatter,
    // xDateFormat: '%H:%M:%S.%L',
  },
  data: seriesData,
})