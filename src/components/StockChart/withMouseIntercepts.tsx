import { Chart } from 'highcharts';
import React from 'react';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { createDrillDownToViewAction, createMoveRiderStartEndAction, createSetTopVisibleBeamAction, IDrillDownToViewAction, IMoveRiderStartEndAction, SetTopVisibleBeamAction } from '../../actions';
import config from '../../app.config';
import { IApplicationState, selectBeamByKey, selectBeamIndexByKey, selectBeamsByKeys } from '../../reducers';
import { INavBeam } from '../../schemas/navigator';
import { FEGrainType, HcClickEvent, NavUpdateType } from '../../types';
import { convertPxDeltaToValDelta } from '../MultiBeamNavigator';
import { getCandlestickBarSize } from './util';


const DEFAULT_MOUSE_STATES = {
  isMouseDown: false,
}


export function withMouseIntercepts<PassThroughProps>(
  WrappedComponent: React.ComponentType<PassThroughProps>
): React.ComponentType<PassThroughProps> {

  type HocProps = {

  } & StoreProps & DispatchProps

  type HocState = {
    isMouseDown: boolean
  }

  class MouseInterceptHOC extends React.Component<PassThroughProps & HocProps, HocState>{

    private chartInstance: Chart | null
    private mousePos: { x: number, y: number }
    private debouncePan: boolean
    private debounceZoom: boolean

    constructor(props: PassThroughProps & HocProps) {
      super(props)
      this.chartInstance = null
      this.mousePos = { x: 0, y: 0 }
      this.debouncePan = false
      this.debounceZoom = false
      this.state = Object.assign({}, DEFAULT_MOUSE_STATES, {
        // additional state props...
      })
    }

    componentDidMount() {
      const { chartInstance } = this
      if (chartInstance !== null) {
        this.interceptZoomActions(chartInstance)
        this.interceptPanActions(chartInstance)
      }
    }

    resetMouseStates() {
      this.setState(DEFAULT_MOUSE_STATES)
    }

    handleUserFillsClick = (
      { point }: HcClickEvent // requires 'highcharts-custom-events'
    ) => {
      console.log(`%chandleUserFillsClick`,'background:orange')
      const { topVisibleBeamKey, topActiveBeamKey, navBeams } = this.props as HocProps
      const topActiveBeam = selectBeamByKey(navBeams, topActiveBeamKey)
      if(
        point !== undefined
        && topActiveBeam !== undefined
        && topVisibleBeamKey !== topActiveBeamKey // if current displayed grainularity is already the highest grainularity active (ie. top most active beam in navigator), block from drilling down further
      ){
        const { x: timestamp } = point
        const timeSize = topActiveBeam.beamSize * 0.5 // For user fills, zoom directly to highest active granularity (whether or not beam is visible)
        this.drilldownToTimeRange(
          timestamp - timeSize / 2, // center zoom around fill's timestamp
          timeSize,
        )
      }
    }

    handleCandlestickClick = (
      { point }: HcClickEvent // requires 'highcharts-custom-events'
    ) => {
      console.log(`%chandleCandlestickClick`,'background:orange')
      //-----------------
      // use current top most visible slider (beam) to determine time size of currently displayed candlestick bar in chart (target of click)
      const { topVisibleBeamKey, topActiveBeamKey } = this.props as HocProps
      if(
        point !== undefined
        && topVisibleBeamKey !== topActiveBeamKey // if current displayed candlestick grainularity is already the highest grainularity active (ie. top most active beam in navigator), block from drilling down further
      ){
        const barSize = getCandlestickBarSize(topVisibleBeamKey)
        const { dataGroup, x: timestamp } = point
        const numBarsInDataGroup = dataGroup === undefined ? 1 : dataGroup.length
        const candleTimeSize = barSize * numBarsInDataGroup
        const candleTimePadding = candleTimeSize * 0.5
        // FIXME: on drill down click, some times high chart is off, eg. BAC drilldown from DAY on candlestick 11:22 - 11:23, x value comes in as 11:20. FOR NOW, adding a time padding so that drill down is approximate and thus obscures any inaccuracies with actual non-padded time range...
        this.drilldownToTimeRange(
          timestamp,
          candleTimeSize,
          candleTimePadding,
        )
      }
    }

    drilldownToTimeRange(timeStart: number, timeSize: number, padding: number = 0,){
      if(timeSize > 0){
        const { navBeams, selectedDayStartUtc, activeBeamKeys } = this.props
        const timeStartRel = timeStart - selectedDayStartUtc - padding
        // console.log('barSize, numBarsInGroup',barSize/ 1000, numBarsInGroup)
        const timeEndRel = timeStartRel + timeSize + padding
        this.props.drillDownToView(
          navBeams,
          activeBeamKeys,
          timeStartRel,
          timeEndRel,
        )
      }
    }

    interceptZoomActions = (chartInstance: Chart) => {
      const { container } = chartInstance

      container.addEventListener('wheel', (e: WheelEvent) => {
        e.preventDefault()
        const { wheelDeltaY } = e

        // TODO: implement better debounce feature!!!
        if (this.debounceZoom) {
          return // exit
        } else {
          this.debounceZoom = true
          setTimeout(() => this.debounceZoom = false, 66)
        }

        const { topLvlBeamStartAbs, topLvlRiderStartAbs, topLvlRiderEndAbs, topLvlBeamEndAbs, topVisibleBeamKey, navBeams, beamContainerW, activeBeamKeys } = this.props


        const activeBeamsSet = selectBeamsByKeys(navBeams, activeBeamKeys)

        const supression = 0.2 // prevent from zooming too fast
        const ZoomInOut = wheelDeltaY <= 0 ? 'IN' : 'OUT'
        // console.log('wheelDeltaY', wheelDeltaY, ZoomInOut)
        const xTravelMouse = Math.abs(wheelDeltaY) * supression
        const refValSize = topLvlBeamEndAbs - topLvlBeamStartAbs // for zooming should be based on beam size, which is fixed, thus giving us a consistent zooming rate. If instead we used rider size as refrence, zooming rate will fluctate and feedback on to itself as rider size changes on the fly while zooming.
        const MIN_RIDER_SIZE_ZOOMABLE = refValSize * 0.08 // prevent zoom in too far

        const xTravelInTimeSeriesValue = Math.floor(convertPxDeltaToValDelta(xTravelMouse, beamContainerW, refValSize))
        const riderStart = topLvlRiderStartAbs - topLvlBeamStartAbs
        const riderEnd = riderStart + (topLvlRiderEndAbs - topLvlRiderStartAbs)
        // console.log('xTravelInTimeSeriesValue',xTravelInTimeSeriesValue, minRangeSizeAfterZoom)
        let cappedRiderStart = riderStart,
          cappedRiderEnd = riderEnd
        switch (ZoomInOut) {
          case 'IN': // wheel up, zoom IN...
            cappedRiderStart -= xTravelInTimeSeriesValue / 2
            cappedRiderEnd += xTravelInTimeSeriesValue / 2
            break;
          case 'OUT': // wheel down, zoom OUT...
            cappedRiderStart += xTravelInTimeSeriesValue / 2
            cappedRiderEnd -= xTravelInTimeSeriesValue / 2
            break;
        }
        if (cappedRiderEnd - cappedRiderStart < MIN_RIDER_SIZE_ZOOMABLE) { // zoomed in too far, past smallest rider size allowed...

          // drill down to next level granularity
          const topActiveBeamIndex = selectBeamIndexByKey(activeBeamsSet, topVisibleBeamKey)
          const beamAboveTopActiveBeam = activeBeamsSet[topActiveBeamIndex-1]
          if(beamAboveTopActiveBeam !== undefined){ // if still finer granularities to zoom into...
            // ------------------
            // ZOOM INTO FINER GRAIN
            // ------------------
            this.props.selectTimeGranularity(
              navBeams,
              activeBeamKeys,
              beamAboveTopActiveBeam.key, {
                riderStart: 0,
                riderEnd: 0 + beamAboveTopActiveBeam.beamSize,
                riderSize: beamAboveTopActiveBeam.beamSize,
              },
            )
            return;
          } else { // if this is the finest granularity available, then do not act on zoom event past smallest rider size allowed...
            return;
          }
        }

        const shouldUpdateAbsBeamVals = false
        const shouldUpdateAbsRiderVals = true

        this.props.moveRiderStartEnd(
          NavUpdateType.ZOOM_ON_CHART,
          navBeams,
          activeBeamKeys,
          topVisibleBeamKey,
          cappedRiderStart,
          cappedRiderEnd,
          shouldUpdateAbsBeamVals,
          shouldUpdateAbsRiderVals,
        )
      })
    }

    interceptPanActions = (chartInstance: Chart) => {
      const { container } = chartInstance
      container.addEventListener('mousedown', (e) => {
        this.mousePos.x = e.clientX
        this.setState({ isMouseDown: true })
      })
      container.addEventListener('mouseup', () => {
        this.resetMouseStates()
      })
      container.addEventListener('mouseleave', () => {
        this.resetMouseStates()
      })
      container.addEventListener('mousemove', (e) => {
        const { isMouseDown } = this.state


        // TODO: implement better debounce feature!!!
        if (this.debouncePan) {
          return // exit
        } else {
          this.debouncePan = true
          setTimeout(() => this.debouncePan = false, 66)
        }

        if (isMouseDown) {
          const { topLvlBeamStartAbs, topLvlRiderStartAbs, topLvlRiderEndAbs, topVisibleBeamKey, beamContainerW, navBeams, activeBeamKeys } = this.props
          const xTravelMouse = (e.clientX - this.mousePos.x) * -1

          if (Math.abs(xTravelMouse) < config.MIN_MOUSE_MOVE_PX_THRESHOLD) return; // do not trigger actions on mouse move less than threshold

          const refValSize = topLvlRiderEndAbs - topLvlRiderStartAbs // should use rider size as reference, since panning action is conducted over the rendered portion of the chart, that with of the area reflects the rider size of the top level beam, and NOT its beam size (which is the entire available data min max range)
          const xTravelInTimeSeriesValue = Math.floor(convertPxDeltaToValDelta(xTravelMouse, beamContainerW, refValSize))

          const riderStart = topLvlRiderStartAbs - topLvlBeamStartAbs
          const riderEnd = riderStart + (topLvlRiderEndAbs - topLvlRiderStartAbs)
          // console.log('mouse travel---->', xTravelMouse, riderStart, riderEnd, xTravelInTimeSeriesValue)
          this.props.moveRiderStartEnd(
            NavUpdateType.PAN_ON_CHART,
            navBeams,
            activeBeamKeys,
            topVisibleBeamKey,
            riderStart + xTravelInTimeSeriesValue,
            riderEnd + xTravelInTimeSeriesValue,
            false, // update top beam vals
            true, // update top rider vals
          )

          this.mousePos.x = e.clientX // increment X position per update, otherwise increasing mouse travel value will be applied to rider value per mousemove event.
        }
      })
    }

    setRef = (el: Chart) => this.chartInstance = el

    render() {
      // console.log('%c render @ MouseInterceptHOC ', Colors.LOG_REACT)
      const {
        children,
        ...restProps
      } = this.props
      return (
        <WrappedComponent
          passRefUpstream={this.setRef}
          onCandlestickClick={this.handleCandlestickClick}
          onUserFillsClick={this.handleUserFillsClick}
          {...restProps as PassThroughProps}
          >
          {children}
        </WrappedComponent>
      )
    }
  }

  type StoreProps = {
    navBeams: INavBeam[]
    activeBeamKeys: (FEGrainType)[]
    topActiveBeamKey: FEGrainType
    topVisibleBeamKey: FEGrainType
    selectedDayStartUtc: number
    topLvlRiderStartAbs: number
    topLvlRiderEndAbs: number
    topLvlBeamStartAbs: number
    topLvlBeamEndAbs: number
    beamContainerW: number
  }

  const mapStateToProps = ({ navigator }: IApplicationState): StoreProps => ({
    navBeams: navigator.navBeams,
    activeBeamKeys: navigator.activeBeamKeys,
    topActiveBeamKey: navigator.activeBeamKeys[0],
    topVisibleBeamKey: navigator.topVisibleBeamKey,
    selectedDayStartUtc:  navigator.selectedDayStartUtc,
    topLvlBeamStartAbs: navigator.topLvlBeamStartAbs,
    topLvlBeamEndAbs: navigator.topLvlBeamEndAbs,
    topLvlRiderStartAbs: navigator.topLvlRiderStartAbs,
    topLvlRiderEndAbs: navigator.topLvlRiderEndAbs,
    beamContainerW: navigator.beamContainerW,
  })

  type DispatchProps = {
    moveRiderStartEnd: IMoveRiderStartEndAction
    selectTimeGranularity: SetTopVisibleBeamAction
    drillDownToView: IDrillDownToViewAction
  }

  const mapDispatchToProps = (dispatch: Dispatch): DispatchProps => ({
    moveRiderStartEnd: createMoveRiderStartEndAction(dispatch),
    selectTimeGranularity: createSetTopVisibleBeamAction(dispatch),
    drillDownToView: createDrillDownToViewAction(dispatch),
  })

  const ConnectedHoc = connect(
    mapStateToProps,
    mapDispatchToProps
  )(MouseInterceptHOC)

  return ConnectedHoc
}



