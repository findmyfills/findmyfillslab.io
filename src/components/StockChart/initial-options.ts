import config from '../../app.config';
import { Colors } from '../../constants';
import { HcAxisType, HcDashStyleType } from '../../types';
import { toPctString } from '../../util';


export default {
  chart: {
    panning: false,
    marginLeft: 0,
    marginRight: 0,
    backgroundColor: Colors.TRANSPARENT,
    events:{

    }
  },
  boost: {
    enabled: true,
    useGPUTranslations: true,
    usePreAllocated: true
  },
  title: {
    text: '',
    align: 'left',
  },
  credits: {
    enabled: false
  },
  time: {
    timezoneOffset: 0,
    useUTC: true,
  },
  rangeSelector: {
    enabled: false,
    inputEnabled: false,
  },
  navigator: {
    enabled: false
  },
  plotOptions:{
    scatter: { // http://jsfiddle.net/highcharts/wd48qr92/1/
      tooltip: {
        headerFormat: '<b>{series.name}</b><br>',
        // pointFormat: '{point.x} cm, {point.y} kg',
        // xDateFormat: '%H:%M:%S.%L',
      },
    },
  },
  tooltip: {
    positioner: function () {
        return { x: 10, y: 50 };
    },
    shadow: false,
    borderWidth: 0,
    backgroundColor: Colors.TOOLTIP_BACKGROUND,
    // valueDecimals: 2
  },
  scrollbar: { enabled: false },
  lang: {
    noData: "no data to display"
  },
  noData: {
    style: {
      fontWeight: 'bold',
      fontSize: '15px',
    }
  },

  yAxis:[{
    visible: true,
    labels: {
      enabled: true, align: "right", x:0,
      style: { color: Colors.ACCENT_1 },
    }, // label settings needs to be locked in that on jumping between granularities, recalculation of yAxis values doesn't reset label positions and result in buggy display
    id: HcAxisType.Y_AXIS_DEFAULT,
    height: toPctString(1.0 - config.HC_VOL_BAR_HEIGHT_PCT - 0.05),
    gridLineDashStyle: HcDashStyleType.Dot,
    gridLineColor: Colors.HC_YAXIS_GRID_LINE,
  }, {
    visible: true,
    labels: {
      enabled: true, align: "right", x:0,
      style: { color: Colors.ACCENT_1 },
    },
    id: HcAxisType.Y_AXIS_VOLUME,
    gridLineDashStyle: HcDashStyleType.Dot,
    gridLineColor: Colors.HC_YAXIS_GRID_LINE,
    // title: { text: 'Volume' },
    offset: 0,
    top: toPctString(1.0 - config.HC_VOL_BAR_HEIGHT_PCT),
    height: toPctString(config.HC_VOL_BAR_HEIGHT_PCT),
  }],
  xAxis: {
    visible: true,
    showEmpty: true,
    type: 'datetime',
    ordinal: false,
    lineColor: Colors.ACCENT_1,
    lineWidth: 2,
    labels: {
      style: { color: Colors.ACCENT_1 },
    },
    // scrollbar: { enabled: false },
    events: {
      // setExtremes: this.handleNavigatorEvent,
      // afterSetExtremes: (e) => console.log('afterSetExtremes', formatUnixUTC(e.min), formatUnixUTC(e.max))
      // setExtremes: (e) => console.log('setExtremes', formatUnixUTC(e.min), formatUnixUTC(e.max))
      // afterSetExtremes: this.handleAfterSetExtremes
    },
    // min: this.props.xAxisTimeMin,
    // max: this.props.xAxisTimeMax,
  },
  // series: [],
}