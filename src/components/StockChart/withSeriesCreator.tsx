import React from 'react';
import { connect } from 'react-redux';
import { Colors } from '../../constants';
import { PayloadCsvSummary } from '../../models';
import { findBEGrainTypeForFEGrainType, findLabelForCsvSummaryKey, shouldRenderVolumeAxisForBEGrainType } from '../../records';
import { IApplicationState, selectDataSliceKeys, SymbolDataState, UserFillsState } from '../../reducers';
import { HcDashStyleType, HcPlotLine } from '../../types';
import { areStrArraysIdentical } from '../../util';
import { createHcScatterSeries, createSeriesOptnFromKeys } from './util';



export function withSeriesCreator<PassThroughProps>(
  WrappedComponent: React.ComponentType<PassThroughProps>
): React.ComponentType<PassThroughProps> {

  type HocProps = ConsumedStoreProps

  type HocState = {
    seriesOptns: any[]
    yAxisPlotLines: HcPlotLine[]
  }

  class DataManagerHOC extends React.Component<PassThroughProps & HocProps, HocState> {
    constructor(props: PassThroughProps & HocProps) {
      super(props);
      this.state = {
        ...this.createNewSeriesDataAndPlotline(
          props.applicableDataKeys,
          props.selectedFmfCsvKey,
          props.symbolData,
          props.fillsMetrics,
        )
      };
    }

    componentWillReceiveProps(nextProps: ConsumedStoreProps) {
      const { applicableDataKeys, selectedFmfCsvKey, symbolData, fillsMetrics } = nextProps

      if( // TODO: need more precise way to filter out unnecessary unpdates
        symbolData !== this.props.symbolData
        || fillsMetrics !== this.props.fillsMetrics
        || selectedFmfCsvKey !== this.props.selectedFmfCsvKey
        || !areStrArraysIdentical(applicableDataKeys, this.props.applicableDataKeys)
      ){

        if(applicableDataKeys.length > 0){
          this.setState({
            ...this.createNewSeriesDataAndPlotline(
              applicableDataKeys,
              selectedFmfCsvKey,
              symbolData,
              fillsMetrics,
            )
          })
        }
      }
    }

    createNewSeriesDataAndPlotline(
      applicableDataKeys: string[],
      selectedFmfCsvKey: string,
      symbolData: SymbolDataState,
      fillsMetrics: UserFillsState,
    ){
      const { onCandlestickClick, onUserFillsClick } = this.props

      const seriesOptns = createSeriesOptnFromKeys(symbolData,applicableDataKeys, onCandlestickClick)
      const selectedFmfCsvMetrics = fillsMetrics[selectedFmfCsvKey]

      let yAxisPlotLines: HcPlotLine[] = [] // highchart plotline options from FMF fills summary metrics
      if(selectedFmfCsvMetrics !== undefined){
        const { summary, fillsAsHcSeriesData } = selectedFmfCsvMetrics
        const summaryMetricsKeys: (keyof PayloadCsvSummary)[] = ['avg_price', 'arrival', 'vwap']
        yAxisPlotLines = summaryMetricsKeys.map((metricsKey)=>{
          return {
            id: metricsKey,
            color: Colors.HC_YAXIS_PLOTLINE,
            value: summary[metricsKey] / 10000,
            width: 1,
            dashStyle: HcDashStyleType.Dot,
            zIndex: 9999,
            label: {
              text: findLabelForCsvSummaryKey(metricsKey),
              style: {
                color: Colors.HC_YAXIS_PLOTLINE,
                fontWeight: 600,
              }
            }
          }
        })
        seriesOptns.push(
          createHcScatterSeries(
            fillsAsHcSeriesData,
            selectedFmfCsvKey,
            onUserFillsClick,
          )
        )
      }

      console.log('%c ----- withSeriesCreator seriesOptns / yAxisPlotLines ----- ', Colors.LOG_HC)
      console.log(applicableDataKeys)
      console.log(seriesOptns)
      console.log(yAxisPlotLines)

      return {
        seriesOptns,
        yAxisPlotLines,
      }
    }

    render() {
      // console.log('%c render @ DataManagerHOC ', Colors.LOG_REACT)
      const {
        // props to omit from children
        applicableDataKeys,
        selectedFmfCsvKey,
        fillsMetrics,
        symbolData,
        onCandlestickClick,
        onUserFillsClick,
        // props to pass down
        shouldRenderVolumeAxis,
        children,
        ...passThroughProps
      } = this.props
      const { seriesOptns, yAxisPlotLines } = this.state
      return (
        <WrappedComponent
          seriesOptns={seriesOptns}
          yAxisPlotLines={yAxisPlotLines}
          shouldRenderVolumeAxis={shouldRenderVolumeAxis}
          {...passThroughProps as PassThroughProps}
          >
          {children}
        </WrappedComponent>
      )
    }
  }


  type ConsumedStoreProps = { // consumed locally, not passed down
    shouldRenderVolumeAxis: boolean
    applicableDataKeys: string[]
    selectedFmfCsvKey: string
    fillsMetrics: UserFillsState
    symbolData: SymbolDataState
    onCandlestickClick: Highcharts.SeriesClickCallbackFunction
    onUserFillsClick: Highcharts.SeriesClickCallbackFunction
  }

  function createMapStateToProps(initialState: HocState, initialOwnProps: HocProps) {
    const { onCandlestickClick, onUserFillsClick } =  initialOwnProps // this is a hack to get around accessing ownProps in mapStateToProps, which prevents redux optimization ...
    return function mapStateToProps({
      navigator, appControls, symbolData, userFills: fillsMetrics
    }: IApplicationState): ConsumedStoreProps {
      const dayStart = navigator.selectedDayStartUtc
      const frontendGrainType = navigator.topVisibleBeamKey
      const backendGrainType = findBEGrainTypeForFEGrainType(frontendGrainType)
      const selectedSymbolKey = appControls.selectedSymbolKey
      const seriesDataTimeMin = dayStart + navigator.topLvlBeamStartAbs
      const seriesDataTimeMax = dayStart + navigator.topLvlBeamEndAbs

      const applicableDataKeys = selectDataSliceKeys(symbolData, selectedSymbolKey, backendGrainType, seriesDataTimeMin, seriesDataTimeMax)
      // console.log('params -->', selectedSymbolKey, backendGrainType, seriesDataTimeMin, seriesDataTimeMax)

      return {
        shouldRenderVolumeAxis: shouldRenderVolumeAxisForBEGrainType(backendGrainType),
        applicableDataKeys,
        selectedFmfCsvKey: appControls.selectedFmfCsvKey,
        fillsMetrics,
        symbolData,
        onCandlestickClick,
        onUserFillsClick,
      }
    }
  }

  return connect(
    createMapStateToProps,
  )(DataManagerHOC)

}

