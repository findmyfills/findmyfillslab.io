import { Chart } from 'highcharts';
import HighchartsMore from 'highcharts/highcharts-more';
import Highcharts from 'highcharts/highstock';
import HcDragPanes from 'highcharts/modules/drag-panes';
import React from 'react';
import { connect } from 'react-redux';
import { Colors } from '../../constants';
import { PayloadCsv } from '../../models';
import { findBEGrainTypeForFEGrainType } from '../../records';
import { IApplicationState } from '../../reducers';
import { HcPlotLine, IHcSeries } from '../../types';
import { formatUnix, getLast } from '../../util';
import initialOptions from './initial-options';
import { createChartTitle, isXAxisRangeWithinDataBounds, toggleVolumeYAxis } from './util';
export * from './withMouseIntercepts';
export * from './withSeriesCreator';


/*
https://github.com/highcharts/highcharts/issues/10588#issuecomment-486180197
the code runs twice and we don't want it to initialize the modules on server side, so add a check and all should be fine. This code for index.js runs fine in your app:
*/
if (typeof Highcharts === 'object') {
  HighchartsMore(Highcharts)
  HcDragPanes(Highcharts)
  // HcCustomEvents(Highcharts)
  // Boost(Highcharts)
  Highcharts.setOptions({
    lang: {
      thousandsSep: ',' // must be set here and NOT in the options object
    }
  })
}


export type Props = {
  containerW: number
  containerH?: number
  // withMouseIntercepts
  passRefUpstream: (el: Chart) => void
  // withSeriesCreator
  seriesOptns: IHcSeries[]
  yAxisPlotLines: HcPlotLine[]
  shouldRenderVolumeAxis: boolean
} & StoreProps


type State = {
  chartOptions: any
}

class StockChart extends React.Component<Props, State> {

  chartInstance: Chart | null
  chartContainer: Element | null

  constructor(props: Props) {
    super(props)
    this.chartInstance = null
    this.chartContainer = null
    // initialOptions.chart.events.load = (e, callback) => {
    //   console.log('%chighchart finished loading','color:purple')
    // }
    this.state = Object.assign({}, {
      chartOptions: initialOptions,
    })
  }

  componentDidMount() {
    this.initChart()
    this.fitChartToContainerDiv()
  }

  componentWillReceiveProps(nextProps: Props) {
    const { containerW, containerH, seriesOptns, xAxisTimeMin, xAxisTimeMax, seriesDataTimeMin, timeZoneOffsetMin, seriesDataTimeMax, backendGrainType, yAxisPlotLines, shouldRenderVolumeAxis, selectedFmfCsvMetrics, navVisibleBeamsCount } = nextProps

    if(navVisibleBeamsCount !== this.props.navVisibleBeamsCount){
      this.fitChartToContainerDiv()
    }

    if(
      selectedFmfCsvMetrics !== this.props.selectedFmfCsvMetrics
      && (
        selectedFmfCsvMetrics === undefined
        || this.props.selectedFmfCsvMetrics === undefined
      )
    ){
      this.fitChartToContainerDiv()
    }

    if(yAxisPlotLines !== this.props.yAxisPlotLines){
      this.updateYAxisPlotLines(yAxisPlotLines)
    }

    if(timeZoneOffsetMin !== this.props.timeZoneOffsetMin){
      this.updateTimezoneOffset(timeZoneOffsetMin)
    }

    if (
      containerH !== this.props.containerH
      || containerW !== this.props.containerW
    ) {
      this.resizeChart(containerW, containerH)
    }

    if (seriesOptns !== this.props.seriesOptns) {
      this.updateSeries(seriesOptns, xAxisTimeMin, xAxisTimeMax, shouldRenderVolumeAxis)
    }

    if (backendGrainType === this.props.backendGrainType){
      if(
        xAxisTimeMin !== this.props.xAxisTimeMin
        || xAxisTimeMax !== this.props.xAxisTimeMax
      ) { // When time range changes while granularity of backend data remains the same, series data itself does not change.  Only x-axis min/max requires updating in this case...
        this.updateXAxisMinMax(xAxisTimeMin, xAxisTimeMax, seriesDataTimeMin, seriesDataTimeMax)
      }
    }

  }

  fitChartToContainerDiv(){
    const { chartInstance: chart } = this
    if (chart !== null) {
      setTimeout(() => chart.reflow(), 200) // reflow the chart to fit the width of the container div.
    }
  }

  updateYAxisPlotLines(yAxisPlotLines: HcPlotLine[]){
    const { chartInstance: chart } = this
    if (chart !== null) {
      chart.yAxis[0].update({ // default axis at index 0
        plotLines: yAxisPlotLines
      }, true)
    }
  }

  updateTimezoneOffset(timeZoneOffsetMin: number){
    const { chartInstance: chart } = this
    if (chart !== null) {
      chart.update({
        time: {
          timezoneOffset: timeZoneOffsetMin
        }
      }, true)
    }
  }

  initChart() {
    const { passRefUpstream, containerW, containerH } = this.props
    const { chartOptions } = this.state
    const { chartContainer } = this
    if (chartContainer !== null) {
      chartOptions.chart.renderTo = chartContainer
      chartOptions.chart.width = containerW
      // chartOptions.chart.height = containerH
      this.chartInstance = Highcharts.stockChart(chartOptions)
      passRefUpstream(this.chartInstance)

      chartContainer.addEventListener('resize',()=>{
        console.log('chartContainer RESIZZZZEEEE !', chartContainer.clientHeight)
      })

    }
  }

  resizeChart(containerW: number, containerH: number = 400) {
    const { chartInstance: chart } = this
    if (chart !== null) {
      chart.setSize( containerW, undefined, false)
    }
  }

  updateLoadingState(isLoading: boolean){
    const { chartInstance: chart } = this
    if (chart !== null) {
      if(isLoading) chart.showLoading()
      else chart.hideLoading()
    }
  }

  updateChartTitle(symbol: string){
    const { chartInstance: chart } = this
    if (chart !== null) {
      chart.setTitle({ text: createChartTitle(symbol) })
    }
  }

  updateXAxisMinMax(xMin: number | undefined, xMax: number | undefined, dataMin: number, dataMax: number) {
    const { chartInstance: chart } = this
    if (
      chart !== null
      && xMin !== undefined
      && xMax !== undefined
    ) {
      if (isXAxisRangeWithinDataBounds(xMin, xMax, dataMin, dataMax)) {
        // If x axis min/max is WITHIN data boundary...
        const xAxis = chart.xAxis[0]
        if (xMin !== xAxis.min || xMax !== xAxis.max) {
          console.log(`%c Update Range | xAxisMinMax: ${formatUnix(xMin)} ${formatUnix(xMax)}, | dataMinMax: ${formatUnix(dataMin)} ${formatUnix(dataMax)} `, Colors.LOG_HC)
          xAxis.update({ min: xMin, max: xMax }, false)
          chart.redraw(false)
        }
      } else {
        // If x axis min/max is OUTSIDE data boundary...

      }
    }
  }

  updateSeries(
    seriesOptns: IHcSeries[],
    xAxisMin: number,
    xAxisMax: number,
    shouldRenderVolumeAxis: boolean,
  ){
    const { chartInstance: chart } = this
    if (chart !== null) {
      console.log(`%c Update ${seriesOptns.length} Data Series | xAxisMinMax: ${formatUnix(xAxisMin)} ${formatUnix(xAxisMax)} `, Colors.LOG_HC)
      // console.log('chartInstance.series.length',chartInstance.series.length)
      // console.log('chartInstance.series[0]',chartInstance.series[0])
      // console.log('type, min, max', type, min, max)

      // Update series objects...
      // trim off exiting series, if no longer exists in new view
      while (chart.series.length > seriesOptns.length) {
        getLast(chart.series).remove(false)
      }
      seriesOptns.forEach((seriesOp, i) => {
        // if(shouldRenderVolumeAxis === false && seriesOp.yAxis === HcAxisType.Y_AXIS_VOLUME){ // Check if any incoming series requires a separate y-axis for volume bars...
        //   shouldRenderVolumeAxis = true
        // }

        if (chart.series[i] !== undefined) { // if series exists...
          chart.series[i].update(seriesOp, false)
        } else {
          chart.addSeries(seriesOp, false)
        }
      })
      // chart.redraw(false)

      // Update y-axis...
      const [ yAxisDefault, yAxisVolume ] = chart.yAxis
      toggleVolumeYAxis(yAxisDefault, yAxisVolume, shouldRenderVolumeAxis)

      // Update x-axis...
      chart.xAxis[0].update({
        min: xAxisMin,
        max: xAxisMax,
        // maxRange: xAxisMax - xAxisMin,
      }, false)
      chart.redraw(false)
    }
  }

  render() {
    // console.log('%c render @ StockChart', Colors.LOG_REACT)
    return (
      <div id="container-highstock"
        ref={ref => this.chartContainer = ref}
        />
    )
  }
}


type StoreProps = {
  selectedFmfCsvMetrics: PayloadCsv | undefined
  navVisibleBeamsCount: number
  xAxisTimeMin: number
  xAxisTimeMax: number
  backendGrainType: string
  seriesDataTimeMin: number
  seriesDataTimeMax: number
  timeZoneOffsetMin: number
}

const mapStateToProps = ({navigator, appControls, userFills: fillsMetrics}: IApplicationState): StoreProps => ({
  selectedFmfCsvMetrics: fillsMetrics[appControls.selectedFmfCsvKey],
  navVisibleBeamsCount: navigator.visibleBeamKeys.length,
  xAxisTimeMin: navigator.selectedDayStartUtc + navigator.topLvlRiderStartAbs,
  xAxisTimeMax: navigator.selectedDayStartUtc + navigator.topLvlRiderEndAbs,
  backendGrainType: findBEGrainTypeForFEGrainType(navigator.topVisibleBeamKey),
  seriesDataTimeMin: navigator.selectedDayStartUtc + navigator.topLvlBeamStartAbs,
  seriesDataTimeMax: navigator.selectedDayStartUtc + navigator.topLvlBeamEndAbs,
  timeZoneOffsetMin: appControls.timeZoneOffsetMin,
})

export default connect(
  mapStateToProps,
)(StockChart)
