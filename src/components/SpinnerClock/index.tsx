import React from 'react';
import AnimateOnChange from 'react-animate-on-change';
import { formatUnix, padDigit } from '../../util';

export class SpinnerDigits extends React.PureComponent<{ digitText: string, id:string }> {

  render() {
    const { digitText } = this.props
    return (
      <AnimateOnChange
        baseClassName='spinner-clock-digits'
        animationClassName='spinner-clock-digits--flash'
        animate={true}
        id={this.props.id}
        >
        <span className="bp3-tag bp3-minimal bp3-large">
          {digitText}
        </span>
      </AnimateOnChange>
    )
  }
}



type Props = {
  unix: number
  tz?: string
}

class SpinnerClock extends React.PureComponent<Props> {

  static unixToString = (
    unix: number,
    tz: string = 'ET'
  ) => {
    return formatUnix(unix, tz).split(':').map(str => padDigit(+str))
  }

  render(){
    const { unix, tz } = this.props
    const digits = SpinnerClock.unixToString(unix, tz)
    return (
      <div className="spinner-clock-container">
        <SpinnerDigits id="digit-hr" digitText={digits[0]} />
        <span className="separator">:</span>
        <SpinnerDigits id="digit-min" digitText={digits[1]} />
        <span className="separator">:</span>
        <SpinnerDigits id="digit-sec" digitText={digits[2]} />
      </div>
    )
  }
}

export default SpinnerClock