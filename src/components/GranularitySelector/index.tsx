import { Button, Checkbox, Classes, FormGroup } from '@blueprintjs/core';
import React from 'react';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { createSelectActiveBeamKeysAction, SelectActiveBeamKeysAction } from '../../actions';
import { IApplicationState } from '../../reducers';
import { INavBeam } from '../../schemas/navigator';
import { FEGrainType } from '../../types';
import { areStrArraysIdentical } from '../../util';


type Props = {

} & StoreProps & DispatchProps

type State = {
  selectedGrainTypes:  Array<FEGrainType>
  didSelectionChange: boolean
}


class GranularitySelector extends React.Component<Props, State>{

  constructor(props: Props) {
    super(props)
    this.state = {
      selectedGrainTypes: props.activeGrainTypes.slice(),
      didSelectionChange: false,
    }
  }

  componentWillReceiveProps(nextProps: Props) {
    // const {  activeGrainTypes } = nextProps
    // if(activeGrainTypes !== this.props.activeGrainTypes){
    //   this.setState({
    //     selectedGrainTypes: activeGrainTypes.slice(),
    //   })
    // }
  }

  handleGrainSelect = (grainType: FEGrainType, shouldSelect: boolean) => {
    const { activeGrainTypes } = this.props
    const { selectedGrainTypes } = this.state

    let updatedSelections: (FEGrainType)[]
    if(shouldSelect){ // If SELECT action...
      updatedSelections = selectedGrainTypes.concat([ grainType ])
    } else { // If DE-SELECT action...
      if(selectedGrainTypes.length > 1){
        updatedSelections = selectedGrainTypes.filter(key => key !== grainType)
      } else {
        return; // keep at least one grain type selected...
      }
    }

    this.setState({
      selectedGrainTypes: updatedSelections,
      didSelectionChange: !areStrArraysIdentical(activeGrainTypes, updatedSelections)
    })

  }

  handleConfirmGrainSelection = () => {
    const { possibleTimeGrains } = this.props
    const { selectedGrainTypes } = this.state
    this.props.selectActiveTimeGrains(possibleTimeGrains, selectedGrainTypes)
  }

  renderGranularityOptions(){
    const { possibleTimeGrains } = this.props
    const { selectedGrainTypes } = this.state

    return possibleTimeGrains.map((grain)=> {
      const isSelected = selectedGrainTypes.find(key => key === grain.key) !== undefined
      return (
        <Checkbox
          key={grain.key}
          checked={isSelected}
          label={grain.label}
          disabled={grain.key === FEGrainType.DAY}
          onChange={() => this.handleGrainSelect(grain.key, !isSelected)}
          />
      )
    })
  }

  renderCommitSelectionButton(){
    const { didSelectionChange } = this.state
    return didSelectionChange ? (
      <div className="grain-selector-commit-change-buttons">
        <Button
          className={Classes.POPOVER_DISMISS}
          icon="tick"
          intent="primary"
          minimal={true}
          onClick={this.handleConfirmGrainSelection}
          />
        <Button
          className={Classes.POPOVER_DISMISS}
          icon="cross"
          intent="danger"
          minimal={true}
          />
      </div>
    ) : null
  }

  render(){
    return (
      <div className="grain-selector-container">
        <FormGroup
          label=""
          labelInfo=""
          >
          {this.renderGranularityOptions()}
        </FormGroup>
        {this.renderCommitSelectionButton()}
      </div>
    )
  }

}

type StoreProps = {
  possibleTimeGrains: INavBeam[]
  activeGrainTypes: Array<FEGrainType>
}

const mapStateToProps = (state: IApplicationState): StoreProps => ({
  activeGrainTypes: state.navigator.activeBeamKeys,
  possibleTimeGrains: state.navigator.navBeams, // IMPORTANT multi-beam navigator interface carries time granularity information!
})

type DispatchProps = {
  selectActiveTimeGrains: SelectActiveBeamKeysAction
}

const mapDispatchToProps = (dispatch: Dispatch): DispatchProps => ({
  selectActiveTimeGrains: createSelectActiveBeamKeysAction(dispatch)
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(GranularitySelector);