import { createUTCDate, getYesterday, rgbToRgba } from '../util';

export const DUR_MILLI = 1
export const DUR_SEC = 1000 * DUR_MILLI
export const DUR_MIN = 60 * DUR_SEC
export const DUR_HR = 60 * DUR_MIN
export const DUR_TRADING_DAY = 6.5 * DUR_HR
export const DUR_DAY = 24 * DUR_HR
export const DURATIONS = [
  DUR_MILLI, DUR_SEC, DUR_MIN, DUR_HR, DUR_TRADING_DAY
]


export const BACKEND_FLOAT_MULTIPLIER = 10000 // to avoid floating point issues, some backend metrics (eg. price) are multiplied by 10000 before serialization

// new Date(Date.UTC(2019, 6, 1, 13, 30, 0, 0)).valueOf()
export const TRADING_DAY_START = createUTCDate(2019, 6, 6, 13, 30, 0, 0).valueOf()
// export const TRADING_DAY_START = createUTCDate(2019, 6, 13, 13, 30, 0, 0).valueOf() // 190701 13:30am UTC or 9:30am ET



export const TRADING_DAY_END = TRADING_DAY_START + DUR_TRADING_DAY

export const DATEPICKER_MIN_DATE = createUTCDate(2019, 1, 2)
export const DATEPICKER_MAX_DATE = getYesterday()




export const MONTH_NAMES_FULL = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"
];





const BLUE_1 = 'rgb(14,90,138)' // #0E5A8A
const BLUE_2 = 'rgb(16,107,163)' // #106BA3
const BLUE_3 = 'rgb(19,124,189)' // #137CBD
const BLUE_4 = 'rgb(43,149,214)' // #2B95D6
const BLUE_5 = 'rgb(72,175,240)' // #48AFF0

const TURQUOISE_1 = 'rgb(0,128,117)'// #008075

const TEAL_1 = 'rgb(10,150,180)' // #0A96B4

const RED_1 =  'rgb(168,42,42)'// #A82A2A
const RED_3 =  'rgb(219,55,55)'// #DB3737
const RED_4 =  'rgb(245,86,86)'// #F55656
const FUSCIA =  'rgb(173, 68, 96)'// #AD4460

const BLACK = 'rgb(34,34,34)' // #222
const WHITE_0 = 'rgb(255,255,255)' // #fff
const WHITE_1 = 'rgb(245,248,250)' //

const GRAY_1 = 'rgb(102,102,102)' // #666
const GRAY_2 = 'rgb(92,112,128)'// #5c7080
const GRAY_3 = 'rgb(140,153,165)' // #8C99A5
const GRAY_4 = 'rgb(204,204,204)' // #ccc
const GRAY_5 = 'rgb(191,204,214)' // #BFCCD6
const GRAY_6 = 'rgb(227,233,237)' // #E3E9ED

const GREEN_1 = 'rgb(70,144,71)' // #469047
const GREEN_2 = 'rgb(13,128,80)' // #0D8050
const GREEN_3 = 'rgb(15,153,96)' // #0F9960
const GREEN_4 = 'rgb(21,179,113)' // #15B371

const SEPIA_5 = 'rgb(201,151,101)'// #C99765


const TRANSPARENT = rgbToRgba(RED_1, 0)

const BP3_DISABLED_BTN_TEXT = 'rgba(92, 112, 128, 0.6)'
const BP3_DISABLED_BTN = 'rgba(206, 217, 224, 0.5)'
const BP3_TABLE_STRIPE = 'rgba(191, 204, 214, 0.15)'
const BP3_ICON = GRAY_2


const VENUE_NYSE_FAM = 'rgb(0, 56, 144)'
const VENUE_NASDAQ_FAM = 'rgb(0, 161, 203)'
const VENUE_CBOE_FAM = 'rgb(0, 173, 20)'
const VENUE_IEX = 'rgb(255, 162, 0)' // #FFA200
const VENUE_CHX = 'rgb(102,102,102)' //
const VENUE_FINRA = BP3_DISABLED_BTN_TEXT //
const VENUE_UNKNOWN = BP3_TABLE_STRIPE //

// const ACCENT_1 = 'rgb(36,89,108)'
const ACCENT_1 = 'rgb(58,86,108)'
const ACCENT_2 = FUSCIA
const CANVAS = 'rgb(250,252,251)'

export const Colors = {
  CANVAS,
  ACCENT_1,
  ACCENT_2,
  LOG_REACT:'background:#282C34;color:#61DAFB',
  LOG_HC:'background:#47475C;color:#EEEAEA',
  LOG_DATA:'background:teal;color:#fff',
  BP3_TABLE: WHITE_1,
  BP3_DISABLED_BTN,
  BP3_DISABLED_BTN_TEXT,
  BP3_ICON,
  TRANSPARENT,
  //-----------------
  // Venue colors
  //-----------------
  VENUE_NYSE_FAM,
  VENUE_NASDAQ_FAM,
  VENUE_CBOE_FAM,
  VENUE_IEX,
  VENUE_CHX,
  VENUE_FINRA,
  VENUE_UNKNOWN,
  //-----------------
  // Multi-Beam Navigator related colors
  //-----------------
  // Preview Graph
  NAV_PREVIEW_PLINE: TEAL_1,
  NAV_PREVIEW_AREA: rgbToRgba(TEAL_1, 0.25),
  NAV_PREVIEW_OHLC_BAR_BLOCK: TEAL_1,
  NAV_PREVIEW_OHLC_BAR: TEAL_1,
  // Beam
  NAV_BEAM_TRACK_MASK:rgbToRgba(GRAY_6, 0.6),
  NAV_BEAM_TRACK: rgbToRgba(GRAY_6, 0.1),
  NAV_BEAM_RGB_VAL_LIGHTEST: 245,
  // Beam helpers
  NAV_STEPARROW: ACCENT_1,
  NAV_SLIDERULE_CURSOR: ACCENT_1,
  NAV_RULER_TEXT: ACCENT_1,
  // Rider (on Beam)
  NAV_RIDER_BAR: TRANSPARENT, // NOTE: must be zero-opacity color so that mouse event still registers
  NAV_RIDER_BAR_MOUSEDOWN: TRANSPARENT,
  NAV_RIDER_HANDLE: ACCENT_1,
  // Child Rider (on Beam)
  NAV_CHILD_RIDER_TRACK: TRANSPARENT, // NOTE: must be zero-opacity color so that mouse event still registers

  //-----------------
  // Highcharts related colors
  //-----------------
  HC_YAXIS_GRID_LINE: GRAY_4,
  TOOLTIP_BACKGROUND: rgbToRgba(WHITE_0, 0.8),
  // Rendering for 'min', 'sec' granularity data...
  HC_CANDLESTICK_UP: rgbToRgba(GREEN_2, 0.65),
  HC_CANDLESTICK_DOWN: rgbToRgba(RED_1, 0.5),
  HC_VOLUME_BAR: ACCENT_1,
  // Rendering for 'full' granularity data...
  HC_AREARANGE_NBBO_STROKE: rgbToRgba(TEAL_1, 0.25),
  HC_AREARANGE_NBBO_FILL: rgbToRgba(TEAL_1, 0.15),
  HC_SCATTER_TRADES: rgbToRgba(TEAL_1, 0.15),
  HC_SCATTER_BLOCKS: rgbToRgba(TEAL_1, 0.15),
  HC_SCATTER_FILLS_MATCHED: ACCENT_2,
  HC_SCATTER_FILLS_UNMATCHED: rgbToRgba(ACCENT_2, 0.5),
  // Csv Fills Metrics...
  HC_YAXIS_PLOTLINE: ACCENT_2,
}



