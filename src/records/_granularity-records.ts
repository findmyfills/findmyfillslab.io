import { DUR_MILLI, DUR_MIN, DUR_SEC } from '../constants';
import { BEGrainType, FEGrainType } from '../types';

export function findBEGrainTypeForFEGrainType(frontendGrainType: FEGrainType){
  return MAP_FE_GRAIN_TO_BE_GRAIN_TYPE[frontendGrainType]
}

export function findDurationForBEGrainType(backendGrainType: BEGrainType){
  return MAP_BE_GRAIN_TYPE_TO_DURATION[backendGrainType]
}

export function shouldRenderVolumeAxisForBEGrainType(backendGrainType: BEGrainType){
  return MAP_BE_GRAIN_TYPE_TO_SHOULD_RENDER_VOLUME_YAXIS[backendGrainType]
}

const MAP_FE_GRAIN_TO_BE_GRAIN_TYPE: {
  [K in FEGrainType]: BEGrainType
}= {
  [FEGrainType.MILLI]: BEGrainType.FULL,
  [FEGrainType.SEC]: BEGrainType.FULL,
  [FEGrainType.MIN]: BEGrainType.SEC,
  [FEGrainType.HR]: BEGrainType.MIN,
  [FEGrainType.DAY]: BEGrainType.MIN,
}



export const MAP_BE_GRAIN_TYPE_TO_DURATION: {
  [K in BEGrainType]: number
} = {
  [BEGrainType.MIN]: DUR_MIN,
  [BEGrainType.SEC]: DUR_SEC,
  [BEGrainType.FULL]: DUR_MILLI,
}

export const MAP_BE_GRAIN_TYPE_TO_SHOULD_RENDER_VOLUME_YAXIS: {
  [K in BEGrainType]: boolean
} = {
  [BEGrainType.MIN]: true,
  [BEGrainType.SEC]: true,
  [BEGrainType.FULL]: false,
}

