import { Dispatch } from "redux";

export * from './_app-controls-actions';
export * from './_navigator-actions';
export * from './_symbol-data-actions';
export * from './_user-fills-actions';


// ----------------------
export const RESET_APP_STATE = 'RESET_APP_STATE'
// ----------------------
export type ResetAppStateAction = () => void
export const createResetAppStateAction = (dispatch: Dispatch): ResetAppStateAction => () => dispatch({
  type: RESET_APP_STATE,
})
