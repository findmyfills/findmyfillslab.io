import { Dispatch } from 'redux';
import config from '../app.config';
import { getBeamBoundsAbs, getRiderBoundsAbs, selectBeamByKey, selectBeamIndex, selectBeamIndexByKey, selectBeamKeysBelowAndIncluding, selectBeamsByKeys } from '../reducers';
import { INavBeam } from '../schemas/navigator';
import { Dir, FEGrainType, NavUpdateType } from '../types';
import { capBtw, getItemsBeforeAfter, getLast, getMMDDYYYY, getTradingDayStart } from '../util';
import { configureNavigatorBeams, centerBeamAndRiderToView, getUpdatedNavBeamsSet } from '../components/MultiBeamNavigator';


// ----------------------
export const SET_NAVIGATOR_DIMENSIONS = 'SET_NAVIGATOR_DIMENSIONS'
// ----------------------
export type SetNavigatorDimensions = (
  navigatorW: number,
  navigatorH: number,
) => void

export const createSetNavigatorDimensions = (dispatch: Dispatch): SetNavigatorDimensions => ( navigatorW, navigatorH ) => dispatch({
  type: SET_NAVIGATOR_DIMENSIONS,
  data: { navigatorW, navigatorH }
})



// ----------------------
export const SET_DAY_START = 'SET_DAY_START'
// ----------------------
export type SetDayStartAction = (
  date: Date,
) => void

export const createSetDayStartAction = (dispatch: Dispatch): SetDayStartAction => (
  date,
) => {
  const [ yyyy, mm, dd ] = getMMDDYYYY(date).split('-')
  const tradingDayStartUtc = getTradingDayStart(yyyy, mm, dd)
  console.log(`%cnew date selected: ${tradingDayStartUtc}`,'background:lightyellow')
  dispatch({
    type: SET_DAY_START,
    data: tradingDayStartUtc,
  })
}



// ----------------------
export const SELECT_ACTIVE_BEAM_KEYS = 'SELECT_ACTIVE_BEAM_KEYS'
// ----------------------
export type SelectActiveBeamKeysAction = (
  navBeams: INavBeam[],
  activeBeamKeys: Array<FEGrainType>,
) => void

export const createSelectActiveBeamKeysAction = (dispatch: Dispatch): SelectActiveBeamKeysAction => (
  navBeams,
  activeBeamKeys
) => {

  dispatch({
    type: SELECT_ACTIVE_BEAM_KEYS,
    data: activeBeamKeys,
  })

  const activeBeamsSet = selectBeamsByKeys(navBeams, activeBeamKeys)
  const botMostBeam = getLast(activeBeamsSet)

  createSetTopVisibleBeamAction(dispatch)(
    navBeams,
    activeBeamKeys,
    botMostBeam.key, // set lowest grain as new top lvl beam
  )
}



// ----------------------
export const SET_NEW_TOP_LVL_BEAM_KEY = 'SET_NEW_TOP_LVL_BEAM_KEY'
// ----------------------

export type ISetNewTopLvlBeamKey = (
  newTopLvlBeamKey: string,
) => void

export const createSetNewTopLvlBeamKey = (dispatch: Dispatch): ISetNewTopLvlBeamKey => (
  newTopLvlBeamKey
) => dispatch({
  type: SET_NEW_TOP_LVL_BEAM_KEY,
  data: newTopLvlBeamKey,
})


// ----------------------
export const UPDATE_NAV_BEAMS_SET = 'UPDATE_NAV_BEAMS_SET'
// ----------------------

export type IUpdateNavBeamsSetAction = (
  navBeams: INavBeam[],
  activeBeamKeys: (FEGrainType)[],
  beamKey: string,
  newRiderStart: number,
  newRiderEnd: number,
  newBeamStart?: number,
) => INavBeam[] | undefined

export const createUpdateNavBeamsSetAction = (dispatch: Dispatch): IUpdateNavBeamsSetAction => (
  navBeams,
  activeBeamKeys,
  beamKey,
  newRiderStart,
  newRiderEnd,
  newBeamStart,
) => {
  const updatedBeams = getUpdatedNavBeamsSet(
    navBeams,
    activeBeamKeys,
    beamKey,
    newRiderStart,
    newRiderEnd,
    newBeamStart,
  )
  dispatch({
    type: UPDATE_NAV_BEAMS_SET,
    data: updatedBeams,
  })

  return updatedBeams
}


// ----------------------
export const SET_NAVIGATOR_VALUES = 'SET_NAVIGATOR_VALUES'
// ----------------------

export type ISetNavAbsValsFromBeamAction = (
  updatedBeam: INavBeam | undefined,
  shouldUpdateAbsBeamVals: boolean,
  shouldUpdateAbsRiderVals: boolean
) => void

export const createSetNavAbsValsFromBeamAction = (dispatch: Dispatch): ISetNavAbsValsFromBeamAction => (
  updatedBeam,
  shouldUpdateAbsBeamVals,
  shouldUpdateAbsRiderVals
) => {
  if(updatedBeam === undefined) return
  let updatedParams
  if (shouldUpdateAbsBeamVals) {
    const absBeamVals = getBeamBoundsAbs(updatedBeam)
    if (updatedParams === undefined) updatedParams = {}
    Object.assign(updatedParams, {
      topLvlBeamStartAbs: absBeamVals.start,
      topLvlBeamEndAbs: absBeamVals.end,
    })
  }
  if (shouldUpdateAbsRiderVals) {
    const absRiderVals = getRiderBoundsAbs(updatedBeam)
    if (updatedParams === undefined) updatedParams = {}
    Object.assign(updatedParams, {
      topLvlRiderStartAbs: absRiderVals.start,
      topLvlRiderEndAbs: absRiderVals.end,
    })
  }
  dispatch({
    type: SET_NAVIGATOR_VALUES,
    data: updatedParams,
  })
}



// ----------------------
// Mutli-action Action Creator...
// ----------------------
export type IMoveRiderStartEndAction = (
  updateType: NavUpdateType,
  navBeams: INavBeam[], // Set of all navigator beams
  activeBeamKeys: (FEGrainType)[],
  beamKey: string, // Key of the beam that the updated rider sits
  newRiderStart: number, // New rider start value (relative) to update to
  newRiderEnd: number, // New rider start value (relative) to update to
  shouldUpdateAbsBeamVals: boolean,
  shouldUpdateAbsRiderVals: boolean
) => void

export const createMoveRiderStartEndAction = (dispatch: Dispatch): IMoveRiderStartEndAction => (
  updateType,
  navBeams,
  activeBeamKeys,
  updatedBeamKey,
  newRiderStart,
  newRiderEnd,
  shouldUpdateAbsBeamVals,
  shouldUpdateAbsRiderVals,
) => {

  const activeBeamsSet = selectBeamsByKeys(navBeams, activeBeamKeys)
  const beamToUpdate = selectBeamByKey(activeBeamsSet, updatedBeamKey)
  if (beamToUpdate === undefined) return

  const { didLeftEdgeOverflow, didRightEdgeOverflow } = checkRiderMovementForBoundaryBreach(beamToUpdate, newRiderStart, newRiderEnd)


  const willMovementBreachBoundary = didLeftEdgeOverflow || didRightEdgeOverflow
  const willMovementBreachBoundaryAtBothEnds = didLeftEdgeOverflow && didRightEdgeOverflow

  switch (updateType) {
    case NavUpdateType.RIDER_BAR_DRAG:
    case NavUpdateType.RIDER_HANDLE_DRAG:
      if (willMovementBreachBoundary){
        // Do nothing, which contains rider handle drag to be kept within bounds of current beam, overshooting this boundary will NOT be respected...
      } else {
        handleBoundSafeRiderMovement(dispatch, navBeams, activeBeamKeys, beamToUpdate, updatedBeamKey, newRiderStart, newRiderEnd, shouldUpdateAbsBeamVals, shouldUpdateAbsRiderVals)
      }
      break;
    case NavUpdateType.ZOOM_ON_CHART:
      if (willMovementBreachBoundaryAtBothEnds){
        // Special handling here when zooming action causes rider to breach bounds on either end simulanteously...
        handleOutOfBoundsZooming(dispatch, navBeams, activeBeamKeys, beamToUpdate)
      } else {
        // Handle rider movement regularly if only one end is out-of-bounds...
        handleBoundSafeRiderMovement(dispatch, navBeams, activeBeamKeys, beamToUpdate, updatedBeamKey, newRiderStart, newRiderEnd, shouldUpdateAbsBeamVals, shouldUpdateAbsRiderVals)
      }
      break;
    case NavUpdateType.PAN_ON_CHART:
    case NavUpdateType.RIDER_STEP_ARROW:
      if (willMovementBreachBoundary){
        handleOutOfBoundsRiderMovement(dispatch, updateType, navBeams, activeBeamKeys, beamToUpdate, newRiderStart, newRiderEnd)
      } else {
        handleBoundSafeRiderMovement(dispatch, navBeams, activeBeamKeys, beamToUpdate, updatedBeamKey, newRiderStart, newRiderEnd, shouldUpdateAbsBeamVals, shouldUpdateAbsRiderVals)
      }
      break;
  }
}

function handleBoundSafeRiderMovement(
  dispatch: Dispatch,
  navBeams: INavBeam[],
  activeBeamKeys: (FEGrainType)[],
  beamToUpdate: INavBeam,
  updatedBeamKey: string,
  riderStart: number,
  riderEnd: number,
  shouldUpdateAbsBeamVals: boolean,
  shouldUpdateAbsRiderVals: boolean
){

  const boundedVals = capRiderOverflow(beamToUpdate, riderStart, riderEnd) // TODO: this capping here is currently needed to prevent zooming action from going out of bounds when only one handle ends hits the edge...

  //------------------------------
  // Update all beams based on rider movement...
  //------------------------------
  const updatedNavBeams = createUpdateNavBeamsSetAction(dispatch)(
    navBeams,
    activeBeamKeys,
    updatedBeamKey,
    boundedVals.boundedRiderStart,
    boundedVals.boundedRiderEnd,
    // riderStart,
    // riderEnd,
  ) // provide access to updated states before it is dispatched to store
  const updatedBeam = selectBeamByKey(updatedNavBeams, updatedBeamKey)
  createSetNavAbsValsFromBeamAction(dispatch)( // TRY HERE TO ALWAY UPDATE VALUE FROM TOP LEVEL BEAM? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ?
    updatedBeam,
    shouldUpdateAbsBeamVals,
    shouldUpdateAbsRiderVals,
  )
}


function handleOutOfBoundsZooming(
  dispatch: Dispatch,
  navBeams: INavBeam[],
  activeBeamKeys: (FEGrainType)[],
  thisBeam: INavBeam,
){
  const activeBeamsSet = selectBeamsByKeys(navBeams, activeBeamKeys)
  const thisBeamIndex = selectBeamIndex(activeBeamsSet, thisBeam)
  const beamBelow = activeBeamsSet[thisBeamIndex + 1]
  if (beamBelow === undefined) return false // IMPORTANT: no beam below indicates rider belongs to the only active beam in navgiator, thus any boundary breaches should not be handled...
  // -- Action --> zoom out to granularity (beam) below
  createSetTopVisibleBeamAction(dispatch)(
    navBeams,
    activeBeamKeys,
    beamBelow.key, // new top level beam
  )
}



function handleOutOfBoundsRiderMovement(
  dispatch: Dispatch,
  updateType: NavUpdateType,
  navBeams: INavBeam[],
  activeBeamKeys: (FEGrainType)[],
  thisBeam: INavBeam,
  newRiderStart: number,
  newRiderEnd: number
) {
  //------------------------------
  // Gather context information required for calculations below...
  //------------------------------
  const activeBeamsSet = selectBeamsByKeys(navBeams, activeBeamKeys)
  const newRiderSize = newRiderEnd - newRiderStart
  const thisBeamIndex = selectBeamIndex(activeBeamsSet, thisBeam)
  const beamBelow = activeBeamsSet[thisBeamIndex + 1]
  const botMostBeam = getLast(activeBeamsSet)

  let isOnlyActiveBeam = false
  if (beamBelow === undefined){
    isOnlyActiveBeam = true
  }

  const navStartAbs = botMostBeam.beamStart
  const navEndAbs = botMostBeam.beamStart + botMostBeam.beamSize
  const riderStartAbsToBe = thisBeam.beamStart + newRiderStart
  const riderEndAbsToBe = thisBeam.beamStart + newRiderStart + newRiderSize
  const { didLeftEdgeOverflow, didRightEdgeOverflow } = checkRiderMovementForBoundaryBreach(thisBeam, newRiderStart, newRiderEnd)

  // console.log(`%c riderStartAbsToBe ${riderStartAbsToBe} | navStartAbs ${navStartAbs}` , 'background:yellow')

  let outOfBoundsDir: Dir
  let willMovementBreachBotBeamAbs: boolean // For bottom most beam (representing navigator absolute min-max boundaries), determine if next step will shoot it past it's bounds...
  if (didLeftEdgeOverflow){
    outOfBoundsDir = Dir.LEFT
    willMovementBreachBotBeamAbs = riderStartAbsToBe < navStartAbs
  } else if (didRightEdgeOverflow){
    outOfBoundsDir = Dir.RIGHT
    willMovementBreachBotBeamAbs = riderEndAbsToBe > navEndAbs
  }

  handleOutOfBoundsRiderMovementLeftRight(
    dispatch,
    updateType,
    navBeams,
    activeBeamKeys,
    thisBeam,
    beamBelow,
    outOfBoundsDir,
    newRiderSize,
    willMovementBreachBotBeamAbs,
    isOnlyActiveBeam
  )
}


function handleOutOfBoundsRiderMovementLeftRight(
  dispatch: Dispatch,
  updateType: NavUpdateType,
  navBeams: INavBeam[],
  activeBeamKeys: (FEGrainType)[],
  thisBeam: INavBeam,
  beamBelow: INavBeam,
  outOfBoundsDir: Dir,
  newRiderSize: number,
  willMovementBreachBotBeamAbs: boolean,
  isOnlyActiveBeam: boolean,
){

  // console.log(`%c willMovementBreachNavAbs ${willMovementBreachNavAbs}`, 'background:red')
  let cappedRiderStart: number
  let cappedRiderEnd: number
  let beanBelowRiderStart: number
  let beanBelowRiderEnd: number
  if (
    isOnlyActiveBeam
    || willMovementBreachBotBeamAbs // If current rider update is about to send rider absolute LEFT past navigator LEFT-most absolute value; or RIGHT past RIGHT-most...
  ){
    switch (outOfBoundsDir) {
      case Dir.LEFT:
        // -- Action --> Pin current rider at LEFT end
        cappedRiderStart = 0
        cappedRiderEnd = 0 + newRiderSize
        break;
      case Dir.RIGHT:
        // -- Action --> Pin current rider at RIGHT end
        cappedRiderStart = thisBeam.beamSize - newRiderSize
        cappedRiderEnd = thisBeam.beamSize
        break;
    }
    // -- Action --> Pin current rider at LEFT or RIGHT end
    createMoveRiderStartEndAction(dispatch)(
      updateType,
      navBeams,
      activeBeamKeys,
      thisBeam.key,
      cappedRiderStart,
      cappedRiderEnd,
      false, // prevent from triggering data call
      true,
    )
  } else {
    // If current rider update is still safe within navigator absolute bounds AND is NOT the only active beam...
    switch (outOfBoundsDir) {
      case Dir.LEFT:
        // -- Action --> Cycle current rider through to RIGHT end
        cappedRiderStart = thisBeam.beamSize - newRiderSize
        cappedRiderEnd = thisBeam.beamSize
        // -- Action --> LEFT step beam rider below
        beanBelowRiderStart = beamBelow.riderStart - beamBelow.riderSize
        beanBelowRiderEnd = beamBelow.riderEnd - beamBelow.riderSize
        break;
      case Dir.RIGHT:
        // -- Action --> Cycle current rider through to LEFT end
        cappedRiderStart = 0
        cappedRiderEnd = 0 + newRiderSize
        // -- Action --> RIGHT step beam rider below
        beanBelowRiderStart = beamBelow.riderStart + beamBelow.riderSize
        beanBelowRiderEnd = beamBelow.riderEnd + beamBelow.riderSize
        break;
    }

    // -- Action --> LEFT or RIGHT step beam rider below conditionally
    createMoveRiderStartEndAction(dispatch)(
      updateType,
      navBeams,
      activeBeamKeys,
      beamBelow.key,
      beanBelowRiderStart,
      beanBelowRiderEnd,
      false, // hold off updating nav beam abs vals
      false, // hold off updating nav rider abs vals
    )

    // -- Action --> Cycle current rider through to LEFT or RIGHT end conditionally
    // IMPORTANT: must update current slider last, since above implementations will update beams below current, which will propagate beam.beamStart values upstream, affecting current beam's beamStart
    createMoveRiderStartEndAction(dispatch)(
      updateType,
      navBeams,
      activeBeamKeys,
      thisBeam.key,
      cappedRiderStart,
      cappedRiderEnd,
      true, // should set abs val because beams jumped
      true, // should set abs val because beams jumped
    )
  }
}



function checkRiderMovementForBoundaryBreach(beam: INavBeam, newRiderStart: number, newRiderEnd: number) {
  const beamEdgeL = 0
  const beamEdgeR = beam.beamSize
  let didLeftEdgeOverflow = false
  let didRightEdgeOverflow = false
  if (newRiderStart < beamEdgeL){ // Rider overflow LEFT side...
    console.log('%cRider left edge overflow','background:pink')
    didLeftEdgeOverflow = true
  }
  if (newRiderEnd > beamEdgeR) { // Rider overflow RIGHT side...
    console.log('%cRider right edge overflow','background:pink')
    didRightEdgeOverflow = true
  }
  return {
    didLeftEdgeOverflow,
    didRightEdgeOverflow,
  }

}

function capRiderOverflow(beam: INavBeam, newRiderStart: number, newRiderEnd: number){
  const newRiderSize = newRiderEnd - newRiderStart
  const boundedRiderSize = capBtw(newRiderSize, 0, beam.beamSize)
  const maxRiderStartAllowed = beam.beamSize - boundedRiderSize
  const boundedRiderStart = capBtw(newRiderStart, 0, maxRiderStartAllowed)
  const boundedRiderEnd = boundedRiderStart + boundedRiderSize
  return {
    boundedRiderStart,
    boundedRiderSize,
    boundedRiderEnd,
  }
}



// ----------------------
// Mutli-action Action Creator...
// ----------------------
export type SetTopVisibleBeamAction = (
  navBeams: INavBeam[],
  activeBeamKeys: (FEGrainType)[],
  newTopLvlBeamKey: string,
  topBeamParamOverride?: Record<string, number>,
) => void

export const createSetTopVisibleBeamAction = (dispatch: Dispatch): SetTopVisibleBeamAction => (
  navBeams,
  activeBeamKeys,
  newTopLvlBeamKey,
  topBeamParamOverride,
) => {
  const beamsSetCopy = navBeams.slice()
  const activeBeamsSet = selectBeamsByKeys(navBeams, activeBeamKeys)

  const newTopLvlBeam = selectBeamByKey(beamsSetCopy, newTopLvlBeamKey)
  if (newTopLvlBeam === undefined) return;

  // Implement param override if provided...
  if (topBeamParamOverride !== undefined){
    Object.assign(newTopLvlBeam, topBeamParamOverride)
  }

  // Re-implement beam params from newly selected beam downwards, keeping riderStart, but reseting riderSize
  const newTopLvlBeamIndex = selectBeamIndexByKey(activeBeamsSet, newTopLvlBeamKey)
  if (newTopLvlBeamIndex !== -1) {
    const {
      itemsAfter: beamsBelowNewTopLvlBeam, // After and including
    } = getItemsBeforeAfter<INavBeam>(activeBeamsSet, newTopLvlBeamIndex)
    // re-configure beam params from newly selected level downwards...
    if(topBeamParamOverride === undefined){ // If NO top level beam param override

      // BELOW: attempt at centering new top level beam rider view on jumping to a below beam...
      // const currentRiderCenter = getBeamRiderCenter(newTopLvlBeam)
      // const newRiderSize = newTopLvlBeam.beamSize * config.DEFAULT_BEAM_RIDER_PCT
      // const {
      //   start: newRiderStart,
      // } = frameRangeAroundCenter(
      //   currentRiderCenter,
      //   newRiderSize
      // )

      configureNavigatorBeams( // newTopLvlBeam params are mutated as part of this configuration step
        beamsBelowNewTopLvlBeam,
        newTopLvlBeam.riderStart,
        newTopLvlBeam.beamSize * config.DEFAULT_BEAM_RIDER_PCT,
        // newRiderStart,
        // newRiderSize,
      )
    } else { // If WITH top level beam param override

      configureNavigatorBeams( // newTopLvlBeam params are mutated as part of this configuration step
        beamsBelowNewTopLvlBeam,
        newTopLvlBeam.riderStart,
        topBeamParamOverride.riderSize
      )
    }
  }

  // must get abs measurements after implementation about, since implementation auto sets top lvl beam rider size...
  const absBeamVals = getBeamBoundsAbs(newTopLvlBeam)
  const absRiderVals = getRiderBoundsAbs(newTopLvlBeam)

  const visibleBeamKeys = selectBeamKeysBelowAndIncluding(activeBeamsSet, newTopLvlBeamKey)

  dispatch({
    type: SET_NAVIGATOR_VALUES,
    data: {
      navBeams: beamsSetCopy,
      activeBeamKeys,
      visibleBeamKeys,
      topVisibleBeamKey: newTopLvlBeamKey,
      topLvlBeamStartAbs: absBeamVals.start,
      topLvlBeamEndAbs: absBeamVals.end,
      topLvlRiderStartAbs: absRiderVals.start,
      topLvlRiderEndAbs: absRiderVals.end,
    }
  })
}



export type IDrillDownToViewAction = (
  navBeams: INavBeam[],
  activeBeamKeys: (FEGrainType)[],
  drilldownViewStart: number,
  drilldownViewEnd: number,
) => void

export const createDrillDownToViewAction = (dispatch: Dispatch): IDrillDownToViewAction => (
  navBeams,
  activeBeamKeys,
  drilldownViewStart,
  drilldownViewEnd,
) => {
  const drilldownViewSize = drilldownViewEnd - drilldownViewStart
  const activeBeamsSet = selectBeamsByKeys(navBeams, activeBeamKeys)

  // -------------------
  // Find beam with time range most tightly fitting(and inclusive of) time range of candlestick to drilldown to...
  let beamToDrillDownTo: INavBeam | undefined, beamIndex: number | undefined
  for (let i = activeBeamsSet.length -1; i >= 0; i--) {
    const thisBeam = activeBeamsSet[i]
    if(drilldownViewSize <= thisBeam.beamSize){
      beamToDrillDownTo = thisBeam // do not exit, keep looping through to continuously update watermark to find closest fit
      beamIndex = i
    }
  }

  // -------------------
  // Re-center beam + rider around drilldown view...
  if(
    beamToDrillDownTo !== undefined
    && beamIndex !== undefined
  ){
    const {
      newBeamStart,
      newRiderStart,
      newRiderEnd,
    } = centerBeamAndRiderToView(
      navBeams,
      beamToDrillDownTo,
      drilldownViewStart,
      drilldownViewEnd,
    )

    // Set new top level beam + rider size/position
    const updatedBeamSet = createUpdateNavBeamsSetAction(dispatch)(
      navBeams,
      activeBeamKeys,
      beamToDrillDownTo.key,
      newRiderStart,
      newRiderEnd,
      newBeamStart,
    )
    // set new top level beam
    if(updatedBeamSet !== undefined){
      createSetTopVisibleBeamAction(dispatch)(
        navBeams,
        activeBeamKeys,
        beamToDrillDownTo.key, {
          riderStart: newRiderStart,
          riderEnd: newRiderEnd,
          riderSize: drilldownViewSize,
        },
      )
    }
  }
}