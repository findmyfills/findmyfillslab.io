import { Dispatch } from "redux";
import { createAddToastAction } from "../actions";
import config from '../app.config';
import { createDataCallErrorToast } from '../components/Toaster';
import { Colors } from '../constants';
import { getCsvMetrics } from "../models";
import { PayloadCsvFill } from '../records';
import { SpinnerHosts } from "../reducers";
import { CsvSideType, CsvTimezoneType, HcScatterData } from '../types';
import { capBtw } from '../util';
import { createActivateSpinnerAction, createDeactivateSpinnerAction } from './_app-controls-actions';

export const UPDATE_USER_FILLS_METRICS_DATA = 'UPDATE_USER_FILLS_METRICS_DATA'


export type GetCsvFillsMetricsAction = (
  fileId: string,
  csv: string[][],
  symbol: string,
  date: string,
  tz: CsvTimezoneType,
  side: CsvSideType,
  endpoint: string | undefined
) => void


// ----------------------
export const SET_SELECT_FMF_CSV_DATA = 'SET_SELECT_FMF_CSV_DATA'
// ----------------------
export type SetSelectedFmfCsvData = (csvId: string) => void
export const createSetSelectedFmfCsvData = (dispatch: Dispatch): SetSelectedFmfCsvData => (csvId) => dispatch({
  type: SET_SELECT_FMF_CSV_DATA,
  data: csvId,
})



export const createGetCsvFillsMetricsAction = (dispatch: Dispatch): GetCsvFillsMetricsAction => (
  fileId,
  csv,
  symbol,
  date,
  tz,
  side,
  endpoint,
) => {
  createActivateSpinnerAction(dispatch)(SpinnerHosts.FILLS_SUMMARY_TABBLE)

  getCsvMetrics(
    csv,
    symbol,
    date,
    tz,
    side,
    endpoint,
  ).then((payload)=>{
    createDeactivateSpinnerAction(dispatch)(SpinnerHosts.FILLS_SUMMARY_TABBLE)

    if(payload instanceof Error){
      // UN-SUCCESSFUL data call...
      createAddToastAction(dispatch)(
        createDataCallErrorToast(payload.message)
      )
    } else {
      dispatch({
        type: UPDATE_USER_FILLS_METRICS_DATA,
        data: {
          sourceCsvId: fileId,
          summary: payload.summary,
          fills: payload.fills,
          fillsAsHcSeriesData: normalizeFillsToHcSeriesData(payload.fills)
          // Display user fills as Highcharts series
        }
      })

      createSetSelectedFmfCsvData(dispatch)(fileId)
    }
  })
}

export function getScatterPlotMarkerRadius(
  size: number,
  sizeLow: number,
  sizeHigh: number,
  pxRadiusMin: number,
  pxRadiusMax: number
){
  const ptPxSizeLow = pxRadiusMin
  const ptPxSizeHigh = pxRadiusMax * 0.5
  const ptPxSizeRange = ptPxSizeHigh - ptPxSizeLow
  const szRange = sizeHigh - sizeLow
  const markerRadius = size / szRange * ptPxSizeRange + ptPxSizeLow

  return capBtw(markerRadius, pxRadiusMin, pxRadiusMax)
}


function normalizeFillsToHcSeriesData(trades: PayloadCsvFill[]){

  const fillsScatterPlotSeriesData: HcScatterData[] = trades.map((trade)=>{
    const {TIME, SYMBOL, PRICE, SIZE, SIDE, VENUE, matched } = trade
    const markerR = getScatterPlotMarkerRadius(
      SIZE,
      config.TICKS_SIZE_LOW,
      config.TICKS_SIZE_HIGH,
      config.TICKS_DATA_SCATTER_NODE_RADIUS_MIN,
      config.TICKS_DATA_SCATTER_NODE_RADIUS_MAX,
    )
    return {
      x: TIME,
      y: PRICE,
      name: `${SIZE} ${SIDE} at ${VENUE}`, // accessible via point.key in tooltip formatter
      labelrank: SIZE,
      isBlockTrade: false, // TODO: make optional
      marker: {
        radius: markerR,
        lineWidth: 3,
        fillColor: Colors.TRANSPARENT,
        lineColor: matched > 0 ? Colors.HC_SCATTER_FILLS_MATCHED : Colors.HC_SCATTER_FILLS_UNMATCHED
      }
    }
  })
  return fillsScatterPlotSeriesData
}

