import { DUR_HR, DUR_MILLI, DUR_MIN, DUR_SEC, DUR_TRADING_DAY } from '../constants';
import { FEGrainType } from '../types';



export type INavBeam = {
  key: FEGrainType
  label: string
  beamColor: string
  beamStart: number // absolute value
  beamSize: number
  tickIntervalSize: number
  riderColor: string
  riderStart: number // relative to beam min
  riderEnd: number // relative to beam min
  riderSize: number // = riderEnd - riderStart
  childBeamRiderColor: string
  childBeamRiderStart: number // relative to beam min
  childBeamRiderSize: number
}

const MILLI_COUNT = 500
export const GRAIN_MILLI: INavBeam = {
  key: FEGrainType.MILLI,
  label: `${MILLI_COUNT}-Ms`,
  beamColor: '',
  beamStart: 0,
  beamSize: MILLI_COUNT * DUR_MILLI,
  tickIntervalSize: 50,
  riderColor:'',
  riderStart: 0,
  riderEnd: 0,
  riderSize: 0,
  childBeamRiderColor:'',
  childBeamRiderStart: 0, // not used...
  childBeamRiderSize: 0, // not used...
}

const SEC_COUNT = 30
export const GRAIN_SEC: INavBeam = {
  key: FEGrainType.SEC,
  label: `${SEC_COUNT}-Sec`,
  beamColor: '',
  beamStart: 0,
  beamSize: SEC_COUNT * DUR_SEC,
  tickIntervalSize: 1 * 1000,
  riderColor:'',
  riderStart: 0,
  riderEnd: 0,
  riderSize: 0,
  childBeamRiderColor:'',
  childBeamRiderStart: 0, // not used...
  childBeamRiderSize: 0, // not used...
}

const MIN_COUNT = 15
export const GRAIN_MIN: INavBeam = {
  key: FEGrainType.MIN,
  label: `${MIN_COUNT}-Min`,
  beamColor: '',
  beamStart: 0,
  beamSize: MIN_COUNT * DUR_MIN,
  tickIntervalSize: 5 * 1000,
  riderColor:'',
  riderStart: 0,
  riderEnd: 0,
  riderSize: 0,
  childBeamRiderColor:'',
  childBeamRiderStart: 0,
  childBeamRiderSize: 0,
}


const HR_COUNT = 1
export const GRAIN_HR: INavBeam = {
  key: FEGrainType.HR,
  label: `${HR_COUNT}-Hr`,
  beamColor: '',
  beamStart: 0,
  beamSize: HR_COUNT * DUR_HR,
  tickIntervalSize: 10 * 60 * 1000,
  riderColor:'',
  riderStart: 0,
  riderEnd: 0,
  riderSize: 0,
  childBeamRiderColor:'',
  childBeamRiderStart: 0,
  childBeamRiderSize: 0,
}

export const GRAIN_DAY: INavBeam = {
  key: FEGrainType.DAY,
  label: 'Day',
  beamColor: '',
  beamStart: 0,
  beamSize: DUR_TRADING_DAY,
  tickIntervalSize: 60 * 60 * 1000,
  riderColor:'',
  riderStart: 0,
  riderEnd: 0,
  riderSize: 0,
  childBeamRiderColor:'',
  childBeamRiderStart: 0,
  childBeamRiderSize: 0,
}

export const GRANULARITIES = [
  GRAIN_DAY, GRAIN_HR, GRAIN_MIN, GRAIN_SEC, GRAIN_MILLI
]


export const NAVIGATOR_BEAM_SCHEMAS = [GRAIN_SEC, GRAIN_MIN, GRAIN_HR, GRAIN_DAY, GRAIN_MILLI]






