// import '@blueprintjs//icons/lib/css/blueprint-icons.css';
// import '@blueprintjs/core/lib/css/blueprint.css';
// import 'normalize.css/normalize.css';
import React from 'react';
import LogOutButton from '../../components/LogOutButton';
import { withLogin } from '../../containers/FirebaseLogin';
import RealApp from '../../containers/App';

const DummyApp = ({
  onSignOut,
}) => (
  <div>
    <LogOutButton onSignOut={onSignOut}/>
    <h1>App Page</h1>
    <div>App contents </div>
  </div>
)

// const AppWithLogIn = withLogin(DummyApp)
const AppWithLogIn = withLogin(RealApp)

export default () => (
  <>
    <title>Find My Fills Login</title>
    <h1>login test page</h1>
    <AppWithLogIn/>
  </>
)