import { Reducer } from 'redux';
import { UPDATE_USER_FILLS_METRICS_DATA } from '../actions';
import { PayloadCsv } from '../models';


export function createCsvId(fileName: string, uploadTime: number){
  return `${fileName}_${uploadTime}`
}


export type UserFillsAction = {
  type: string
  data: any
}

export type UserFillsState = {
  [sourceCsvId: string]: PayloadCsv
}

const DEFAULT_STATE: UserFillsState = { }

const fillsMetricsReducer: Reducer<UserFillsState> = (
  state = DEFAULT_STATE,
  action
) => {
  const { type } = (action as UserFillsAction)
  switch (type) {
    case UPDATE_USER_FILLS_METRICS_DATA: {
      const { sourceCsvId, summary, fills, fillsAsHcSeriesData }: PayloadCsv & { sourceCsvId: string } = action.data
      if(state[sourceCsvId] === undefined){ // only update on new entry...
        return Object.assign({}, state, {
          [sourceCsvId]: {
            summary,
            fills,
            fillsAsHcSeriesData,
          },
        })
      }
    }
    default:
      return state
  }
}

export default fillsMetricsReducer