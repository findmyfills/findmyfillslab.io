import { Reducer } from 'redux';
import { SELECT_ACTIVE_BEAM_KEYS, SET_DAY_START, SET_NAVIGATOR_DIMENSIONS, SET_NAVIGATOR_VALUES, SET_NEW_TOP_LVL_BEAM_KEY, UPDATE_NAV_BEAMS_SET } from '../actions';
import { TRADING_DAY_START } from '../constants';
import { GRAIN_DAY, GRAIN_MIN, GRAIN_SEC, INavBeam, NAVIGATOR_BEAM_SCHEMAS } from '../schemas/navigator';
import { FEGrainType } from '../types';
import { getLast } from '../util';
import { configureNavigatorBeams } from '../components/MultiBeamNavigator';


export function sortBeamAscByBeamSize(navBeams: INavBeam[]) {
  return navBeams.sort((A, B) => A.beamSize - B.beamSize)
}

export type INavigatorAction = {
  type: string
  data: any
}


export function isTopVisibleBeamAlsoTopMostActive(state: INavigatorState){
  const { topVisibleBeamKey, activeBeamKeys } = state
  return topVisibleBeamKey === activeBeamKeys[0]
}

export function selectBeamsByKeys(navBeams: INavBeam[], beamKeys: string[]){
  return navBeams.filter(beam => beamKeys.indexOf(beam.key) !== -1)
}

export function selectBeamByKey(navBeams: INavBeam[] | undefined, beamKey: string){
  if(navBeams !== undefined){
    return navBeams.find(({ key }) => key === beamKey)
  }
}

export function selectBeamsBelowAndIncluding(navBeams: INavBeam[], beamKey: string){
  let beamsBelow: INavBeam[] = []
  const beamIndex = navBeams.findIndex(({ key }) => key === beamKey)
  if (beamIndex !== undefined){
    beamsBelow = navBeams.filter((beam, i) => i >= beamIndex)
  }
  return beamsBelow
}

export function selectBeamKeysBelowAndIncluding(navBeams: INavBeam[], beamKey: string){
  let beamKeysBelow: Array<FEGrainType> = []
  const beamIndex = navBeams.findIndex(({ key }) => key === beamKey)
  if (beamIndex !== undefined){
    navBeams.forEach((beam, i) => {
      if(i >= beamIndex) beamKeysBelow.push(beam.key)
    })
  }
  return beamKeysBelow
}

export function selectTopLvlBeam(state: INavigatorState){
  return selectBeamByKey(state.navBeams, state.topVisibleBeamKey)
}

export function selectBeamIndexByKey(navBeams: INavBeam[], beamKey: string){
  return navBeams.findIndex(({ key }) => key === beamKey)
}

export function selectBeamIndex(navBeams: INavBeam[], beam: INavBeam){
  return navBeams.findIndex(({ key }) => key === beam.key)
}

export function selectBeamBoundsAbsByKey(navBeams: INavBeam[], beamKey: string){
  const beam = selectBeamByKey(navBeams, beamKey)
  if (beam !== undefined){
    return getBeamBoundsAbs(beam)
  }
}
export function selectRiderBoundsAbsByKey(navBeams: INavBeam[], beamKey: string){
  const beam = selectBeamByKey(navBeams, beamKey)
  if (beam !== undefined){
    return getRiderBoundsAbs(beam)
  }
}
export function getBeamBoundsAbs(beam: INavBeam) { // but still needs day start for actual unix...
  const { beamStart } = beam
  const beamSize = Math.floor(beam.beamSize)
  return {
    start: Math.floor(beamStart),
    end: Math.floor(beamStart + beamSize),
    size: beamSize,
  }
}

export function getRiderBoundsAbs(beam: INavBeam) {
  const { beamStart, riderStart } = beam
  const riderSize = Math.floor(beam.riderSize)
  return {
    start: Math.floor(beamStart + riderStart),
    end: Math.floor(beamStart + riderStart + riderSize),
    size: riderSize,
  }
}

export type INavigatorState = {
  navBeams: INavBeam[]
  activeBeamKeys: Array<FEGrainType>
  visibleBeamKeys: Array<FEGrainType>
  selectedDayStartUtc: number
  isRiderAtNavStartAbs: boolean
  isRiderAtNavEndAbs: boolean
} & ITopBeamParams & INavLayout

export type ITopBeamParams = {
  topVisibleBeamKey: FEGrainType
  topLvlBeamStartAbs: number
  topLvlBeamEndAbs: number
  topLvlRiderStartAbs: number
  topLvlRiderEndAbs: number
}

export type INavLayout = {
  beamContainerW: number
  beamContainerH: number
  beamBarX: number
  beamBarY: number
  beamBarW: number
  beamBarH: number
  stepArrowW: number
  stepArrowH: number
  riderBarHandleW: number
  riderBarHandleH: number
  beamTicksH: number
  beamTicksLabelH: number
}

export function calcBeamLayout(beamContainerW: number, beamContainerH: number): INavLayout {
  const beamTicksH = 5
  const beamTicksLabelH = 10
  const beamBarH = beamContainerH - beamTicksH - beamTicksLabelH
  const stepArrowW = beamBarH
  const stepArrowH = stepArrowW
  const riderBarHandleH = beamBarH * 0.6
  const riderBarHandleW = riderBarHandleH * 1.0
  const beamBarX = stepArrowW
  const beamBarY = 0
  const beamBarW = beamContainerW - stepArrowW * 2
  return {
    beamContainerW,
    beamContainerH,
    riderBarHandleH,
    riderBarHandleW,
    beamBarX,
    beamBarY,
    beamBarW,
    beamBarH,
    stepArrowW,
    stepArrowH,
    beamTicksH,
    beamTicksLabelH,
  }
}

export const selectBeamLayoutParams = (state: INavigatorState): INavLayout => ({
  beamContainerW: state.beamContainerW,
  beamContainerH: state.beamContainerH,
  beamBarX: state.beamBarX,
  beamBarY: state.beamBarY,
  beamBarW: state.beamBarW,
  beamBarH: state.beamBarH,
  stepArrowW: state.stepArrowW,
  stepArrowH: state.stepArrowH,
  riderBarHandleW: state.riderBarHandleW,
  riderBarHandleH: state.riderBarHandleH,
  beamTicksH: state.beamTicksH,
  beamTicksLabelH: state.beamTicksLabelH,
})

export const selectTopBeamParams = (state: INavigatorState): ITopBeamParams => ({
  topVisibleBeamKey: state.topVisibleBeamKey,
  topLvlBeamStartAbs: state.topLvlBeamStartAbs,
  topLvlBeamEndAbs: state.topLvlBeamEndAbs,
  topLvlRiderStartAbs: state.topLvlRiderStartAbs,
  topLvlRiderEndAbs: state.topLvlRiderEndAbs,
})




function checkIfRiderAtNavBoundary(state: INavigatorState){
  const { topLvlRiderStartAbs, topLvlRiderEndAbs, navBeams } = state
  const botMostBeam = getLast(navBeams)

  return {
    isRiderAtNavStartAbs: topLvlRiderStartAbs === 0,
    isRiderAtNavEndAbs: topLvlRiderEndAbs === botMostBeam.beamStart + botMostBeam.beamSize
  }
}



const sortedBeamSchemas = sortBeamAscByBeamSize(NAVIGATOR_BEAM_SCHEMAS)
configureNavigatorBeams(sortedBeamSchemas)
console.log('sortedBeamSchemas', sortedBeamSchemas)


const activeBeamKeys = [
  GRAIN_SEC.key,
  GRAIN_MIN.key,
  // GRAIN_HR.key,
  GRAIN_DAY.key,
]
const visibleBeamKeys = [
  GRAIN_DAY.key,
]

const DEFAULT_STATE: INavigatorState = {
  navBeams: sortedBeamSchemas,
  activeBeamKeys,
  visibleBeamKeys,
  selectedDayStartUtc: TRADING_DAY_START,
  isRiderAtNavStartAbs: true,
  isRiderAtNavEndAbs: false,
  topVisibleBeamKey: GRAIN_DAY.key,
  topLvlBeamStartAbs: 0,
  topLvlBeamEndAbs: 0 + 23400 * 1000,
  topLvlRiderStartAbs: 0,
  topLvlRiderEndAbs: 0 + 3600 * 1000,

  // INavLayout
  ...calcBeamLayout(700, 45)
}

const navigatorReducer: Reducer<INavigatorState> = (
  state = DEFAULT_STATE,
  action
) => {
  const { type } = (action as INavigatorAction)
  switch (type) {

    case UPDATE_NAV_BEAMS_SET: {
      const updatedBeams: INavBeam[] = action.data
      return Array.isArray(updatedBeams) ?
        Object.assign({}, state, {
          navBeams: updatedBeams
        }) : state
    }

    case SET_NAVIGATOR_VALUES: {
      const updatedNavVals: ITopBeamParams | undefined = action.data
      if(updatedNavVals === undefined) return state

      const updatedState = Object.assign({}, state, updatedNavVals)

      return Object.assign(updatedState,{
        ...checkIfRiderAtNavBoundary(updatedState)
      })
    }

    case SET_NEW_TOP_LVL_BEAM_KEY: {
      const newTopLvlBeamKey: string = action.data
      return Object.assign({}, state, {
        topVisibleBeamKey: newTopLvlBeamKey,
      })
    }

    case SET_DAY_START: {
      const selectedDayStartUtc: number = action.data
      return Object.assign({}, state, {
        selectedDayStartUtc,
      })
    }

    case SELECT_ACTIVE_BEAM_KEYS: {
      const activeBeamKeys: Array<FEGrainType> = action.data
      return Object.assign({}, state, {
        activeBeamKeys,
      })
    }

    case SET_NAVIGATOR_DIMENSIONS: {
      const { navigatorW, navigatorH }: Record<string, number> = action.data

      return Object.assign({}, state, {
        ...calcBeamLayout(navigatorW, state.beamContainerH)
      })
    }

    default:
      return state
  }
}

export default navigatorReducer




