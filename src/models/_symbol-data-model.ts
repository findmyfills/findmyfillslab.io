import config from '../app.config';
import { BEGrainType, IOption, SymbolDataPayload } from '../types';
export * from './_user-fills-model';

export function createSymbolDataApiUrl(backendGrainType: BEGrainType, symbol: string, startMilli: number, endMilli: number) {
  return `${config.API_DOMAIN}/chart/${backendGrainType.toLowerCase()}/${symbol}/${Math.floor(startMilli)}/${Math.floor(endMilli)}/`
}

export function getSymbolData(symbolDataApiUrl: string) {
  const reqStart = Date.now()
  return new Promise<SymbolDataPayload | Error>((resolve) => {
    fetch(symbolDataApiUrl, {
      credentials: 'include',
    }).then(res => {
      if(res.status === 200){
        res.json().then((data: SymbolDataPayload) => {
          const reqEnd = Date.now()
          const reqTime = reqEnd - reqStart
          console.log(`%c <--- Symbol data received in ${reqTime}ms: ${data.ticks.length} data pts --- `, 'background:teal;color:white')
          resolve (data)
        })
      } else {
        res.json().then((data: { error: string }) => {
          const { error } = data
          resolve(new Error(`${res.status}: ${error}`))
        })
      }
    })
  })
}

type IexSymbolRefData = {
  date: string // "2019-06-19"
  iexId: string // "2"
  isEnabled: boolean // true
  name: string // "Agilent Technologies Inc."
  symbol: string // "A"
  type: string // "cs"
}

export function getSymbolRefData() {
  return new Promise(function(resolve) {
    fetch("https://api.iextrading.com/1.0/ref-data/symbols").then(res => {
      res.json().then(data => {
        resolve(
          data.reduce((prev: IOption[], { symbol, name }: IexSymbolRefData) => {
            prev.push({
              value: symbol,
              label: `${symbol} - ${name}`,
            })
            return prev
          }, []) // key by symbol name
        )
      })
    })
  })
}
