# Find My Fills
Find My Fills is a single stock exploration tool that allow users to drill down into ticks for a given symbol and match their executions to trades on the tape. Developed and maintained by [Proof](https://prooftrading.com).

This repo contains the front end for findmyfills.com, built with Next.js and served using GitLab Pages. For more information check out our [introductory blog post](https://medium.com/prooftrading/find-my-fills-introducing-a-new-tool-for-visualizing-market-activity-at-every-timescale-4bb7df2dee0e) and [technical deep dive](https://medium.com/prooftrading) on Medium, or give us a shout at [dev@prooftrading.com](mailto:dev@prooftrading.com).

[![pipeline status](https://gitlab.com/findmyfills/findmyfills.gitlab.io/badges/master/pipeline.svg)](https://gitlab.com/findmyfills/findmyfills.gitlab.io/-/commits/master)



<!---------------------------------->
## I. Quick Start
<!---------------------------------->

### Local Development

1. Install dependencies.
	```sh
	$ npm install
	```
2. Start Next.js dev server.
	```sh
	$ npm run next:dev
	```
3. Go to [localhost:8080](http://localhost:8080).

4. Make changes in `src/pages/index.js`. Dev server will auto-update browser as you edit.

### Deploying to Production

1. Commit your changes, push or merge to `master`. Any commit added to `master` will trigger a build and release into production.
2. See your changes on [https://findmyfills.com](https://findmyfills.com).



<!---------------------------------->
## II. Project Information
<!---------------------------------->


### License

This project is made available under MIT License.
